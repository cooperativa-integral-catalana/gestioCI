# Codenames

The following naming proposal was written during the morning without consuming any toxic substances a part from happiness. The criteria behind this list was to find an Actor-Actitude binomial expression that could cause impact to the reader. The references to real world are purely arbitrary.

## 1st iteration

* Activista Apalancado
* Barrendero Bolchevique
* Camarada Concentrado
* Dictador Destituido
* Etarra Empoderado
* Falangista Fulminado
* Guru Gamberro
* Heliogábalo Hipócrita
* Insurreccionalista Inmolado
* Jabalí Jodido
* Kinki Kamikaze
* Letrado Leninista
* Marqués Miserable
* Narcotraficante Neurótico
* Obrero Organizado
* Pensador Profesionalizado
* Quilombo Queer
* Revuelta Radical
* Saboteador Solipsista
* Terrorista Tranquilo
* Ungüento Urticante
* Virus Visionario
* Waterpolista Wagneriano
* Xenófobo Xicache
* Yegua Yiadista
* Zar Zumbado

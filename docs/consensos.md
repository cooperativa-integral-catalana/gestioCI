Consensos
=========

Lista de consensos a los que hemos llegado entre reuniones y mailing lists.

* Los commits se firman.
	
	# la opción -S sirve para firmar con la llave pública por defecto.
	git commit -S -am "my signed commit"

* Utilizamos pep8 salvo alguna excepción. Hay un hook pre-commit para git en la carpeta hooks/.

* Las actas de las reuniones deberán de estar en docs/actas/.


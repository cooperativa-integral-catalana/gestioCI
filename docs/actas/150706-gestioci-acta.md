Ordre del día - Dilluns 6 de July 2015
======================================

Propostes
---------

* Parlar sobre els canvis fets al fitxer de consensos [done]
* CRUD empreses [done]
* Parlar del workflow que fem servir amb GIT [done]
* uwsgi vs gunicorn vs runserver [done]
* error #1180 [done]


Calendari
---------

* Reunio pas a produccio:
    - Jueves dia 9 de 17:30h a 19:00h

* Reunio dahsboard:
    - Semana que viene día a concretar el lunes en la reunión.


Scrum
-----

### Retrospectiva

### Demo

### feature/crud-clients-proveidors [done]
* revisio de part clients...

#### feature/deploy [done]
* Configurar django-uwsgi-nginx. uwsgi queda integrat al sistema i no cal preocupar-se d'arrancar-lo o reiniciar-lo.
Falta hacer mas pruebas...

#### feature/consensos [done]
* afegir els canvis conensuats com a nous consensos

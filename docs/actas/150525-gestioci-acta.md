Orden del día
=============

Informativos
------------

### Entorn de proves ###

* https://gestio-test.cooperativa.cat:7582/

Coses que s'han de provar a l'entorn de proves:
    * canvi de contrasenya
    * alta i modificacio de sessions d'acollida i de moneda social
    * workflow amb un cas complet de projecte autoocupat
 -> duub ho provarà

 -> pablo: actualitzar stage a la 0.5.0


Reunió 2n esprint
-----------------

### Retrospectiva (15min.)

* Daily Scrum decidim deixar de fer-lo i només comentar si hi ha dubtes, problemes crítics o es preveu un desfase de la planificació.

* Revisar hores/setmana/equip efectives.

Passem a contar:
    - 18h/setmana efkin
    - 20h/setmana pablo 
         
de sprint efectiu.

### Demos 

* s'han pogut fer gran part de les demos dels dos punts.

Propuestas
----------

### Asier has buscat un widget javascript selector de dates i hores? ###

No. (està fora i tornem a parlar 1 de juny)

### Aclarir dubtes sobre Crear Projecte (implementació botó)

S'ha decidit de que es posaria en el pas 20 i s'han aclarit els dubtes tècnics per poder-ho fer.

### Planificació Sprint

efkin ha pres/continua les històries #1021 i #1033
pablo ha pres/continua les històries #1025 i #1029

també es farà la tasca #1051 merge de la release 0.5.0 a master



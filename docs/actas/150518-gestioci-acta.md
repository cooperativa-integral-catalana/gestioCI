Orden del día
=============

Informativos
------------

### Repaso de git-flow ###

### Entorn de proves ###
* https://gestio-test.cooperativa.cat:7582/
Coses que s'han de provar a l'entorn de proves:
    * canvi de contrasenya
    * alta i modificacio de sessions d'acollida i de moneda social
    * workflow amb un cas complet de projecte autoocupat
    
### Intro a scrum ###
Duub presenta scrum al dev-team junto a "Scrum en 1000 palabras" 


Reunió de planificacio 1er esprint
----------------------------------

### Histories: (títol història - repsonsable - importancia - estimació) ###
    Crear projecte - efkin - 110 - 3 
    Models facturació - pablo - 116 - 4

### Revisar: ###
    Tasques, dependències, tasques per sprint i assignacions de tasques (em sembla lògic que sigui en aquest ordre) [DONE]

### Tasquejar facturació ###
    - Definir quines funcions inclouran a cada versió per poder posar el roadmap al redmine, una forma fàcil podria ser que cada versió fos les funcions d'un sprint.

    Es crearà una nova "target versión" i es posaran allà les històries del nou procés de scrum 


Propuestas
----------

### Proposta de format per taules ###

* Hem d'arribar a un consens sobre com mostrem dades tabulades, p.ex. afegir atributs class="" als <td>, <th>, etc.? Asier te cap altra proposta en ment? com fem aliniat centrat, a la dreta, com fem colspans i rowspans aliniats a dalt, o al centre, etc.
<table>
<thead><tr><th>xx</th></tr><thead>
<tbody><tr><td>yy</td></tr></tbody>
</table>

    Aprobat!!!


### Asier has buscat un widget javascript selector de dates i hores? ###

    No. 


### Idees per un logo / icona favicon? ###

    hasta que no haya propuestas concretas y/o decentes no se vuelve a traer el punto.


### Propuesta consenso, estandarizar nombre versiones ###
Ahora hay un tag: 0.4.0, y un release en proceso v0.5. Decidir el formato y añadirlo a docs/consensos.md
Asier: me gusta x.y.z, la "v" me parece que no aporta nada y los tres numeros (x,y,z) queda más bonito en listas:
    0.5.2      0.5.2
    0.5.0  vs  0.5
    0.4.0      0.4
     ...        ...
     pablo: ok
     efkin: ok
     aprobado!!! falta duub!!!
     duub: ok!

### Propuesta consenso, estandarizar como se tabula: espacios vs tabs? 2 vs 4? ###
Diria que casi todo el código esta en 4 espacios, pero me acabo de encontrar tabuladores. Poner en consesos que se tabula con 4 espacios?

    pablo: ok

    efkin: ok

    aprobado!!! falta duub!!! aunque esto ya es un consenso del pep8

    duub: ok!


### Simplificar modelos User-Persona? ###
Tiene pinta de que hay más cosas de las que se necesitan, una tabla intermedia para juntar persona y user? preferencias?

    pablo revisará los modelos, per ara es treu del codi.

    https://docs.djangoproject.com/en/1.8/topics/auth/customizing/#extending-the-existing-user-model

    https://docs.djangoproject.com/en/1.8/topics/db/models/#proxy-models

### Referirse al servidor de pruebas como Staging en vez de Testing ###
Ahora hay un settings/testing.py y puede llevar a confusión si es de los tests unit/funcionales o del servidor.

    doup cambiará el nombre de testing.py a staging.py y entre todas nos referiremos a testing como "staging".

    stage.gestioci.cooperativa.cat

    gestioci-stage.cooperativa.cat

    pablo parlar amb verneda per arreglar l'accés a la instància de staging.


Propuestas de merge
-------------------

### feature/env-feedback ###
Environment feedback added:
– ENV property added in settings (development + production)
– background change for testing ENV
– show ENV in footer in non-production ENVs
– context_processors & gci_tags clean-up
[DONE]

### feature/tables-design ###
Style Guide updated with table & align examples
[DONE]

### Export a CSV ###
[DONE]

### merge branch "test/pablo-consolidation" a "develop" ###
la branca "test/pablo-consolidation" 'es el resultat d'un merge 'traumatic' que inclou:
* base "develop", amb bugfixes simples pero importants incorporats des de "release/v0.5"
* "feature/fix-python-packaging-mayhem" — arreglar packaging arran del refactor a apps/
* "feature/edit-pao-redirect-vs-forbidden" — enlloc de 'petar' quan manquen permissos, redirigir l'usuari a vista_resum
* "release/v0.5" — col·leccio de canvis que calien per anar a testing i a produccio
[DONE]

Propuesta de master
-------------------
Se produce release/0.5.0!!! 
La semana que viene pasaremos a master
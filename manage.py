#!/usr/bin/env python
# import os
import sys

if __name__ == "__main__":
    # this was not the place to update this thing.
    # please set your own environment variable, or tell your IDE, along with a random SECRET_KEY
    # e.g.
    # echo export GCI_SECRET_KEY='asdfg' >> ~/.bashrc
    # echo export DJANGO_SETTINGS_MODULE='gestioci.settings.development' >> ~/.bashrc
    #
    # os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gestioci.settings.development")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

from django_cron import CronJobBase, Schedule

from facturacio.models import SequenciaNumeroFacturaEmesa
from .models import Trimestre


class CronJobMantenimentTrimestres(CronJobBase):

    RUN_AT_TIMES = ['5:51']

    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'facturacio.CronJobMantenimentTrimestres'

    def do(self):
        return Trimestre.crear_trimestres_necessaris()


class CronJobMantenimentSequenciaNumeroFacturaEmesa(CronJobBase):

    RUN_AT_TIMES = ['5:52']

    schedule = Schedule(run_at_times=RUN_AT_TIMES)
    code = 'facturacio.CronJobMantenimentSequenciaNumeroFacturaEmesa'

    def do(self):
        return SequenciaNumeroFacturaEmesa.crear_sequencies_necessaries()

# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from facturacio.models import FacturaRebuda, LiniaFactura

import codecs


class Command(BaseCommand):
    help = 'Export to csv all FacturaEmesa with an invoice number set'

    def handle(self, *args, **options):

        frs = FacturaRebuda.objects.filter(estat=FacturaRebuda.ESTAT_ACCEPTADA)
        
        with codecs.open("gci-factures-rebudes-2016.csv", "w", encoding="utf-8") as f:
            f.write("num_socia$cif_client$cif_proveidor$nom_client$nom_proveidor$num_factura$data$base_imponible$tipus_iva$iva_parcial$perc_irpf$irpf$total_factura")
            f.write("\n")
            for fr in frs:
                iva_tuples = [(sum([l.base_total for l in fr.liniafactura_set.filter(tipus_iva=tipus)]), tipus, sum([l.iva_total for l in fr.liniafactura_set.filter(tipus_iva=tipus)])) for tipus in [LiniaFactura.IVA_0, LiniaFactura.IVA_4, LiniaFactura.IVA_10, LiniaFactura.IVA_21]]
                for t in iva_tuples:
                    f.write(fr.projecte.compte_ces_assignat+"$"+fr.client.nif+"$"+fr.proveidor.nif+"$"+fr.client.nom_fiscal+"$"+fr.proveidor.nom_fiscal+"$"+fr.numero+"$"+str(fr.data)+"$"+str(t[0])+"$"+str(t[1])+"$"+str(t[2])+"$"+str(fr.tipus_irpf)+"$"+str(fr.irpf_total)+"$"+str(fr.import_total))
                    f.write("\n")



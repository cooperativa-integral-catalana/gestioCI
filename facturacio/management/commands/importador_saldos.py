from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from projectes.models import ProjecteAutoocupat
from facturacio.helpers import trobar_balanc_actual
from facturacio.models import Moviment
from gestioci.helpers import now

from decimal import Decimal
import csv


class Command(BaseCommand):
    help = "Importador sencill de saldos desde el transversal cap al gci"

    def add_arguments(self, parser):
        parser.add_argument('filename', type=str)
        parser.add_argument(
            '--dry-run',
            action='store_true',
            dest='dryrun',
            default=False,
            help='Prevent applying any changes to the database',
        )
        
    def handle(self, *args, **options):

        self.stdout.write("Initializing script... Opening file..")
        with open(options['filename']) as csvfile:
            
            reader = list(csv.DictReader(csvfile))

            self.stdout.write(
                "In database: %d\n" % ProjecteAutoocupat.objects.count()
            )
            self.stdout.write(
                "In file: %d\n" % len(reader)
            )
            import_failures = 0
            import_successes = 0
            for row in reader:
                coop = row['coop']
                if ProjecteAutoocupat.objects.filter(
                        compte_ces_assignat=coop
                ).exists():
                    projecte = ProjecteAutoocupat.objects.get(
                        compte_ces_assignat=coop
                    )
                    balanc = trobar_balanc_actual(projecte)['balanc_actual']
                    self.stdout.write("Actual balance: %d\n" % balanc)
                    saldo_real = Decimal(
                        row['saldo_socio'].replace(",", "")  # remove commas
                    ) * Decimal(-1)  # csv comes with inverted signs
                    self.stdout.write("Saldo real: %s\n" % saldo_real)
                    diferencia = saldo_real - balanc
                    if diferencia == Decimal("0.00"):
                        self.stdout.write(
                            "No operation needed for %s\n" % coop
                        )
                        continue
                    if not options['dryrun']:
                        data = now().date()
                        user = User.objects.get(
                            username="efkin@cooperativa.cat"
                        )
                        Moviment.objects.create(
                            projecte_id=projecte.id,
                            cooperativa_id=projecte.cooperativa_assignada.id,
                            quantitat=diferencia,
                            data=data,
                            concepte="Saldo actualitzat amb data %s" % data,
                            created_by=user,
                            updated_by=user,
                        )
                    import_successes += 1
                else:
                    self.stdout.write(
                        "No match on database for %s\n" % coop
                    )
                    import_failures += 1

            self.stdout.write(
                "%d successfully imported\n" % import_successes
            )
            self.stdout.write(
                "%d unsuccessfully imported\n" % import_failures
            )

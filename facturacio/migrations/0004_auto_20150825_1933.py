# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('facturacio', '0003_auto_20150822_1650'),
    ]

    operations = [
        migrations.AddField(
            model_name='facturarebuda',
            name='vist_i_plau_data',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='facturarebuda',
            name='vist_i_plau_usuaria',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='facturarebuda',
            name='vist_i_plau_observacions',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='facturarebuda',
            name='estat',
            field=models.CharField(default=b'rebuda', max_length=16, choices=[(b'rebuda', 'Pendent de revisi\xf3'), (b'acceptada', 'Acceptada'), (b'rebutjada', 'Rebutjada')]),
        ),
    ]

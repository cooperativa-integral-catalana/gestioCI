# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0014_auto_20160225_0009'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='balanctancamenttrimestre',
            name='created_by',
        ),
        migrations.RemoveField(
            model_name='balanctancamenttrimestre',
            name='projecte',
        ),
        migrations.RemoveField(
            model_name='balanctancamenttrimestre',
            name='trimestre',
        ),
        migrations.RemoveField(
            model_name='balanctancamenttrimestre',
            name='updated_by',
        ),
        migrations.DeleteModel(
            name='BalancTancamentTrimestre',
        ),
    ]

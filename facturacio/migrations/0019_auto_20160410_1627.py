# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0018_add_quotatrimestral'),
    ]

    operations = [
        migrations.AlterField(
            model_name='factura',
            name='forma_de_pagament',
            field=models.CharField(default=b'efectiu', max_length=16, choices=[(b'efectiu', 'Cobrament al comptat'), (b'transferencia', 'Ingr\xe9s al compte corrent de la cooperativa')]),
        ),
        migrations.AlterField(
            model_name='facturarebuda',
            name='tipus_irpf',
            field=models.DecimalField(default=0, help_text='en tant percent (%)', verbose_name="retenci\xf3 per l'IRPF", max_digits=4, decimal_places=2),
        ),
    ]

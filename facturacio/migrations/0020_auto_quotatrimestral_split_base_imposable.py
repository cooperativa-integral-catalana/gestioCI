# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0019_auto_20160410_1627'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='quotatrimestral',
            name='base_imposable',
        ),
        migrations.AddField(
            model_name='quotatrimestral',
            name='base_imposable_emesa',
            field=models.DecimalField(default=-666, max_digits=8, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='quotatrimestral',
            name='base_imposable_rebuda',
            field=models.DecimalField(default=-666, max_digits=8, decimal_places=2),
            preserve_default=False,
        ),
    ]

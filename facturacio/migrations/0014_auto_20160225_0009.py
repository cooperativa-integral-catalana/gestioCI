# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0013_conciliaciomovimentextractebancari'),
    ]

    operations = [
        migrations.AddField(
            model_name='extractebancari',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 15, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='extractebancari',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 15, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='movimentextractebancari',
            name='created_at',
            field=models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 15, tzinfo=utc), auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='movimentextractebancari',
            name='estat',
            field=models.CharField(default=b'pendent', max_length=16, choices=[(b'pendent', 'Pendent de conciliaci\xf3'), (b'conciliat', 'Conciliat'), (b'descartat', 'Descartat'), (b'delegat', 'Delegat')]),
        ),
        migrations.AddField(
            model_name='movimentextractebancari',
            name='numero_moviment',
            field=models.IntegerField(),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='movimentextractebancari',
            name='updated_at',
            field=models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 15, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AlterUniqueTogether(
            name='movimentextractebancari',
            unique_together=set([('extracte_bancari', 'numero_moviment')]),
        ),
        migrations.RemoveField(
            model_name='movimentextractebancari',
            name='status',
        ),
    ]


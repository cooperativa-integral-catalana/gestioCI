# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0020_auto_quotatrimestral_split_base_imposable'),
    ]

    operations = [
        migrations.AddField(
            model_name='factura',
            name='observacions',
            field=models.TextField(max_length=4096, blank=True),
        ),
    ]

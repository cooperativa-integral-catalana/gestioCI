# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0002_auto_20150822_1600'),
    ]

    operations = [
        migrations.CreateModel(
            name='NumeroFacturaEmesa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.AlterModelOptions(
            name='quotatrimestral',
            options={'verbose_name_plural': 'quotes trimestrals'},
        ),
        migrations.RemoveField(
            model_name='factura',
            name='numero',
        ),
        migrations.RemoveField(
            model_name='factura',
            name='year',
        ),
        migrations.RemoveField(
            model_name='facturaemesa',
            name='recarreg_equivalencia',
        ),
        migrations.RemoveField(
            model_name='facturarebuda',
            name='retencio_irpf',
        ),
        migrations.AddField(
            model_name='factura',
            name='base_total',
            field=models.DecimalField(default=0, max_digits=8, decimal_places=2),
        ),
        migrations.AddField(
            model_name='facturaemesa',
            name='req_total',
            field=models.DecimalField(default=0, max_digits=8, decimal_places=2),
        ),
        migrations.AddField(
            model_name='facturarebuda',
            name='irpf_total',
            field=models.DecimalField(default=0, max_digits=8, decimal_places=2),
        ),
        migrations.AddField(
            model_name='facturarebuda',
            name='numero',
            field=models.CharField(default='(none)', max_length=64),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='facturarebuda',
            name='tipus_irpf',
            field=models.DecimalField(default=0, help_text='en tant percent', verbose_name="retenci\xf3 per l'IRPF", max_digits=4, decimal_places=2),
        ),
        migrations.AddField(
            model_name='liniafactura',
            name='base_total',
            field=models.DecimalField(default=0, max_digits=8, decimal_places=2),
        ),
        migrations.AddField(
            model_name='liniafactura',
            name='import_total',
            field=models.DecimalField(default=0, max_digits=8, decimal_places=2),
        ),
        migrations.AddField(
            model_name='liniafactura',
            name='iva_total',
            field=models.DecimalField(default=0, max_digits=8, decimal_places=2),
        ),
        migrations.AddField(
            model_name='liniafactura',
            name='req_total',
            field=models.DecimalField(default=0, max_digits=8, decimal_places=2),
        ),
        migrations.AddField(
            model_name='liniafactura',
            name='tipus_req',
            field=models.DecimalField(default=0, help_text='en tant percent', verbose_name="tipus de rec\xe0rrec d'equival\xe8ncia", max_digits=4, decimal_places=2),
        ),
        migrations.AlterField(
            model_name='liniafactura',
            name='tipus_iva',
            field=models.DecimalField(default=21, help_text='en tant percent', verbose_name="tipus d'iva", max_digits=4, decimal_places=2),
        ),
        migrations.AddField(
            model_name='facturaemesa',
            name='_numero',
            field=models.ForeignKey(blank=True, to='facturacio.NumeroFacturaEmesa', null=True),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('facturacio', '0012_facturaemesa_numero'),
    ]

    operations = [
        migrations.CreateModel(
            name='ConciliacioMovimentExtracteBancari',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comentaris', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.RenameField(
            model_name='moviment',
            old_name='detalls',
            new_name='concepte',
        ),
        migrations.AlterField(
            model_name='movimentextractebancari',
            name='status',
            field=models.CharField(default=b'pendent', max_length=16, choices=[(b'pendent', 'Pendent de conciliaci\xf3'), (b'conciliat', 'Conciliat'), (b'descartat', 'Descartat')]),
        ),
        migrations.CreateModel(
            name='ConciliacioMovimentExtracteBancariFactura',
            fields=[
                ('conciliaciomovimentextractebancari_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='facturacio.ConciliacioMovimentExtracteBancari')),
                ('conciliacio_automatica', models.BooleanField(default=False)),
                ('conciliacio_automatica_observacions', models.TextField()),
                ('factura', models.ForeignKey(to='facturacio.Factura')),
                ('moviment_balanc_ajust', models.ForeignKey(to='facturacio.Moviment', null=True)),
            ],
            bases=('facturacio.conciliaciomovimentextractebancari',),
        ),
        migrations.AddField(
            model_name='conciliaciomovimentextractebancari',
            name='created_by',
            field=models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='conciliaciomovimentextractebancari',
            name='moviment_balanc',
            field=models.ForeignKey(to='facturacio.Moviment'),
        ),
        migrations.AddField(
            model_name='conciliaciomovimentextractebancari',
            name='moviment_extracte',
            field=models.ForeignKey(to='facturacio.MovimentExtracteBancari'),
        ),
        migrations.AddField(
            model_name='conciliaciomovimentextractebancari',
            name='updated_by',
            field=models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL),
        ),
    ]

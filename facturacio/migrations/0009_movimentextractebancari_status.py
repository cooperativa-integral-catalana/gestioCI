# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0008_auto_20160205_0936'),
    ]

    operations = [
        migrations.AddField(
            model_name='movimentextractebancari',
            name='status',
            field=models.CharField(default=b'pendent', max_length=16, choices=[(b'pendent', 'pendent'), (b'conciliat', 'conciliat'), (b'descartat', 'descartat')]),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models, connection


def initialize_sequencianumerofacturaemesa(apps, schema_editor):

    connection.cursor().execute(
        """INSERT INTO
               facturacio_sequencianumerofacturaemesa (year, value)
           SELECT
               EXTRACT(year FROM factura.data) AS year,
               MAX(facturaemesa._numero) AS value
           FROM
               facturacio_factura AS factura JOIN facturacio_facturaemesa AS facturaemesa
                   ON factura.id = facturaemesa.factura_ptr_id
           GROUP BY
               year""")


def assemble_numero_factura(apps, schema_editor):

    connection.cursor().execute(
        """UPDATE
               facturacio_facturaemesa
           SET
               numero = lookup.numero
           FROM (

                   SELECT
                       factura.id AS id,
                       CONCAT(EXTRACT(YEAR FROM factura.data), '/', TO_CHAR(facturaemesa._numero, 'FM000000')) AS numero
                   FROM
                       facturacio_factura AS factura JOIN facturacio_facturaemesa AS facturaemesa
                           ON factura.id = facturaemesa.factura_ptr_id
                   WHERE facturaemesa._numero IS NOT NULL
           ) AS lookup
           WHERE
               facturacio_facturaemesa.factura_ptr_id = lookup.id""")


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0011_movimentextractebancari_validation'),
    ]

    operations = [
        migrations.CreateModel(
            name='SequenciaNumeroFacturaEmesa',
            fields=[
                ('year', models.SmallIntegerField(serialize=False, primary_key=True)),
                ('value', models.IntegerField()),
            ],
        ),
        migrations.AlterField(
            model_name='facturaemesa',
            name='_numero',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.DeleteModel(
            name='NumeroFacturaEmesa',
        ),
        migrations.RunPython(initialize_sequencianumerofacturaemesa),
        migrations.AddField(
            model_name='facturaemesa',
            name='numero',
            field=models.CharField(max_length=11, null=True, blank=True),
        ),
        migrations.RunPython(assemble_numero_factura),
        migrations.RemoveField(
            model_name='facturaemesa',
            name='_numero',
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import re
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0010_movimentextractebancari_null_to_blank'),
    ]

    operations = [
        migrations.AlterField(
            model_name='movimentextractebancari',
            name='compte_projecte',
            field=models.CharField(blank=True, max_length=16, validators=[django.core.validators.RegexValidator(regex=re.compile(b'^(COOP\\d{4})?$'), message='el format del compte de projecte ha de ser COOP9999')]),
        ),
        migrations.AlterField(
            model_name='movimentextractebancari',
            name='numero_factura',
            field=models.CharField(blank=True, max_length=64, validators=[django.core.validators.RegexValidator(regex=re.compile(b'^(\\d{4}/\\d{6})?$'), message='el n\xfamero de factura ha de ser yyyy/999999')]),
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0016_liquidaciotrimestre'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='QuotaTrimestral',
            new_name='TarifaQuotaTrimestral',
        ),
    ]

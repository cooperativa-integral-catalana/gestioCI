# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('facturacio', '0006_extractebancari_movimentextractebancari'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='factura',
            name='data_prevista_pagament',
        ),
    ]

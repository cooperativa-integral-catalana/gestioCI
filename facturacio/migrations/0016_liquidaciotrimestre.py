# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('projectes', '0003_auto_20160213_1257'),
        ('facturacio', '0015_delete_balanctancamenttrimestre'),
    ]

    operations = [
        migrations.CreateModel(
            name='LiquidacioTrimestre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_auto_liquidacio', models.DateTimeField(null=True)),
                ('data_liquidacio', models.DateTimeField(null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('balanc_final', models.DecimalField(default=0, max_digits=8, decimal_places=2)),
                ('projecte', models.ForeignKey(related_name='+', to='projectes.ProjecteAutoocupat')),
                ('trimestre', models.ForeignKey(related_name='+', to='facturacio.Trimestre')),
                ('usuari_auto_liquidacio', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, null=True)),
                ('usuari_liquidacio', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'Liquidaci\xf3 de trimestre',
                'verbose_name_plural': 'Liquidacions de trimestres',
            },
        ),
        migrations.AlterUniqueTogether(
            name='liquidaciotrimestre',
            unique_together=set([('trimestre', 'projecte')]),
        ),
    ]

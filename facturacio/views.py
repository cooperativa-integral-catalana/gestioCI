# coding=utf-8
import csv
import datetime
import json
import re

from decimal import Decimal

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.exceptions import SuspiciousOperation
from django.db import transaction
from django.db.models import Sum, Max
from django.forms import inlineformset_factory, modelformset_factory, TextInput, Select
from django.http.response import HttpResponseForbidden, HttpResponse, HttpResponseBadRequest
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader, Context
from django.views.generic.list import ListView
from django.utils.decorators import method_decorator

from empreses.models import Empresa, Cooperativa
from facturacio.forms import FormLiquidacioTrimestre
from facturacio.helpers import trobar_o_crear_liquidaciotrimestre
from gestioci.helpers import now, today
from gestioci.settings import auth_groups
from projectes.models import ProjecteAutoocupat
from socies.models import AdrecaFiscal

from .forms import FormCrearFacturaEmesa, FormConfirmarFacturaEmesa, FormCrearFacturaRebuda, \
    FormEditarFacturaRebuda, FormCercarValidacioFacturaRebuda, FormValidacioFacturaRebuda, \
    FormSeleccioVolumFacturacio, BuscarEmpresaForm, FormulariImportExtracteBancari, \
    FormConciliacioAutomaticaMovimentExtracteBancariFactura, FormDescartarMovimentExtracteBancari, \
    FormFiltreLiquidacioLlistatProjectes, CrearEmpresaForm, ModificarEmpresaForm, ExportInvoicesCsvForm,\
    ExportFeesCsvForm, FormulariFiltrarLlistatAportacionsTrimestre
from .helpers import calcular_periode, trobar_base_imposable_i_iva, trobar_quota_trimestral, trobar_primer_trimestre_obert, \
    trobar_ultim_trimestre_tancat, trobar_projectes_usuari, trobar_balanc_actual, is_user_responsable_facturacio, \
    tancar_trimestre_projecte, is_user_responsable_bancari
from .models import TarifaQuotaTrimestral, Factura, FacturaEmesa, FacturaRebuda, Trimestre, \
    ExtracteBancari, MovimentExtracteBancari, LiniaFactura, Moviment, QuotaTrimestral, \
    ConciliacioMovimentExtracteBancariFactura, LiquidacioTrimestre, ConciliacioMovimentExtracteBancari


@login_required
def rectificar_factura_emesa(request, id_factura):

    factura = get_object_or_404(FacturaEmesa, id=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden()

    try:
        assert factura.numero, "No es pot rectificar un'esborrall de factura"
        with transaction.atomic():
            f = FacturaEmesa.objects.create(
                projecte=factura.projecte,
                proveidor=factura.proveidor,
                client=factura.client,
                data=now().date(),
            )
            for linea in factura.liniafactura_set.all():
                LiniaFactura.objects.create(
                    factura=f,
                    quantitat=linea.quantitat,
                    concepte="Linea rectificativa factura %s" % factura.numero,
                    preu_unitari=linea.preu_unitari * Decimal('-1'),
                    tipus_iva=linea.tipus_iva,
                    tipus_req=linea.tipus_req,
                )
            f.assignar_numero_sequencia()
            assert factura.import_total == f.import_total * Decimal('-1'), "No cuadren les factures"
            messages.success(request, "S'ha rectificat correctament la factura")
            return redirect("facturacio:resum_facturacio_projecte", id_projecte=factura.projecte.pk)
    except Exception as e:
        messages.error(request, "S'ha produit un error al rectificar la factura: %s" % e)
        return redirect("facturacio:resum_facturacio_projecte", id_projecte=factura.projecte.pk)


@login_required
def rectificar_factura_rebuda(request, id_factura):

    factura = get_object_or_404(FacturaRebuda, id=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden()

    try:
        assert factura.numero, "No es pot rectificar un'esborrall de factura"
        assert factura.pertany_a_trimestre_obert(), "No es pot rectificar una factura en altres trimestres"
        with transaction.atomic():
            f = FacturaRebuda.objects.create(
                projecte=factura.projecte,
                proveidor=factura.proveidor,
                client=factura.client,
                data=factura.data,
                numero=factura.numero,
                irpf_total=factura.irpf_total,
                tipus_irpf=factura.tipus_irpf,
            )
            for linea in factura.liniafactura_set.all():
                LiniaFactura.objects.create(
                    factura=f,
                    quantitat=linea.quantitat,
                    concepte="Linea rectificativa factura %s" % factura.numero,
                    preu_unitari=linea.preu_unitari * Decimal('-1'),
                    tipus_iva=linea.tipus_iva,
                )
            f.save()
            assert factura.import_total == f.import_total * Decimal('-1'), "No cuadren les factures"
            messages.success(request, "S'ha rectificat correctament la factura")
            return redirect("facturacio:resum_facturacio_projecte", id_projecte=factura.projecte.pk)
    except Exception as e:
        messages.error(request, "S'ha produit un error al rectificar la factura: %s" % e)
        return redirect("facturacio:resum_facturacio_projecte", id_projecte=factura.projecte.pk)


@login_required
@user_passes_test(is_user_responsable_facturacio)
def llistat_aportacions_trimestre_projectes(request):

    title = u"Llistat aportacions per trimestre"
    descripcio = u"Tria un trimestre per coneixer les aportacions dels projectes"

    aportacions = []
    
    form = FormulariFiltrarLlistatAportacionsTrimestre(request.GET) if 'trimestre' in request.GET \
           else FormulariFiltrarLlistatAportacionsTrimestre()
    
    if form.is_valid():
        trimestre = form.cleaned_data['trimestre']
        for projecte in ProjecteAutoocupat.objects.all().order_by("compte_ces_assignat"):
            try:
                quota_trimestral = QuotaTrimestral.objects.get(projecte=projecte,
                                                               trimestre=trimestre)
                moviment_iva = [moviment for moviment in Moviment.objects.filter(
                    projecte=projecte) if "IVA" in moviment.concepte and trimestre.nom in moviment.concepte][0]
                aportacions += [
                    {
                        "coop": projecte.compte_ces_assignat,
                        "nom_i_cognoms": projecte.membres_de_referencia.all()[0].nom_sencer_sense_email,
                        "rao_social": projecte.cooperativa_assignada.nom_fiscal,
                        "aportacio_cic": str(quota_trimestral.import_quota),
                        "aportacio_iva": str(moviment_iva.quantitat),
                    }
                ]
            except Exception as e:
                continue

    return render(request,
                  "facturacio/llistat_aportacions_trimestre_projectes.html",
                  {
                      "title": title,
                      "descripcio": descripcio,
                      "form": form,
                      "aportacions": aportacions,
                  })


@login_required
@user_passes_test(is_user_responsable_facturacio)
def exportar_projectes_per_comarca(request):

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="gci-projectes-comarca.csv"'

    csv_data = []

    header = [
        "coop",
        "nom_i_cognoms",
        "telefon",
        "email",
        "comarca",
        "rao_social",
    ]

    csv_data += [header]

    for projecte in ProjecteAutoocupat.objects.all():

        try:
            csv_data += [
                [
                    projecte.compte_ces_assignat,
                    projecte.membres_de_referencia.all()[0].nom_sencer_sense_email,
                    projecte.telefon,
                    projecte.email,
                    projecte.adreces.all()[0].comarca,
                    projecte.cooperativa_assignada.nom_fiscal,
                ]
            ]
        except:
            continue
        
    t = loader.get_template("facturacio/export_factures_csv.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    return response


@login_required
@user_passes_test(is_user_responsable_facturacio)
def exportar_projectes_per_liquidacio(request):

    trimestre_actual = trobar_primer_trimestre_obert()

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="gci-projectes-liquidacions.csv"'

    csv_data = []

    header = [
        "coop",
        "nom_i_cognoms",
        "telefon",
        "email",
        "saldo_moviments",
        "estat_liquidacio",
        "rao_social",
    ]

    csv_data += [header]

    for projecte in ProjecteAutoocupat.objects.all():

        try:
            liquidacio = trobar_o_crear_liquidaciotrimestre(projecte, trimestre_actual)
            balanc = trobar_balanc_actual(projecte)
            balanc_trimestre_a_tancar = balanc['balanc_trimestre_a_tancar']

            csv_data += [
                [
                    projecte.compte_ces_assignat,
                    projecte.membres_de_referencia.all()[0].nom_sencer_sense_email,
                    projecte.telefon,
                    projecte.email,
                    str(balanc_trimestre_a_tancar),
                    liquidacio.estat,
                    projecte.cooperativa_assignada.nom_fiscal,
                ]
            ]
        except:
            continue
        
    t = loader.get_template("facturacio/export_factures_csv.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    return response


@login_required
@user_passes_test(is_user_responsable_facturacio)
def derivar_exportacio_aportacions_csv(request):

    title = u"Exportar aportacions trimestre a CSV"
    descripcio = u"Si us plau, selecciona el trimestre a considerar. Solament pots exportar aportacions de trimestres tancats"

    if request.method == "POST":
        form = ExportFeesCsvForm(request.POST)
        if form.is_valid():
            trimestre = form.cleaned_data['trimestre']
            return redirect('facturacio:exportar_aportacions_csv', trimestre=trimestre.nom)

    else:
        form = ExportFeesCsvForm()

    return render(request,
                  'facturacio/exportar_aportacions_csv.html',
                  {
                      "title": title,
                      "descripcio": descripcio,
                      "form": form,
                  })


@login_required
@user_passes_test(is_user_responsable_facturacio)
def exportar_aportacions_csv(request, trimestre=None):

    if not trimestre:
        return HttpResponseBadRequest

    trimestre_obj = Trimestre.objects.get(nom=trimestre)
    
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="gci-aportacions-trimestre-%s.csv"' % (trimestre)

    csv_data = []

    header = [
        "coop",
        "nom_i_cognoms",
        "rao_social",
        "aportacio_cic",
        "aportacio_iva",
    ]

    csv_data += [header]
    
    for projecte in ProjecteAutoocupat.objects.all():
        try:
            quota_trimestral = QuotaTrimestral.objects.get(projecte=projecte, trimestre=trimestre_obj)
            moviment_iva = [moviment for moviment in Moviment.objects.filter(projecte=projecte) if \
                            "IVA" in moviment.concepte and trimestre in moviment.concepte][0]

            csv_data += [
                [
                    projecte.compte_ces_assignat,
                    projecte.membres_de_referencia.all()[0].nom_sencer_sense_email,
                    projecte.cooperativa_assignada.nom_fiscal,
                    str(quota_trimestral.import_quota),
                    str(moviment_iva.quantitat),
                ]
            ]
        except:
            continue

    t = loader.get_template("facturacio/export_factures_csv.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    return response
    
    
@login_required
@user_passes_test(is_user_responsable_facturacio)
def derivar_exportacio_factures_csv(request):

    title = u"Exportar factures trimestre a CSV"
    descripcio = u"Si us plau, selecciona el trimestre i el tipus de factura a considerar. Solament pots exportar factures de trimestres tancats" 

    if request.method == "POST":
        form = ExportInvoicesCsvForm(request.POST)
        if form.is_valid():
            trimestre = form.cleaned_data['trimestre']
            tipus_factures = form.cleaned_data['tipus_factures']
            if tipus_factures == 'emeses':
                return redirect('facturacio:exportar_factures_emeses_csv', trimestre=trimestre.nom)
            elif tipus_factures == 'rebudes':
                return redirect('facturacio:exportar_factures_rebudes_csv', trimestre=trimestre.nom)
            else:
                messages.error(request, "On vols accedir?")
                return redirect('dashboard_gestio_economica')
    else:
        form = ExportInvoicesCsvForm()

    return render(request,
                  "facturacio/exportar_factures_csv.html",
                  {
                      "title": title,
                      "descripcio": descripcio,
                      "form": form,
                  })


@login_required
@user_passes_test(is_user_responsable_facturacio)
def exportar_factures_emeses_csv(request, trimestre=None):

    if not trimestre:
        return HttpResponseBadRequest

    trimestre_obj = Trimestre.objects.get(nom=trimestre)
    
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="gci-factures_emeses.csv"'

    fes = FacturaEmesa.objects.exclude(numero=None).filter(data__gte=trimestre_obj.data_inici)\
          .filter(data__lte=trimestre_obj.data_final)

    csv_data = []
    
    header = [
        "num_socia",
        "cif_client",
        "cif_proveidor",
        "nom_client",
        "nom_proveidor",
        "num_factura",
        "data",
        "base_imponible",
        "tipus_iva",
        "iva_parcial",
        "perc_req",
        "req_total",
        "total_factura",
        "cobrar_por",
    ]

    csv_data += [header]
    for fe in fes:
        iva_dicts = [{
            "base_total": str(sum([l.base_total for l in fe.liniafactura_set.filter(tipus_iva=tipus)])),
            "tipus": tipus,
            "iva_total": str(sum([l.iva_total for l in fe.liniafactura_set.filter(tipus_iva=tipus)]))
            } for tipus in [LiniaFactura.IVA_0, LiniaFactura.IVA_4, LiniaFactura.IVA_10, LiniaFactura.IVA_21]]
        try:
            perc_req = fe.req_total * 100 / fe.base_total
        except:
            perc_req = 0

        for iva_d in iva_dicts:
            csv_data += [
                [
                    fe.projecte.compte_ces_assignat,
                    fe.client.nif,
                    fe.proveidor.nif,
                    fe.client.nom_fiscal,
                    fe.proveidor.nom_fiscal,
                    fe.numero,
                    str(fe.data),
                    iva_d["base_total"],
                    iva_d["tipus"],
                    iva_d["iva_total"],
                    str(perc_req),
                    str(fe.req_total),
                    str(fe.import_total),
                    fe.forma_de_pagament,
                ]
            ]
            
    t = loader.get_template("facturacio/export_factures_csv.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    return response


@login_required
@user_passes_test(is_user_responsable_facturacio)
def exportar_factures_rebudes_csv(request, trimestre=None):

    if not trimestre:
        return HttpResponseBadRequest

    trimestre_obj = Trimestre.objects.get(nom=trimestre)
    
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="gci-factures_rebudes.csv"'

    frs = FacturaRebuda.objects.filter(estat=FacturaRebuda.ESTAT_ACCEPTADA)\
          .filter(data__gte=trimestre_obj.data_inici).filter(data__lte=trimestre_obj.data_final)

    csv_data = []
    
    header = [
        "num_socia",
        "cif_client",
        "cif_proveidor",
        "nom_client",
        "nom_proveidor",
        "num_factura",
        "data",
        "base_imponible",
        "tipus_iva",
        "iva_parcial",
        "perc_irpf",
        "irpf",
        "total_factura",
    ]

    csv_data += [header]
    for fr in frs:
        iva_dicts = [{
            "base_total": str(sum([l.base_total for l in fr.liniafactura_set.filter(tipus_iva=tipus)])),
            "tipus": tipus,
            "iva_total": str(sum([l.iva_total for l in fr.liniafactura_set.filter(tipus_iva=tipus)]))
            } for tipus in [LiniaFactura.IVA_0, LiniaFactura.IVA_4, LiniaFactura.IVA_10, LiniaFactura.IVA_21]]
        for iva_d in iva_dicts:
            csv_data += [
                [
                    fr.projecte.compte_ces_assignat,
                    fr.client.nif,
                    fr.proveidor.nif,
                    fr.client.nom_fiscal,
                    fr.proveidor.nom_fiscal,
                    fr.numero,
                    str(fr.data),
                    iva_d["base_total"],
                    iva_d["tipus"],
                    iva_d["iva_total"],
                    str(fr.tipus_irpf),
                    str(fr.irpf_total),
                    str(fr.import_total),
                ]
            ]
            
    t = loader.get_template("facturacio/export_factures_csv.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    return response


@login_required
@user_passes_test(is_user_responsable_bancari)
def conciliacio_manual(request):

    moviments_extracte = MovimentExtracteBancari.objects.filter(estat=MovimentExtracteBancari.ESTAT_PENDENT)


    coincidencies = []

    return render(request,
                  'facturacio/conciliacio_manual.html',
                  dict(
                      moviments_extracte=moviments_extracte,
                      coincidencies=coincidencies,
                  ))


@login_required
@user_passes_test(is_user_responsable_bancari)
def conciliacio_manual_factura(request, numero_factura):

    moviments_extracte = MovimentExtracteBancari.objects.filter(numero_factura=numero_factura)
    try:
        factura = FacturaEmesa.objects.get(numero=numero_factura)
    except FacturaEmesa.DoesNotExist:
        factura = None

    return render(request,
                  'facturacio/conciliacio_manual_factura.html',
                  dict(
                      numero_factura=numero_factura,
                      moviments_extracte=moviments_extracte,
                      factura=factura,
                  ))


@login_required
@user_passes_test(is_user_responsable_bancari)
def conciliacio_manual_projecte(request, compte_projecte):

    moviments_extracte = MovimentExtracteBancari.objects.filter(compte_projecte=compte_projecte)

    try:
        projecte = ProjecteAutoocupat.objects.get(compte_ces_assignat=compte_projecte)
        factures = FacturaEmesa.objects.filter(projecte=projecte)
    except ProjecteAutoocupat.DoesNotExist:
        projecte = None
        factures = None


    return render(request,
                  'facturacio/conciliacio_manual_projecte.html',
                  dict(
                      compte_projecte=compte_projecte,
                      moviments_extracte=moviments_extracte,
                      projecte=projecte,
                      factures=factures,
                  ))


@login_required
@user_passes_test(is_user_responsable_bancari)
def conciliacio_automatica_factures(request):

    def comparar_moviment_extracte_amb_factura(moviment_extracte, factura):

        WARNING = 'warning'
        ERROR = 'error'
        OK = 'ok'

        # preferences
        # TODO it would be nice if these were configurable by the end user
        DIFERENCIA_IMPORT_TOLERABLE = Decimal('1.00')
        DIFERENCIA_DATA_VENCIMENT_TOLERABLE = datetime.timedelta(days=45)
        DIFERENCIA_DATA_EMISSIO_TOLERABLE = datetime.timedelta(days=7)
        DIFERENCIA_PAGAT_MOLT_TARD_TOLERABLE = datetime.timedelta(days=15)

        messages = []

        if moviment_extracte.compte_projecte != factura.projecte.compte_ces_assignat:
            messages.append(('error', u"No coincideix el compte COOP"))

        diferencia_import = abs(moviment_extracte.quantitat + factura.import_total)
        if diferencia_import:
            if diferencia_import <= DIFERENCIA_IMPORT_TOLERABLE:
                tipus = WARNING
            else:
                tipus = ERROR
            messages.append((tipus, u"Diferència de %.2f €" % diferencia_import))

        if factura.data_venciment and (factura.data_venciment > moviment_extracte.data):
            diferencia_dates = factura.data_venciment - moviment_extracte.data
            if diferencia_dates > DIFERENCIA_DATA_VENCIMENT_TOLERABLE:
                tipus = ERROR
            else:
                tipus = WARNING
            messages.append((tipus, u"Cobrada %d dies abans del venciment" % diferencia_dates.days))
        elif factura.data_venciment:
            diferencia_dates = moviment_extracte.data - factura.data_venciment
            if diferencia_dates > DIFERENCIA_PAGAT_MOLT_TARD_TOLERABLE:
                messages.append((WARNING, u"Cobrada %d dies després del venciment" % diferencia_dates.days))

        if factura.data > moviment_extracte.data:
            diferencia_dates = factura.data - moviment_extracte.data
            if diferencia_dates > DIFERENCIA_DATA_EMISSIO_TOLERABLE:
                tipus = ERROR
            else:
                tipus = WARNING
            messages.append((tipus, u"Cobrada %d dies abans de la data d'emissió" % diferencia_dates.days))

        if factura.estat != FacturaEmesa.ESTAT_EMESA:
            messages.append((ERROR, u"Aquesta factura ja està cobrada"))

        if factura.forma_de_pagament != Factura.FORMA_PAGAMENT_TRANSFERENCIA:
            messages.append((WARNING, u"Aquesta factura no s'esperava per transferència"))

        if not messages:
            messages.append((OK, u"Sembla que tot encaixa"))

        return messages

    coincidencies = []

    # moviments d'extractes bancaris que encara estan pendents
    # de "conciliar" i que fan referència a alguna factura
    moviments_extracte_pendents = MovimentExtracteBancari.objects\
        .filter(estat=MovimentExtracteBancari.ESTAT_PENDENT)\
        .filter(numero_factura__contains='/')

    for moviment_extracte in moviments_extracte_pendents:
        try:
            factura = FacturaEmesa.objects.get(numero=moviment_extracte.numero_factura)
            observacions = comparar_moviment_extracte_amb_factura(moviment_extracte, factura)
            coincidencies.append(dict(
                moviment_extracte=moviment_extracte,
                factura=factura,
                observacions=observacions,
                observacions_compactes='\n'.join(['%s: %s' % o for o in observacions])
            ))
        except FacturaEmesa.DoesNotExist:
            pass

    form = FormConciliacioAutomaticaMovimentExtracteBancariFactura()

    return render(request,
                  'facturacio/conciliacio_automatica_factures.html',
                  dict(
                      coincidencies=coincidencies,
                      form=form,
                  ))


@login_required
@user_passes_test(is_user_responsable_bancari)
def conciliacio_automatica_moviments_simples(request):

    coincidencies = []

    # moviments d'extractes bancaris que encara estan pendents
    # i que considerem "simples" (aquesta és la definició, en forma de queryset:
    moviments_extracte_pendents = MovimentExtracteBancari.objects\
        .filter(estat=MovimentExtracteBancari.ESTAT_PENDENT)\
        .filter(numero_factura='')\
        .filter(tipus='aportacio')

    for moviment_extracte in moviments_extracte_pendents:
        try:
            projecte = ProjecteAutoocupat.objects.get(compte_ces_assignat=moviment_extracte.compte_projecte)
            # amago el balanços pq pot confondre si hi ha més d'un moviment del mateix projecte
            # caldria agrupar i ajax-play per que tingui sentit una cosa així
            # balanc_actual = trobar_balanc_actual(projecte)['balanc_actual']
            # balanc_final = balanc_actual + moviment_extracte.quantitat
            coincidencies.append(dict(
                moviment_extracte=moviment_extracte,
                projecte=projecte,
                # balanc_actual=balanc_actual,
                # balanc_final=balanc_final,
            ))
        except ProjecteAutoocupat.DoesNotExist:
            pass

    return render(request,
                  'facturacio/conciliacio_automatica_moviments_simples.html',
                  dict(
                      coincidencies=coincidencies,
                  ))


@transaction.atomic  # django docs use the decorator along with the "with transaction.atomic()" construct
@login_required
@user_passes_test(is_user_responsable_bancari)
def executar_conciliacio_automatica_factura(request):

    ok = False
    moviment_extracte_id = -1
    report = []

    if request.method != 'POST':
        return HttpResponseBadRequest(u"Què mireu? :)")

    form = FormConciliacioAutomaticaMovimentExtracteBancariFactura(data=request.POST)

    try:
        if form.is_valid():
            with transaction.atomic():

                factura_id = form.cleaned_data['factura_id']
                moviment_extracte_id = form.cleaned_data['moviment_extracte_id']
                comentaris = form.cleaned_data['comentaris']
                conciliacio_automatica_observacions = form.cleaned_data['conciliacio_automatica_observacions']

                factura = FacturaEmesa.objects.get(pk=factura_id)
                if factura.estat == FacturaEmesa.ESTAT_COBRADA:
                    raise Exception(u"La factura %s ja estava marcada com COBRADA." % factura.numero)

                moviment_extracte = MovimentExtracteBancari.objects.get(pk=moviment_extracte_id)
                if moviment_extracte.estat != MovimentExtracteBancari.ESTAT_PENDENT:
                    raise Exception(u"El moviment de l'extracte bancari %d no està marcat com PENDENT." % moviment_extracte.id)

                data_moviment = now().date()  # TODO GE ha de decidir quina data fem servir
                # data_moviment = moviment_extracte.data  # TODO Atenció! no fabriquem moviments a un trimestre tancat!
                diferencia = moviment_extracte.quantitat + factura.import_total

                concepte_principal = u"Ingrés a compte factura %s" % factura.numero
                if diferencia != Decimal('0.00'):
                    concepte_principal += u" (diferència de %+.2f€)" % diferencia

                # Moviments negatius a favor del projecte.
                quantitat = factura.import_total * Decimal('-1')
                
                moviment_balanc_principal = Moviment.objects.create(
                    projecte_id=factura.projecte_id,
                    cooperativa_id=factura.proveidor_id,
                    empresa_id=factura.client_id,
                    quantitat=quantitat,
                    data=data_moviment,
                    concepte=concepte_principal,
                    created_by=request.user,
                    updated_by=request.user
                )

                report.append(u"Nova entrada al balanç del projecte %s: %s." % (
                    factura.projecte.compte_ces_assignat,
                    moviment_balanc_principal.concepte))

                if diferencia != Decimal('0.00'):
                    moviment_balanc_ajust = Moviment.objects.create(
                        projecte_id=factura.projecte_id,
                        cooperativa_id=factura.proveidor_id,
                        empresa_id=factura.client_id,
                        quantitat=diferencia,
                        data=data_moviment,
                        concepte=u"Ajust diferència ingrés a compte factura %s" % factura.numero,
                        created_by=request.user,
                        updated_by=request.user
                    )

                    report.append(u"Nova entrada al balanç del projecte %s: "
                                  u"%s." % (factura.projecte.compte_ces_assignat, moviment_balanc_ajust.concepte))
                else:
                    moviment_balanc_ajust = None

                ConciliacioMovimentExtracteBancariFactura.objects.create(
                    factura=factura,
                    moviment_extracte=moviment_extracte,
                    comentaris=comentaris,
                    conciliacio_automatica_observacions=conciliacio_automatica_observacions,
                    conciliacio_automatica=True,
                    moviment_balanc=moviment_balanc_principal,
                    moviment_balanc_ajust=moviment_balanc_ajust,
                    created_by=request.user,
                    updated_by=request.user
                )

                report.append(u"Nova entrada al registre de conciliacions.")

                factura.estat = FacturaEmesa.ESTAT_COBRADA
                factura.save(actualitzar_dades=False)
                report.append(u"Factura %s marcada com COBRADA" % factura.numero)

                moviment_extracte.estat = MovimentExtracteBancari.ESTAT_CONCILIAT
                moviment_extracte.save()
                ok = True

    except Exception as e:
        report.append(u"ERROR — no s'ha pogut executar la conciliació."
                      u" No s'ha modificat RES a la base de dades."
                      u" Causa: %s" % e)

    result = json.dumps(dict(
        moviment_extracte_id=moviment_extracte_id,
        result='ok' if ok else 'nok',
        report=report
    ))

    return HttpResponse(result, content_type='application/json')


@transaction.atomic  # django docs use the decorator along with the "with transaction.atomic()" construct
@login_required
@user_passes_test(is_user_responsable_bancari)
def descartar_moviment_extracte_bancari(request):

    ok = False
    moviment_extracte_id = -1
    report = []

    if request.method != 'POST':
        return HttpResponseBadRequest(u"Què mireu? :)")

    form = FormDescartarMovimentExtracteBancari(data=request.POST)

    try:
        if form.is_valid():
            moviment_extracte_id = form.cleaned_data['moviment_extracte_id']
            moviment_extracte = MovimentExtracteBancari.objects.get(pk=moviment_extracte_id)
            if moviment_extracte.estat != MovimentExtracteBancari.ESTAT_PENDENT:
                raise Exception(u"El moviment de l'extracte bancari %d no està marcat com PENDENT." % moviment_extracte.id)

            moviment_extracte.estat = MovimentExtracteBancari.ESTAT_DESCARTAT
            moviment_extracte.save()
            report.append(u"S'ha marcat el moviment de l'extracte com a DESCARTAT")
            ok = True

    except Exception as e:
        report.append(u"ERROR — no s'ha pogut descartar el moviment."
                      u" Causa: %s" % e)

    result = json.dumps(dict(
        moviment_extracte_id=moviment_extracte_id,
        result='ok' if ok else 'nok',
        report=report
    ))

    return HttpResponse(result, content_type='application/json')


@transaction.atomic  # django docs use the decorator along with the "with transaction.atomic()" construct
@login_required
@user_passes_test(is_user_responsable_bancari)
def executar_conciliacio_moviments_simples(request):

    if request.method != 'POST':
        return HttpResponseBadRequest(u"Què mireu? :)")

    moviments_a_conciliar = request.POST.get('moviments_a_conciliar', '').split(',')

    data_moviment = now().date()  # TODO GE ha de decidir quina data fem servir
    # data_moviment = moviment_extracte.data  # TODO Atenció! no fabriquem moviments a un trimestre tancat!

    try:
        with transaction.atomic():

            for moviment_extracte_id in moviments_a_conciliar:

                moviment_extracte = MovimentExtracteBancari.objects.get(pk=moviment_extracte_id)
                concepte = u"Ingrés a compte: %s" % moviment_extracte.concepte

                if moviment_extracte.estat != MovimentExtracteBancari.ESTAT_PENDENT:
                    raise Exception(u"El moviment de l'extracte bancari %d no està marcat "
                                    u"com PENDENT." % moviment_extracte.id)

                projecte = ProjecteAutoocupat.objects.get(compte_ces_assignat=moviment_extracte.compte_projecte)

                moviment_balanc = Moviment.objects.create(
                    projecte_id=projecte.id,
                    cooperativa_id=projecte.cooperativa_assignada.id,
                    quantitat=moviment_extracte.quantitat,
                    data=data_moviment,
                    concepte=concepte,
                    created_by=request.user,
                    updated_by=request.user
                )

                ConciliacioMovimentExtracteBancari.objects.create(
                    moviment_extracte=moviment_extracte,
                    moviment_balanc=moviment_balanc,
                    created_by=request.user,
                    updated_by=request.user
                )

                moviment_extracte.estat = MovimentExtracteBancari.ESTAT_CONCILIAT
                moviment_extracte.save()

    except Exception as e:

        messages.error(request, u"ERROR — ha fallat la conciliació d'algun dels moviments."
                                u" No s'ha modificat RES a la base de dades."
                                u" Causa: %s" % e)
        return redirect('facturacio:conciliacio_automatica_moviments_simples')

    n = len(moviments_a_conciliar)
    if n > 1:
        messages.success(request, u"S'han conciliat %d moviments" % n)
    else:
        messages.success(request, u"S'ha conciliat un moviment")

    return redirect('dashboard_gestio_economica')


@login_required
@user_passes_test(is_user_responsable_bancari)
def llistat_moviments_extracte_descartats(request, tots=False):
    """
    View that renders a list of discarded bank statements
    ordered by date
    """
    title = u"Moviments importats que s'han descartat"
    queryset = MovimentExtracteBancari.objects.filter(estat=MovimentExtracteBancari.ESTAT_DESCARTAT)\
                   .order_by('-data')[:20]

    if tots:
        queryset = MovimentExtracteBancari.objects.filter(estat=MovimentExtracteBancari.ESTAT_DESCARTAT)\
            .order_by('data')

    return render(request,
                  'facturacio/llistat_moviments_extracte_descartats.html',
                  { 'title': title,
                    'queryset': queryset,
                    'tots': tots
                })


@login_required
@user_passes_test(is_user_responsable_bancari)
@transaction.atomic
def exportar_extracte_bancari(request, id=None):
    """
    Exports a bank statement in csv format.
    """

    if id:

        d = now().date().isoformat()

        fitxer = get_object_or_404(ExtracteBancari, id=id)

        nom_final = fitxer.nom_arxiu
        if nom_final.lower().endswith('.csv'):
            nom_final = nom_final[0:-4]  # treiem l'extensió
        nom_final += '-exportat_%s' % d

        header = [
            u"Número",
            u"Tipus",
            u"Data",
            u"Concepte",
            u"Quantitat",
            u"Compte",
            u"Projecte",
            u"Número factura",
            u"Comentaris",
            u"Estat",
            u"Comentaris conciliació"
        ]

        csv_data = [header]

        for m in fitxer.movimentextractebancari_set.all().order_by('numero_moviment'):

            conciliacions = m.conciliaciomovimentextractebancari_set.all()
            if conciliacions.exists():
                assert(conciliacions.count() == 1)
                comentaris_conciliacio = conciliacions[0].comentaris
            else:
                comentaris_conciliacio = ''

            linia = [
                m.numero_moviment,
                m.tipus,
                m.data.isoformat(),
                m.concepte,
                str(m.quantitat),  # TODO quin format expera per defecte el LibreOffice de GE?
                m.compte,
                m.compte_projecte,
                m.numero_factura,
                m.comentaris,
                m.estat,
                comentaris_conciliacio,
            ]
            linia = ['' if camp is None else camp for camp in linia]
            csv_data.append(linia)

            # update MovimentExtracteBancari.estat
            # it is not a problem if some records are updated and then the downloaded file
            # gets corrupted because of a network issue, or if the user somehow loses the
            # retrieved file. The records will be exported again when she retries, and the
            # "last updated" date on the listing is ignoring "pendent" and "delegat" statuses.
            if m.estat == MovimentExtracteBancari.ESTAT_PENDENT:
                m.estat = MovimentExtracteBancari.ESTAT_DELEGAT
                m.save()

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="%s.csv"' % nom_final

        # TODO I guess we should go for unicodecsv
        # see https://docs.djangoproject.com/en/1.8/howto/outputting-csv/
        # writer = csv.writer(response)
        # for row in csv_data:
        #     writer.writerow(row)
        # copying the altasocies.views.exportar_a_csv method in the meantime :)
        # TODO: do we need a template to create a CSV file?
        t = loader.get_template("facturacio/export_extractebancari.txt")
        c = Context({'data': csv_data})
        response.write(t.render(c))
        return response

    # show a list with recent MovimentExtracteBancari
    title = u"Descarregar un extracte bancari"
    items = []

    fitxers = ExtracteBancari.objects.all().order_by('-data_importacio')[0:30]  # TODO create UI instead of hardcoding
    for f in fitxers:
        total = f.movimentextractebancari_set.count()
        pendents = f.movimentextractebancari_set.filter(estat=MovimentExtracteBancari.ESTAT_PENDENT).count()
        ultima_modificacio = f.movimentextractebancari_set \
            .exclude(estat__in=(MovimentExtracteBancari.ESTAT_DELEGAT, MovimentExtracteBancari.ESTAT_PENDENT)) \
            .aggregate(Max('updated_at'))['updated_at__max'] or u"sense modificacions"

        items.append(dict(
            fitxer=f,
            total=total,
            pendents=pendents,
            ultima_modificacio=ultima_modificacio
        ))

    return render(request, 'facturacio/exportar_extracte_bancari.html',
                  dict(
                      title=title,
                      items=items,
                  ))


@login_required
@user_passes_test(is_user_responsable_bancari)
def importar_extracte_bancari(request):
    """
    View that imports a bank statement in csv format.
    """

    col_numero_moviment = 0
    col_tipus = 1
    col_data = 2
    col_concepte = 3
    col_quantitat = 4
    col_compte = 5
    col_compte_projecte = 6
    col_numero_factura = 7
    col_comentaris = 8
    col_estat = 9

    dateformat_dmy = re.compile(r'^\d{2}[/-]\d{2}[/-]\d{4}$')

    def string_to_date(s):
        if dateformat_dmy.match(s):
            s = "-".join([s[6:], s[3:5], s[0:2]])
        return datetime.datetime.strptime(s, '%Y-%m-%d')

    def string_to_decimal(s):
        s = s.replace(',', '')
        try:
            if len(s.split('.')[1]) != 2:
                raise
            v = Decimal(s)
        except Exception:
            raise Exception(u"Els imports han de tenir 2 decimals separats per punt")
        return v

    def normalitzar_tipus(s):
        s = s.lower()
        # if type(s) == 'unicode':  # TODO it seems we are getting str instead of unicode even though the file is UTF8
        #     if s == u'aportació':
        #         s = u"aportacio"
        if s == 'aportació':
            s = u"aportacio"

        valid_values = [u"aportacio", u"factura", u"reintegrament"]
        if s not in valid_values:
            raise Exception(u"Tipus \"%s\" no és vàlid; ha de ser un de %s" % (s, str(valid_values)))

        return s

    title = u"Carregar un extracte bancari"
    
    if request.method == 'POST':
        form = FormulariImportExtracteBancari(request.POST, request.FILES)

        if form.is_valid():

            nl = 1  # line number; stored on a local variable so we can use it in the 'except' clause

            try:
                with transaction.atomic():

                    extracte = ExtracteBancari.objects.create(
                        usuaria_importadora=request.user,
                        nom_arxiu=request.FILES['file'].name)

                    # TODO set up the reader in a way that yields unicode instead of str
                    # reader = csv.reader(codecs.EncodedFile(request.FILES['file'], 'utf-8'))
                    reader = csv.reader(request.FILES['file'])
                    reader.next()  # skip header row

                    for row in reader:
                        nl += 1

                        numero_moviment = row[col_numero_moviment].strip()
                        tipus = normalitzar_tipus(row[col_tipus].strip())
                        data = string_to_date(row[col_data].strip())
                        concepte = row[col_concepte].strip()
                        quantitat = string_to_decimal(row[col_quantitat].strip())
                        compte = row[col_compte].strip()
                        compte_projecte = row[col_compte_projecte].strip()
                        numero_factura = row[col_numero_factura].strip()
                        comentaris = row[col_comentaris].strip()
                        estat = row[col_estat].strip()

                        o = MovimentExtracteBancari(
                                numero_moviment=numero_moviment,
                                extracte_bancari=extracte,
                                compte=compte,
                                tipus=tipus,
                                data=data,
                                concepte=concepte,
                                quantitat=quantitat,
                                compte_projecte=compte_projecte,
                                numero_factura=numero_factura,
                                comentaris=comentaris,
                                estat=(estat or MovimentExtracteBancari.ESTAT_PENDENT))
                        o.full_clean()
                        o.save()

                messages.success(request, u"S'ha carregat el fitxer: %s línies importades" % (nl - 1))

            except Exception as e:
                messages.error(request, u"No es pot carregar el fitxer: error a la línia %d: %s, %s" %
                               (nl, type(e).__name__, e.args))

            return redirect('facturacio:importar_extracte_bancari')
    
    else:
        form = FormulariImportExtracteBancari()

    return render(request, 'facturacio/importar_extracte_bancari.html',
                  {'title': title,
                   'form': form
                   })


@login_required
@user_passes_test(is_user_responsable_facturacio)
def rebutjar_empresa(request, empresa_id):
    """
    View for reject a pending revision enterprise
    """
    if empresa_id is None or not Empresa.objects.filter(id=empresa_id).exists():
        messages.error(request, u"No juguis amb les URLs, si us plau :)")
        return redirect('inici')
    empresa = Empresa.objects.get(id=empresa_id)
    empresa.delete()
    messages.success(request, u"L'empresa s'ha eliminat correctament")
    return redirect('facturacio:validacio_empreses')


@login_required
@user_passes_test(is_user_responsable_facturacio)
def validar_empresa(request, empresa_id):
    """
    View for validate a pending revision enterprise
    """
    if empresa_id is None or not Empresa.objects.filter(id=empresa_id).exists():
        messages.error(request, u"No juguis amb les URLs, si us plau :)")
        return redirect('inici')
    empresa = Empresa.objects.get(id=empresa_id)
    empresa.pendent_de_revisio = False
    empresa.vist_i_plau_data = now()
    empresa.vist_i_plau_usuaria = request.user
    empresa.save()
    messages.success(request, u"L'empresa s'ha validat correctament")
    return redirect('facturacio:validacio_empreses')


@login_required
@user_passes_test(is_user_responsable_facturacio)
def modificar_empresa(request, empresa_id):
    """
    Update custom view for Empresa instances
    """
    if empresa_id is None or not Empresa.objects.filter(id=empresa_id).exists():
        messages.error(request, u"No juguis amb les URLs, si us plau :)")
        return redirect('inici')

    empresa = Empresa.objects.get(id=empresa_id)
    form = ModificarEmpresaForm(request.POST or {
        'nif': empresa.nif,
        'nom_fiscal': empresa.nom_fiscal,
        'telefon': empresa.telefon,
        'email': empresa.email,
        'adreca_fiscal': empresa.adreca_fiscal.adreca,
        'poblacio': empresa.adreca_fiscal.poblacio,
        'codi_postal': empresa.adreca_fiscal.codi_postal,
        })
                                
    if request.method == 'POST':
        if form.is_valid():
            try:
                with transaction.atomic():
                    af = AdrecaFiscal.objects.get(id=empresa.adreca_fiscal.id)
                    af.adreca = form.cleaned_data['adreca_fiscal']
                    af.poblacio = form.cleaned_data['poblacio']
                    af.codi_postal = form.cleaned_data['codi_postal']
                    af.save()
                    empresa.nif = form.cleaned_data['nif']
                    empresa.nom_fiscal = form.cleaned_data['nom_fiscal']
                    empresa.telefon = form.cleaned_data['telefon']
                    empresa.email = form.cleaned_data['email']
                    empresa.save()
                    messages.success(request,
                                     u"Les dades de l'empresa s'han actualitzat; si tot és correcte, procediu a validar-la.")
                    return redirect('facturacio:validacio_empreses')
            except:
                messages.error(request,
                               u"Amb les dades introduides no es pot completar el proces d'actualització, reviseu-les")
                return redirect('facturacio:validacio_empreses')

    return render(request,
                  'facturacio/update_empresa_pendent_revisio.html',
                  {'form': form}
    )
                
    
class EmpresesPendingRevisionListView(ListView):
    """
    CBV listing the 'empreses' pending revision
    from a GE admin
    """
    template_name = 'facturacio/llistat_empreses_pendents_revisio.html'
    queryset = Empresa.objects.filter(pendent_de_revisio=True)
    context_object_name = 'empreses'

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_facturacio))
    def dispatch(self, *args, **kwargs):
        return super(EmpresesPendingRevisionListView, self).dispatch(*args, **kwargs)


def llistat_quotes_trimestrals(request):

    avui = now().date()

    dates_vigor = TarifaQuotaTrimestral.objects.order_by('-data_vigor').values_list('data_vigor').distinct()
    dates_vigor = [d[0] for d in dates_vigor]

    taules = []
    hem_passat_actual = 0

    for data_vigor in dates_vigor:

        taula = TarifaQuotaTrimestral.objects.filter(data_vigor=data_vigor).order_by('base_imposable_minima')

        if data_vigor > avui:
            temps = 'futur'
        else:

            if data_vigor <= avui and hem_passat_actual == 0:
                temps = 'present'
            else:
                temps = 'passat'

            hem_passat_actual += 1

        taules.append(dict(data_vigor=data_vigor,
                           taula=taula,
                           temps=temps))

        if hem_passat_actual > 1:
            break

    return render(
        request,
        'facturacio/llistat_quotes_trimestrals.html',
        dict(taules=taules)
    )


@login_required
def resum_liquidacio_trimestre(request):

    projectes = trobar_projectes_usuari(request.user)
    if not projectes:
        messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
        return redirect('inici')

    projecte = projectes[0]

    trimestre = trobar_primer_trimestre_obert()

    resum_facturacio = trobar_base_imposable_i_iva(projecte, trimestre)

    import_quota_trimestral = trobar_quota_trimestral(trimestre.data_limit_tancament,
                                                      resum_facturacio['rebudes_base'],
                                                      resum_facturacio['emeses_base'])

    bal = trobar_balanc_actual(projecte)

    assert trimestre == bal['trimestre_a_tancar']

    balanc_final = bal['balanc_actual'] + import_quota_trimestral

    return render(
        request,
        'facturacio/resum_liquidacio_trimestre.html',
        dict(
            projecte=projecte,
            trimestre=trimestre,

            emeses_count=resum_facturacio['emeses_count'],
            emeses_base=resum_facturacio['emeses_base'],
            emeses_iva=resum_facturacio['emeses_iva'],
            rebudes_count=resum_facturacio['rebudes_count'],
            rebudes_base=resum_facturacio['rebudes_base'],
            rebudes_iva=resum_facturacio['rebudes_iva'],
            base=resum_facturacio['base'],
            iva=resum_facturacio['iva'],

            import_quota_trimestral=import_quota_trimestral,

            ultim_trimestre_tancat=bal['ultim_trimestre_tancat'],
            balanc_ultim_trimestre_tancat=bal['balanc_ultim_trimestre_tancat'],
            trimestre_a_tancar=bal['trimestre_a_tancar'],
            balanc_trimestre_a_tancar=bal['balanc_trimestre_a_tancar'],
            balanc_actual=bal['balanc_actual'],
            balanc_final=balanc_final,

        )
    )


@login_required
def resum_facturacio_projecte(request, id_projecte=None):

    if id_projecte is None:
        projectes = trobar_projectes_usuari(request.user)
        if not projectes:
            messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
            return redirect('inici')

        projecte = projectes[0]

    else:
        projectes = ProjecteAutoocupat.objects.filter(pk=id_projecte)

        if projectes.count():
                projecte = projectes[0]
        else:
            messages.error(request, u"No hi ha cap projecte autoocupat amb aquest identificador.")
            return redirect('inici')

        if not request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists() and \
                projecte not in trobar_projectes_usuari(request.user):
            messages.error(request, u"No tens permís per consultar aquesta informació.")
            return redirect('inici')

    if 'periode' in request.GET:
        periode = request.GET.get('periode')
        (des_de, fins_a) = calcular_periode(periode)
    else:
        # assumim que el trimestre actual existeix ;)
        trimestre = Trimestre.trobar_trimestre(now())
        periode = trimestre.nom
        des_de = trimestre.data_inici
        fins_a = trimestre.data_final

    if des_de:

        factures_emeses = projecte.factures_emeses(des_de, fins_a)

        resum_emeses = factures_emeses.aggregate(
            Sum('base_total'),
            Sum('iva_total'),
            Sum('req_total'),
            Sum('import_total'))

        factures_emeses = factures_emeses.order_by('numero')

        factures_rebudes = projecte.factures_rebudes(des_de, fins_a)
        factures_rebudes_acceptades = factures_rebudes.filter(estat=FacturaRebuda.ESTAT_ACCEPTADA)
        factures_rebudes_pendents = factures_rebudes.filter(estat=FacturaRebuda.ESTAT_REBUDA)
        factures_rebudes_rebutjades = factures_rebudes.filter(estat=FacturaRebuda.ESTAT_REBUTJADA)

        resum_rebudes_acceptades = factures_rebudes_acceptades.aggregate(
            Sum('base_total'),
            Sum('iva_total'),
            Sum('irpf_total'),
            Sum('import_total'))

        resum_rebudes_pendents = factures_rebudes_pendents.aggregate(
            Sum('base_total'),
            Sum('iva_total'),
            Sum('irpf_total'),
            Sum('import_total'))

        factures_rebudes_acceptades = factures_rebudes_acceptades.order_by('data')
        factures_rebudes_pendents = factures_rebudes_pendents.order_by('data')
        factures_rebudes_rebutjades = factures_rebudes_rebutjades.order_by('data')

    else:
        factures_emeses = FacturaEmesa.objects.none()
        factures_rebudes_acceptades = FacturaRebuda.objects.none()
        factures_rebudes_pendents = FacturaRebuda.objects.none()
        factures_rebudes_rebutjades = FacturaRebuda.objects.none()
        resum_emeses = resum_rebudes_acceptades = resum_rebudes_pendents = None

    return render(
        request,
        'facturacio/resum_facturacio_projecte.html',
        dict(
            projecte=projecte,
            periode=periode,
            des_de=des_de,
            fins_a=fins_a,

            factures_emeses=factures_emeses,
            resum_emeses=resum_emeses,

            factures_rebudes_acceptades=factures_rebudes_acceptades,
            factures_rebudes_pendents=factures_rebudes_pendents,
            factures_rebudes_rebutjades=factures_rebudes_rebutjades,
            resum_rebudes_acceptades=resum_rebudes_acceptades,
            resum_rebudes_pendents=resum_rebudes_pendents,
        )
    )


@login_required
def veure_factura_emesa(request, id_factura):
    factura = get_object_or_404(FacturaEmesa, pk=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden(u"aquesta factura no és teva")

    # potser ho podem questionar, però de moment les proformes no es mostren; s'editen.
    if not factura.numero:
        if factura.liniafactura_set.count():
            return redirect('facturacio:confirmar_factura_emesa', id_factura=factura.pk)
        else:
            return redirect('facturacio:editar_linies_factura_emesa', id_factura=factura.pk)

    resum_iva, resum_req, resum_totals = factura.calcular_resums()

    return render(
        request,
        'facturacio/veure_factura_emesa.html',
        dict(
            f=factura,
            resum_iva=resum_iva,
            resum_req=resum_req,
            resum_totals=resum_totals,
        )
    )


@login_required
def crear_factura_emesa(request, id_client=None):
    projectes = trobar_projectes_usuari(request.user)
    if not projectes:
        messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
        return redirect('inici')

    if projectes.count() > 1:
        messages.error(request, u"No està previst que una persona pugui facturar des de més d'un projecte.")
        return redirect('inici')
    else:
        projecte = projectes[0]

    clients_habituals = projecte.clients()
    initial = {}
    if id_client:
        client_proposat = Empresa.objects.filter(pk=id_client).exclude(pendent_de_revisio=True)
        if client_proposat.count():
            initial['client'] = client_proposat[0].id
            clients_habituals |= client_proposat

    form = None

    if clients_habituals.count():

        if request.method == 'POST':

            form = FormCrearFacturaEmesa(clients=clients_habituals, initial=initial, data=request.POST)

            if form.is_valid():
                factura = form.save(commit=False)
                factura.projecte = projecte
                factura.proveidor = projecte.cooperativa_assignada
                factura.save()

                return redirect('facturacio:editar_linies_factura_emesa', id_factura=factura.pk)

        else:

            form = FormCrearFacturaEmesa(clients=clients_habituals, initial=initial)

    return render(
        request,
        'facturacio/crear_factura_emesa.html',
        dict(
            form=form,
        )
    )


@login_required
def editar_linies_factura_emesa(request, id_factura):
    factura = get_object_or_404(FacturaEmesa, pk=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden(u"aquesta factura no és teva")
    if not factura.pertany_a_trimestre_obert():
        return HttpResponseForbidden(
            u"aquesta factura no es pot editar per què pertany a un trimestre que ja està tancat")
    if factura.numero:
        return HttpResponseForbidden(u"aquesta factura ja no es pot editar")

    # TODO wouldn't it be nice to refactor all this crap out to views.py? to a custom Form or Formset?
    _extra_attrs = {'onchange': 'linia_modificada(event.target)',
                    'style': 'text-align:right;'}

    FormsetLinies = inlineformset_factory(FacturaEmesa,
                                          LiniaFactura,
                                          extra=3,
                                          fields=(
                                              'quantitat',
                                              'concepte',
                                              'preu_unitari',
                                              'tipus_iva',
                                              'tipus_req'),
                                          widgets={
                                              'quantitat': TextInput(attrs=_extra_attrs),
                                              'preu_unitari': TextInput(attrs=_extra_attrs),
                                              'tipus_iva': Select(attrs=_extra_attrs),
                                              'tipus_req': TextInput(attrs=_extra_attrs),
                                          })

    if request.method == 'POST':
        formset = FormsetLinies(data=request.POST, instance=factura)
        if formset.is_valid():
            formset.save()
            factura.save()  # força a actualitzar camps calculats

            if request.POST.get('anem_a') == 'seguent':
                return redirect('facturacio:confirmar_factura_emesa', id_factura=factura.pk)

            return redirect('facturacio:editar_linies_factura_emesa', id_factura=factura.pk)

    else:
        formset = FormsetLinies(instance=factura)

    return render(
        request,
        'facturacio/editar_linies_factura_emesa.html',
        dict(
            factura=factura,
            formset=formset
        )
    )


@login_required
def confirmar_factura_emesa(request, id_factura):
    factura = get_object_or_404(FacturaEmesa, pk=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden(u"aquesta factura no és teva")
    if not factura.pertany_a_trimestre_obert():
        return HttpResponseForbidden(u"aquesta factura pertany a un trimestre que ja està tancat")
    if factura.numero:
        return HttpResponseForbidden(u"aquesta factura ja no es pot editar")

    if request.method == 'POST':

        form = FormConfirmarFacturaEmesa(data=request.POST, instance=factura)
        if form.is_valid():
            form.save()
            factura.save()  # required to update calculated fields

            accio = request.POST.get('anem_a')
            if accio == 'enrera':
                redirect('facturacio:editar_linies_factura_emesa', id_factura=factura.pk)
            elif accio == 'aqui':
                return redirect('facturacio:confirmar_factura_emesa', id_factura=factura.pk)
            else:
                factura.assignar_numero_sequencia()
                # TODO aquí s'hauria d'obrir una finestra nova amb la factura, p.ex. per imprimir, i retornar
                # enlloc de fer això de moment portem l'usuari al resum de facturació per evitar que facin
                # "back" al navegador i es trobin modificant la factura que acaben de crear...
                # return redirect('facturacio:veure_factura_emesa', id_factura=factura.pk)
                return redirect('facturacio:resum_facturacio_projecte', id_projecte=factura.projecte.pk)

    else:

        form = FormConfirmarFacturaEmesa(instance=factura)

    resum_iva, resum_req, resum_totals = factura.calcular_resums()

    return render(
        request,
        'facturacio/confirmar_factura_emesa.html',
        dict(
            factura=factura,
            form=form,
            resum_iva=resum_iva,
            resum_req=resum_req,
            base_total=factura.base_total,
            iva_total=factura.iva_total,
            req_total=factura.req_total,
            import_total=factura.import_total
        )
    )


@login_required
def veure_factura_rebuda(request, id_factura):
    factura = get_object_or_404(FacturaRebuda, pk=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden(u"aquesta factura no és teva")

    resum_iva, resum_totals = factura.calcular_resums()

    return render(
        request,
        'facturacio/veure_factura_rebuda.html',
        dict(
            f=factura,
            resum_iva=resum_iva,
            resum_totals=resum_totals,
        )
    )


@login_required
def crear_factura_rebuda(request, id_proveidor=None):
    projectes = trobar_projectes_usuari(request.user)
    if not projectes:
        messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
        return redirect('inici')

    if projectes.count() > 1:
        messages.error(request, u"No està previst que una persona pugui facturar des de més d'un projecte.")
        return redirect('inici')
    else:
        projecte = projectes[0]

    proveidors = projecte.proveidors()
    initial = {}
    if id_proveidor:
        proveidor_proposat = Empresa.objects.filter(pk=id_proveidor).exclude(pendent_de_revisio=True)
        if proveidor_proposat.count():
            initial['proveidor'] = proveidor_proposat[0].id
            proveidors |= proveidor_proposat

    form = None

    if proveidors.count():

        if request.method == 'POST':

            form = FormCrearFacturaRebuda(proveidors=proveidors, initial=initial, data=request.POST)

            if form.is_valid():
                factura = form.save(commit=False)
                factura.projecte = projecte
                factura.client = projecte.cooperativa_assignada
                factura.save()

                return redirect('facturacio:editar_linies_factura_rebuda', id_factura=factura.pk)

        else:

            form = FormCrearFacturaRebuda(proveidors=proveidors, initial=initial)

    return render(
        request,
        'facturacio/crear_factura_rebuda.html',
        dict(
            form=form,
        )
    )


@login_required
def editar_linies_factura_rebuda(request, id_factura):
    factura = get_object_or_404(FacturaRebuda, pk=id_factura)

    if not factura.usuari_pot_editar(request.user):
        return HttpResponseForbidden(u"aquesta factura no és teva")
    if not factura.pertany_a_trimestre_obert():
        return HttpResponseForbidden(
            u"aquesta factura no es pot editar per què pertany a un trimestre que ja està tancat")

    # TODO wouldn't it be nice to refactor all this crap out to views.py? to a custom Form or Formset?
    _extra_attrs = {'onchange': 'linia_modificada(event.target)',
                    'style': 'text-align:right;'}

    FormsetLinies = inlineformset_factory(FacturaRebuda,
                                          LiniaFactura,
                                          # formset=FormsetLiniaFacturaEmesa,
                                          extra=3,
                                          fields=(
                                              'quantitat',
                                              'concepte',
                                              'preu_unitari',
                                              'tipus_iva'),
                                          widgets={
                                              'quantitat': TextInput(attrs=_extra_attrs),
                                              'concepte': TextInput(attrs=_extra_attrs),
                                              'preu_unitari': TextInput(attrs=_extra_attrs),
                                              'tipus_iva': Select(attrs=_extra_attrs),
                                          })

    if request.method == 'POST':

        form = FormEditarFacturaRebuda(data=request.POST, instance=factura)
        if form.is_valid():
            form.save()

        formset = FormsetLinies(data=request.POST, instance=factura)
        if formset.is_valid():

            formset.save()
            factura.save()  # força a actualitzar camps calculats

            if request.POST.get('anem_a') == 'seguent':
                messages.success(request, "Ja pots visualitzar la teva factura")
                return redirect('facturacio:resum_facturacio_projecte', id_projecte=factura.projecte.pk)

            return redirect('facturacio:editar_linies_factura_rebuda', id_factura=factura.pk)

    else:
        formset = FormsetLinies(instance=factura)
        form = FormEditarFacturaRebuda(instance=factura)

    return render(
        request,
        'facturacio/editar_linies_factura_rebuda.html',
        dict(
            factura=factura,
            formset=formset,
            form=form,
        )
    )


@login_required
def llistat_empreses_pao(request, kind=None):
    projectes = trobar_projectes_usuari(request.user)
    if not projectes:
        messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
        return redirect('inici')

    if projectes.count() > 1:
        messages.error(request, u"No està previst que una persona sigui membre de referència a més d'un projecte.")
        return redirect('inici')
    else:
        projecte = projectes[0]

    if not kind or kind not in ['client', 'proveidor']:
        return HttpResponseBadRequest

    if kind == 'client':
        title = u"Els teus clients"
        empreses = projecte.clients
    elif kind == 'proveidor':
        title = u"Els teus proveidors"
        empreses = projecte.proveidors

    return render(
        request,
        "facturacio/llistat_empreses_pao.html",
        {
            "title": title,
            "empreses": empreses,
        }
    )


@login_required
def dashboard_facturacio(request):
    projectes = trobar_projectes_usuari(request.user)
    if not projectes:
        messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
        return redirect('inici')

    if projectes.count() > 1:
        messages.error(request, u"No està previst que una persona sigui membre de referència a més d'un projecte.")
        return redirect('inici')
    else:
        projecte = projectes[0]

    anys_actiu = Factura.objects.filter(projecte=projecte).distinct('data__year').dates('data', 'year')
    anys_actiu = [d.year for d in anys_actiu]
    anys_actiu.sort(reverse=True)

    if not anys_actiu:
        anys_actiu = [now().year, ]

    # fabriquem una estructura de "nested dict's" amb anys, trimestres i recomptes de factures, d'aquesta manera:
    #  {
    #    2013: {'total': XXX, 'trimestres': {1: XXX, 2: XXX, 3: XXX, 4: XXX}},
    #    2014: {'total': XXX, 'trimestres': {1: XXX, 2: XXX, 3: XXX, 4: XXX}},
    #    2015: {'total': XXX, 'trimestres': {1: XXX, 2: XXX, 3: XXX, 4: XXX}}
    #  }
    # i cada XXX és un dict {'rebudes': n, 'emeses': m}

    activitat = dict()
    for a in anys_actiu:

        activitat[a] = dict(
            total=dict(emeses=0, rebudes=0),
            trimestres=dict())

        for num_t in [1, 2, 3, 4]:
            t = Trimestre.trobar_per_any_i_numero(a, num_t)
            ne = nr = 0
            if t:
                fe = FacturaEmesa.objects.filter(projecte=projecte)
                ne = fe.filter(data__gte=t.data_inici, data__lte=t.data_final).count()
                fr = FacturaRebuda.objects.filter(projecte=projecte)
                nr = fr.filter(data__gte=t.data_inici, data__lte=t.data_final).count()

            activitat[a]['total']['emeses'] += ne
            activitat[a]['total']['rebudes'] += nr
            activitat[a]['trimestres'][num_t] = dict(emeses=ne, rebudes=nr)

    avui = now().date()
    bal = trobar_balanc_actual(projecte)
    trimestre_a_tancar = bal['trimestre_a_tancar']

    ################################################################################################################
    #  TODO: find my clone in "llistat_projectes_autoocupats" and factor this out
    ################################################################################################################
    dies_avis_tancament_a_prop = settings.DIES_AVIS_A_PROP_TANCAMENT_TRIMESTRE
    en_periode_de_tancament = (trimestre_a_tancar.data_final < avui <= trimestre_a_tancar.data_limit_tancament)
    data_avis_tancament_a_prop = trimestre_a_tancar.data_final - datetime.timedelta(days=dies_avis_tancament_a_prop)
    a_prop_del_periode_de_tancament = data_avis_tancament_a_prop <= avui <= trimestre_a_tancar.data_limit_tancament
    passat_periode_de_tancament = trimestre_a_tancar.data_limit_tancament < avui
    dies_per_tancar = (trimestre_a_tancar.data_limit_tancament - avui).days
    dies_per_periode_tancament = trimestre_a_tancar.data_final - avui
    ################################################################################################################
    bi_iva = trobar_base_imposable_i_iva(projecte, trimestre_a_tancar)
    import_quota_trimestral = trobar_quota_trimestral(
        trimestre_a_tancar.data_limit_tancament,
        bi_iva['rebudes_base'],
        bi_iva['emeses_base'],
    )
    import_aportacio_iva = bi_iva['iva'] if bi_iva['iva'] > Decimal('0.00') else Decimal('0.00')
    
    return render(
        request,
        'facturacio/dashboard_facturacio_projecte.html',
        dict(
            projecte=projecte,
            activitat=activitat,
            ultim_trimestre_tancat=bal['ultim_trimestre_tancat'],
            balanc_ultim_trimestre_tancat=bal['balanc_ultim_trimestre_tancat'],
            trimestre_a_tancar=bal['trimestre_a_tancar'],
            balanc_trimestre_a_tancar=bal['balanc_trimestre_a_tancar'],
            estat_trimestre_a_tancar=bal['estat_trimestre_a_tancar'],
            estat_obert=LiquidacioTrimestre.ESTAT_OBERT,
            balanc_actual=bal['balanc_actual'],
            en_periode_de_tancament=en_periode_de_tancament,
            a_prop_del_periode_de_tancament=a_prop_del_periode_de_tancament,
            dies_per_tancar=dies_per_tancar,
            dies_per_periode_tancament=dies_per_periode_tancament.days,
            passat_periode_de_tancament=passat_periode_de_tancament,
            import_quota_trimestral=import_quota_trimestral,
            import_aportacio_iva=import_aportacio_iva,
        )
    )


@login_required
def llistat_moviments(request, id_projecte=None, tots=False):

    if id_projecte is None:
        projectes = trobar_projectes_usuari(request.user)
        if not projectes:
            messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
            return redirect('inici')

        projecte = projectes[0]

    else:
        if request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
            projectes = ProjecteAutoocupat.objects.filter(pk=id_projecte)
            if projectes.count():
                projecte = projectes[0]
            else:
                messages.error(request, u"No hi ha cap projecte autoocupat amb aquest identificador.")
                return redirect('inici')
        else:
            messages.error(request, u"No tens permís per consultar els comptes de cap projecte autoocupat.")
            return redirect('inici')

    # TODO permetre consulta de moviments de periodes anteriors
    # if 'periode' in request.GET:
    #     nom_trimestre = request.GET.get('periode')
    #     t = get_object_or_404(Trimestre, nom=nom_trimestre)

    # TODO permetre consulta de moviments de periodes anteriors
    # if 'periode' not in request.GET:
    #     assumim que el trimestre actual existeix ;)
    #     t = Trimestre.trobar_trimestre(now())

    bal = trobar_balanc_actual(projecte)

    if bal['ultim_trimestre_tancat'] is None:
        data_inicial = None
    else:
        data_inicial = bal['trimestre_a_tancar'].data_inici

    saldo = Decimal('0.00') if tots else bal['balanc_ultim_trimestre_tancat']
    linies = []

    moviments = Moviment.objects.filter(projecte=projecte)
    if data_inicial and not tots:
        moviments = moviments.filter(data__gte=data_inicial)

    # ordenat per l'import evitem vermells que "no calen"
    for m in moviments.order_by('id'):
        saldo += m.quantitat

        linies.append(dict(
            moviment=m,
            saldo=saldo,
        ))

    assert saldo == bal['balanc_actual']

    return render(
        request,
        'facturacio/llistat_moviments.html',
        dict(
            balanc_ultim_trimestre=bal['balanc_ultim_trimestre_tancat'],
            balanc_actual=bal['balanc_actual'],
            ultim_trimestre_tancat=bal['ultim_trimestre_tancat'],
            linies=linies,
            tots=tots,
        )
    )


@login_required
def validacio_factures_rebudes(request):

    if not request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
        messages.error(request, u"No tens permís per validar factures.")
        return redirect('inici')

    FormValidacio = modelformset_factory(FacturaRebuda, form=FormValidacioFacturaRebuda, extra=0)

    form_validacio = None
    post_with_errors = False

    if request.method == 'POST':
        form_validacio = FormValidacio(data=request.POST)
        if form_validacio.is_valid():
            form_validacio.save()

        else:
            post_with_errors = True

    if 'cooperativa' in request.GET:  # there is a search criteria in place

        form_cerca = FormCercarValidacioFacturaRebuda(data=request.GET)

        if not post_with_errors and form_cerca.is_valid():  # si hi havien errors al POST, no cal buscar factures

            cooperativa_id = form_cerca.cleaned_data['cooperativa']
            coop_id = form_cerca.cleaned_data['coop']
            mostrar_estat = form_cerca.cleaned_data['mostrar_estat']
            factures = FacturaRebuda.objects.filter(client__id=cooperativa_id, estat=mostrar_estat)

            if coop_id:
                factures = factures.filter(projecte__id=coop_id)
            des_de = form_cerca.cleaned_data['des_de']
            if des_de:
                factures = factures.filter(data__gte=des_de)
            fins_a = form_cerca.cleaned_data['fins_a']
            if fins_a:
                factures = factures.filter(data__lte=fins_a)

            ordenar_per = form_cerca.cleaned_data['ordenar_per']
            if ordenar_per == FormCercarValidacioFacturaRebuda.OPCIO_PROJECTE:
                factures = factures.order_by('projecte__nom', 'data')
            elif ordenar_per == FormCercarValidacioFacturaRebuda.OPCIO_PROVEIDOR:
                factures = factures.order_by('proveidor__nom_fiscal', 'data')
            else:
                factures = factures.order_by('data', 'projecte__nom', 'proveidor__nom_fiscal')

            limit = form_cerca.cleaned_data['limit']

            factures = factures[:limit]

            form_validacio = FormValidacio(queryset=factures)

    else:  # when there is no search criteria in place

        ordenar_per = FormCercarValidacioFacturaRebuda.OPCIO_DATA

        form_cerca = FormCercarValidacioFacturaRebuda(initial=dict(
            ordenar_per=ordenar_per,
            mostrar_estat=FacturaRebuda.ESTAT_REBUDA,
            limit=20
        ))

    return render(
        request,
        'facturacio/validacio_factures_rebudes.html',
        dict(
            form_cerca=form_cerca,
            ordre=ordenar_per,
            form_validacio=form_validacio,

            ordenar_per_data=FormCercarValidacioFacturaRebuda.OPCIO_DATA,
            ordenar_per_proveidor=FormCercarValidacioFacturaRebuda.OPCIO_PROVEIDOR,
            ordenar_per_projecte=FormCercarValidacioFacturaRebuda.OPCIO_PROJECTE,

            estat_acceptada=FacturaRebuda.ESTAT_ACCEPTADA,
            estat_rebutjada=FacturaRebuda.ESTAT_REBUTJADA,
            estat_rebuda=FacturaRebuda.ESTAT_REBUDA,
        )
    )


@login_required
@user_passes_test(is_user_responsable_facturacio)
def llistat_projectes_autoocupats(request):

    avui = now().date()

    filtre_estat = request.GET.get('estat', '_tots')
    form_filtres = FormFiltreLiquidacioLlistatProjectes(initial=dict(estat=filtre_estat))

    ultim_trimestre_tancat = trobar_ultim_trimestre_tancat()
    trimestre_a_tancar = trobar_primer_trimestre_obert()
    trimestre_actual = Trimestre.trobar_trimestre(avui)

    projectes = []
    recompte_oberts = 0
    recompte_autoliquidats = 0
    recompte_tancats = 0

    for p in ProjecteAutoocupat.objects.all().order_by('nom'):

        bal = trobar_balanc_actual(p)

        assert ultim_trimestre_tancat == bal['ultim_trimestre_tancat']
        assert trimestre_a_tancar == bal['trimestre_a_tancar']

        liquidacions = LiquidacioTrimestre.objects.filter(projecte=p, trimestre=trimestre_a_tancar)
        if liquidacions.exists():
            assert liquidacions.count() == 1
            estat_trimestre_a_tancar = liquidacions[0].estat

            if liquidacions[0].estat == LiquidacioTrimestre.ESTAT_AUTOLIQUIDAT:
                recompte_autoliquidats += 1
            else:
                assert liquidacions[0].estat == LiquidacioTrimestre.ESTAT_TANCAT
                recompte_tancats += 1
        else:
            estat_trimestre_a_tancar = LiquidacioTrimestre.ESTAT_OBERT
            recompte_oberts += 1

        if filtre_estat != '_tots':
            if estat_trimestre_a_tancar != filtre_estat:
                continue  # TODO this is really lame as we are doing a lot of work for items that are not going be shown
                # perhaps some day we will get past this "dirty trick" era; they will stop pushing us so hard I hope

        form_liquidacio = FormLiquidacioTrimestre(initial=dict(projecte_id=p.id, trimestre_id=trimestre_a_tancar.id))

        projectes.append(dict(
            instance=p,
            balanc_ultim_trimestre_tancat=bal['balanc_ultim_trimestre_tancat'],
            balanc_trimestre_a_tancar=bal['balanc_trimestre_a_tancar'],
            estat_trimestre_a_tancar=estat_trimestre_a_tancar,
            balanc_actual=bal['balanc_actual'],
            form=form_liquidacio,
        ))

    ################################################################################################################
    #  TODO: find my match in "dashboard_facturacio" and factor this out
    ################################################################################################################
    dies_avis_tancament_a_prop = settings.DIES_AVIS_A_PROP_TANCAMENT_TRIMESTRE
    en_periode_de_tancament = (trimestre_a_tancar.data_final < avui <= trimestre_a_tancar.data_limit_tancament)
    data_avis_tancament_a_prop = trimestre_a_tancar.data_final - datetime.timedelta(days=dies_avis_tancament_a_prop)
    a_prop_del_periode_de_tancament = data_avis_tancament_a_prop <= avui <= trimestre_a_tancar.data_limit_tancament
    passat_periode_de_tancament = trimestre_a_tancar.data_limit_tancament < avui
    dies_per_tancar = (trimestre_a_tancar.data_limit_tancament - avui).days
    dies_per_periode_tancament = trimestre_a_tancar.data_final - avui
    ################################################################################################################

    return render(
        request,
        'facturacio/llistat_projectes_autoocupats.html',
        dict(
            projectes=projectes,
            ultim_trimestre_tancat=ultim_trimestre_tancat,
            trimestre_a_tancar=trimestre_a_tancar,
            trimestre_actual=trimestre_actual,
            estat_tancat=LiquidacioTrimestre.estat_display_name(LiquidacioTrimestre.ESTAT_TANCAT),
            form_filtres=form_filtres,

            recompte_oberts=recompte_oberts,
            recompte_autoliquidats=recompte_autoliquidats,
            recompte_tancats=recompte_tancats,

            en_periode_de_tancament=en_periode_de_tancament,
            a_prop_del_periode_de_tancament=a_prop_del_periode_de_tancament,
            dies_per_tancar=dies_per_tancar,
            dies_per_periode_tancament=dies_per_periode_tancament.days,
            passat_periode_de_tancament=passat_periode_de_tancament,
        )
    )


@login_required
@user_passes_test(is_user_responsable_facturacio)
def executar_tancament_trimestre_projecte(request):

    if request.method != 'POST':
        raise SuspiciousOperation

    form = FormLiquidacioTrimestre(data=request.POST)
    if not form.is_valid():
        raise SuspiciousOperation

    try:
        projecte = ProjecteAutoocupat.objects.get(pk=form.cleaned_data['projecte_id'])
        trimestre = Trimestre.objects.get(pk=form.cleaned_data['trimestre_id'])
        assert trimestre == trobar_primer_trimestre_obert()
    except:
        raise SuspiciousOperation

    is_responsable_facturacio = request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists()
    if not is_responsable_facturacio:
        raise SuspiciousOperation

    avui = today()  # TODO: WARNING potentially unsafe aware-to-naïve date conversion
    ultim_trimestre_tancat = trobar_ultim_trimestre_tancat()
    trimestre_a_tancar = trobar_primer_trimestre_obert()

    assert trimestre_a_tancar.data_final < avui

    liquidacio = tancar_trimestre_projecte(projecte, trimestre_a_tancar, ultim_trimestre_tancat, request.user)

    # TODO: això s'hauria de fer amb AJAX i aqui es podria retornar el que cal per actualitzar l'html a la llista
    messages.success(request, u"S'ha tancat el trimestre %s del projecte %s" % (trimestre_a_tancar, projecte))
    return redirect('facturacio:llistat_projectes_autoocupats')


@transaction.atomic
@login_required
@user_passes_test(is_user_responsable_facturacio)
def executar_tancament_trimestre(request):

    if request.method != 'POST':
        raise SuspiciousOperation

    is_responsable_facturacio = request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists()
    if not is_responsable_facturacio:
        raise SuspiciousOperation

    avui = today()  # TODO: WARNING potentially unsafe aware-to-naïve date conversion
    ultim_trimestre_tancat = trobar_ultim_trimestre_tancat()
    trimestre_a_tancar = trobar_primer_trimestre_obert()

    assert trimestre_a_tancar.data_final < avui

    # tamquem el trimestre per tots els projectes autoocupats que encara
    # no el tenen tancat, tant si estaven auto-liquidats com si no

    recompte_auto_liquidats = 0
    recompte_oberts = 0

    for projecte in ProjecteAutoocupat.objects.all():

        liquidacio = tancar_trimestre_projecte(projecte, trimestre_a_tancar, ultim_trimestre_tancat, request.user)
        if liquidacio.estat == liquidacio.ESTAT_AUTOLIQUIDAT:
            recompte_auto_liquidats += 1
        else:
            recompte_oberts += 1

    # i ara tanquem el trimestre

    trimestre_a_tancar.obert = False
    trimestre_a_tancar.save()

    estadistiques = (recompte_oberts + recompte_auto_liquidats,
                     recompte_oberts,
                     recompte_auto_liquidats,
                     trimestre_a_tancar)

    messages.success(request, u"S'han tancat %d projectes en total: %d oberts i %d auto-liquidats. "
                              u"I s'ha tancat el trimestre %s" % estadistiques)
    return redirect('facturacio:llistat_projectes_autoocupats')


@transaction.atomic
@login_required
def executar_autoliquidacio_trimestre(request):

    if request.method != 'POST':
        raise SuspiciousOperation

    projectes = trobar_projectes_usuari(request.user)
    if not projectes:
        messages.error(request, u"No ets membre de referència de cap projecte autoocupat.")
        return redirect('inici')

    if projectes.count() > 1:
        messages.error(request, u"No està previst que una persona sigui membre de referència a més d'un projecte.")
        return redirect('inici')
    else:
        projecte = projectes[0]

    trimestre_a_tancar = trobar_primer_trimestre_obert()

    ara = now()
    avui = today()

    assert trimestre_a_tancar.data_final < avui

    liquidacio = trobar_o_crear_liquidaciotrimestre(projecte, trimestre_a_tancar)

    if liquidacio.pk:  # ja hi havia una liquidació prèvia; The UI should not allow this
        raise SuspiciousOperation

    liquidacio.balanc_final = trobar_balanc_actual(projecte)['balanc_trimestre_a_tancar']
    liquidacio.data_auto_liquidacio = ara
    liquidacio.usuari_auto_liquidacio = request.user
    liquidacio.save()  # in this case, we are not doing anything else with this object
    messages.success(request, u"T'agraïm molt que hagis tancat el trimestre! Ens fa la feina més fàcil! :)")
    return redirect('dashboard_projecte_autoocupat')


@login_required
def volum_facturacio_empresa(request, tipus):

    if not request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
        messages.error(request, u"No tens permís per accedir a aquesta informació.")
        return redirect('inici')

    if tipus not in ['client', 'proveidor']:
        messages.error(request, u"Oops! sembla que hi ha un problema al programa... o que jugues a alguna cosa rara.")
        return redirect('inici')

    if tipus == 'client':
        factures = FacturaEmesa.objects.all()
    else:
        factures = FacturaRebuda.objects.all()

    if 'periode' in request.GET:
        periode = request.GET.get('periode')
        (des_de, fins_a) = calcular_periode(periode)
        if not des_de or not fins_a:
            messages.error(request, u"OOps! el periode no està correctament especificat.")
            return redirect('inici')

    else:
        trimestre = Trimestre.trobar_trimestre(now())
        periode = trimestre.nom
        des_de = trimestre.data_inici
        fins_a = trimestre.data_final

    factures = factures.filter(data__gte=des_de)
    factures = factures.filter(data__lte=fins_a)

    # si no s'especifica un id de cooperativa, es calcularan
    # els totals acumulats entre totes les cooperatives
    if 'cooperativa' in request.GET:
        id_cooperativa = request.GET.get('cooperativa')
    else:
        # TODO potser cal alguna manera menys arbitrària de triar la cooperativa per defecte xD
        id_cooperativa = 1

    if not Cooperativa.objects.filter(pk=id_cooperativa).exists():
        messages.error(request, u"No hi ha cap cooperativa amb aquest identificador.")
        return redirect('inici')

    if tipus == 'client':
        factures = factures.filter(proveidor__pk=id_cooperativa)
    else:
        factures = factures.filter(client__id=id_cooperativa)

    totals = dict(
        base=Decimal('0.00'),
        iva=Decimal('0.00'),
        irpf=Decimal('0.00'),
        req=Decimal('0.00'),
        total=Decimal('0.00')
    )

    # 'empreses' és un diccionari amb empresa.id com a clau i un diccionari com a valor
    # aquest segon diccionari conté els totals acumulats per tota la cooperativa, o per
    # totes les cooperatives, segons s'hagi demanat
    empreses = dict()
    for factura in factures:
        if tipus == 'client':
            empresa = factura.client
        else:
            empresa = factura.proveidor

        if empresa.id in empreses:
            acumulat = empreses.get(empresa.id)
        else:
            acumulat = dict(
                instance=empresa,
                base=Decimal('0.00'),
                iva=Decimal('0.00'),
                irpf=Decimal('0.00'),
                req=Decimal('0.00'),
                total=Decimal('0.00')
            )

        acumulat['base'] += factura.base_total
        acumulat['iva'] += factura.iva_total
        acumulat['total'] += factura.import_total

        totals['base'] += factura.base_total
        totals['iva'] += factura.iva_total
        totals['total'] += factura.import_total

        if tipus == 'client':
            acumulat['req'] += factura.req_total
            totals['req'] += factura.req_total
        else:
            acumulat['irpf'] += factura.irpf_total
            totals['irpf'] += factura.irpf_total

        empreses[empresa.id] = acumulat

    return render(
        request,
        'facturacio/volum_facturacio_empresa.html',
        dict(
            periode=periode,
            tipus=tipus,
            empreses=empreses.values(),
            totals=totals,
            form_seleccio=FormSeleccioVolumFacturacio(initial=dict(periode=periode, cooperativa=id_cooperativa))
        )
    )


@login_required
def buscar_empresa(request, nif=None, kind=None):
    """
    This view pretends to fulfill all the needs when creating
    a new Empresa. It consist of a form view and a display view.
    If the Empresa already exists it will be possible to choose it
    and add it to the invoice, provided it's been validated.
    If the Empresa doesn't exist it will be possible to create one.
    """

    if kind not in ['client', 'proveidor']:
        return HttpResponseForbidden(u"No juguis amb les URLs, si us plau :)")

    kind_display = dict(client=u"client", proveidor=u"proveïdor")

    empresas = Empresa.objects.filter(nif=nif) or None 

    create_form_class = CrearEmpresaForm
    create_form = None

    if request.method == 'POST':

        # Make distinction between different forms
        # so we search for the button name in the request data.
        if 'search' in request.POST:
            search_form = BuscarEmpresaForm(request.POST)
            if search_form.is_valid():
                nif = search_form.cleaned_data['nif'].upper()
                return redirect('facturacio:buscar_empresa', kind=kind, nif=nif)

        if 'create' in request.POST:
            create_form = create_form_class(data=request.POST)
            if create_form.is_valid():
                try:
                    with transaction.atomic():
                        adreca = AdrecaFiscal.objects.create(
                            adreca=create_form.cleaned_data['adreca_fiscal'],
                            poblacio=create_form.cleaned_data['poblacio'],
                            codi_postal=create_form.cleaned_data['codi_postal'])
                        Empresa.objects.create(
                            nom_fiscal=create_form.cleaned_data['nom_fiscal'],
                            telefon=create_form.cleaned_data['telefon'],
                            email=create_form.cleaned_data['email'],
                            adreca_fiscal=adreca,
                            nif=nif,
                            usuaria_que_demana_revisio=request.user)

                        messages.success(request, u"S'ha enregistrat el %s. Gestió Econòmica ha de revisar "
                                                  u"les dades abans que el pugueu fer servir." % kind_display[kind])
                        return redirect('dashboard_projecte_autoocupat')
                except:
                    messages.error(request, u"Hi ha un error i no s'ha pogut crear una empresa amb aquestes dades")
                    return redirect('facturacio:buscar_empresa', kind=kind)

    else:

        search_form = BuscarEmpresaForm()
        if not empresas:
            create_form = create_form_class()

    return render(request,
                  'facturacio/crear_buscar_empreses.html',
                  {
                      'empresas': empresas,
                      'search_form': search_form,
                      'create_form': create_form,
                      'tipo': kind,
                      'nif': nif,
                  })

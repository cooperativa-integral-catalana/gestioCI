from django.conf.urls import patterns, url
from .views import EmpresesPendingRevisionListView


urlpatterns = patterns(

    'facturacio.views',

    url(r'^llistat_empreses_pao/(?P<kind>client|proveidor)/$', 'llistat_empreses_pao',       name="llistat_empreses_pao"),
    
    url(r'^llistat_quotes_trimestrals/$',                    'llistat_quotes_trimestrals',   name='llistat_quotes_trimestrals'),

    url(r'^validar_empresa/(?P<empresa_id>\d+)/$',           'validar_empresa',              name='validar_empresa'),
    url(r'^rebutjar_empresa/(?P<empresa_id>\d+)/$',           'rebutjar_empresa',              name='rebutjar_empresa'),
    url(r'^modificar_empresa/(?P<empresa_id>\d+)/$',    'modificar_empresa', name='modificar_empresa'),
    url(r'^validacio_empreses/$',               EmpresesPendingRevisionListView.as_view(),   name='validacio_empreses'),

    url(r'^buscar_empresa/(?P<kind>\w+)/(?P<nif>\w+)$',      'buscar_empresa',               name="buscar_empresa"),
    url(r'^buscar_empresa/(?P<kind>\w+)$',                   'buscar_empresa',               name="buscar_empresa"),


    url(r'^crear_factura_emesa/$',                           'crear_factura_emesa',          name='crear_factura_emesa'),
    url(r'^crear_factura_emesa/(?P<id_client>\d+)$',         'crear_factura_emesa',          name='crear_factura_emesa'),
    url(r'^linies_factura_emesa/(?P<id_factura>\d+)$',       'editar_linies_factura_emesa',  name='editar_linies_factura_emesa'),
    url(r'^confirmar_factura_emesa/(?P<id_factura>\d+)$',    'confirmar_factura_emesa',      name='confirmar_factura_emesa'),
    url(r'^veure_factura_emesa/(?P<id_factura>\d+)$',        'veure_factura_emesa',          name='veure_factura_emesa'),
    url(r'^rectificar_factura_emesa/(?P<id_factura>\d+)$',    'rectificar_factura_emesa',      name='rectificar_factura_emesa'),

    url(r'^crear_factura_rebuda/$',                          'crear_factura_rebuda',         name='crear_factura_rebuda'),
    url(r'^crear_factura_rebuda/(?P<id_proveidor>\d+)$',     'crear_factura_rebuda',         name='crear_factura_rebuda'),
    url(r'^linies_factura_rebuda/(?P<id_factura>\d+)$',      'editar_linies_factura_rebuda', name='editar_linies_factura_rebuda'),
    url(r'^veure_factura_rebuda/(?P<id_factura>\d+)$',       'veure_factura_rebuda',         name='veure_factura_rebuda'),
    url(r'^rectificar_factura_rebuda/(?P<id_factura>\d+)$',    'rectificar_factura_rebuda',      name='rectificar_factura_rebuda'),
    url(r'^validacio_factures_rebudes/$',                    'validacio_factures_rebudes',   name='validacio_factures_rebudes'),

    url(r'^resum_facturacio_projecte/$',                     'resum_facturacio_projecte',    name='resum_facturacio_projecte'),
    url(r'^resum_facturacio_projecte/(?P<id_projecte>\d+)$', 'resum_facturacio_projecte',    name='resum_facturacio_projecte'),

    url(r'^moviments/$',                                     'llistat_moviments',            name='moviments'),
    url(r'^moviments/(?P<id_projecte>\d+)$',                 'llistat_moviments',            name='moviments'),
    url(r'^moviments/(?P<id_projecte>\d+)/tots/$',           'llistat_moviments',            kwargs=dict(tots=True), name='moviments'),
    url(r'^resum_liquidacio_trimestre/$',                     'resum_liquidacio_trimestre',            name='resum_liquidacio_trimestre'),
    url(r'^executar_autoliquidacio_trimestre/$',              'executar_autoliquidacio_trimestre',     name='executar_autoliquidacio_trimestre'),

    url(r'^llistat_projectes_autoocupats/$',                  'llistat_projectes_autoocupats',         name='llistat_projectes_autoocupats'),
    url(r'^executar_tancament_trimestre_projecte/$',          'executar_tancament_trimestre_projecte', name='executar_tancament_trimestre_projecte'),
    url(r'^executar_tancament_trimestre/$',                   'executar_tancament_trimestre',          name='executar_tancament_trimestre'),

    url(r'^facturacio_per_proveidor/$', 'volum_facturacio_empresa', kwargs=dict(tipus='proveidor'), name='facturacio_per_proveidor'),
    url(r'^facturacio_per_client/$',    'volum_facturacio_empresa', kwargs=dict(tipus='client'),    name='facturacio_per_client'),

    url(r'^importar_extracte_bancari/$',                  'importar_extracte_bancari',             name="importar_extracte_bancari"),
    url(r'^exportar_extracte_bancari/$',                  'exportar_extracte_bancari',             name="exportar_extracte_bancari"),
    url(r'^exportar_extracte_bancari/(?P<id>\d+)$',       'exportar_extracte_bancari',             name="exportar_extracte_bancari"),
    url(r'^llistat_moviments_extracte_descartats/tots/$', 'llistat_moviments_extracte_descartats', kwargs=dict(tots=True), name='llistat_moviments_descartats_tots'),
    url(r'^llistat_moviments_extracte_descartats/$',      'llistat_moviments_extracte_descartats', name='llistat_moviments_descartats'),

    url(r'^conciliacio_automatica_factures/$',
        'conciliacio_automatica_factures', name='conciliacio_automatica_factures'),

    url(r'^conciliacio_automatica_moviments_simples/$',
        'conciliacio_automatica_moviments_simples', name='conciliacio_automatica_moviments_simples'),

    url(r'^executar_conciliacio_moviments_simples/$',
        'executar_conciliacio_moviments_simples', name='executar_conciliacio_moviments_simples'),

    url(r'^conciliacio_manual/$',
        'conciliacio_manual', name='conciliacio_manual'),

    url(r'^conciliacio_manual/(?P<compte_projecte>COOP\d{4})$',
        'conciliacio_manual_projecte', name='conciliacio_manual_projecte'),

    url(r'^conciliacio_manual/(?P<numero_factura>\d{4}/\d{6})$',
        'conciliacio_manual_factura', name='conciliacio_manual_factura'),

    url(r'^executar_conciliacio_automatica_factura$',
        'executar_conciliacio_automatica_factura', name='executar_conciliacio_automatica_factura'),

    url(r'^descartar_moviment_extracte_bancari$',
        'descartar_moviment_extracte_bancari', name='descartar_moviment_extracte_bancari'),

    url(r'^llistat_aportacions_trimestre_projectes$',
        'llistat_aportacions_trimestre_projectes', name='llistat_aportacions_trimestre_projectes'),

    url(r'^exportar_projectes_per_comarca$',
        'exportar_projectes_per_comarca', name='exportar_projectes_per_comarca'),
    
    url(r'^exportar_projectes_per_liquidacio$',
        'exportar_projectes_per_liquidacio', name='exportar_projectes_per_liquidacio'),
    
    url(r'^derivar_exportacio_aportacions_csv$',
        'derivar_exportacio_aportacions_csv', name='derivar_exportacio_aportacions_csv'),
    url(r'^exportar_aportacions_csv/(?P<trimestre>\w{6})$',
        'exportar_aportacions_csv', name='exportar_aportacions_csv'),
    
    url(r'^derivar_exportacio_factures_csv$',
        'derivar_exportacio_factures_csv', name='derivar_exportacio_factures_csv'),
    url(r'^exportar_factures_emeses_csv/(?P<trimestre>\w{6})$',
        'exportar_factures_emeses_csv', name='exportar_factures_emeses_csv'),
    url(r'^exportar_factures_rebudes_csv/(?P<trimestre>\w{6})$',
        'exportar_factures_rebudes_csv', name='exportar_factures_rebudes_csv'),

)

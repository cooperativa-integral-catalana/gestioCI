# coding=utf-8
import re

from datetime import timedelta
from decimal import Decimal

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator
from django.db import models, transaction
from django.db.models import Max, Sum

from gestioci.helpers import now
from projectes.models import ProjecteAutoocupat
from empreses.models import Empresa, Cooperativa
from socies.models import PersonaUsuaria, Persona
from gestioci.settings import auth_groups
from .helpers import calcular_trimestre


class TarifaQuotaTrimestral(models.Model):

    base_imposable_minima = models.DecimalField(max_digits=8, decimal_places=2)
    import_quota_trimestral = models.DecimalField(max_digits=8, decimal_places=2)
    data_vigor = models.DateField(verbose_name=u"data d'entrada en vigor")

    class Meta:
        verbose_name_plural = 'quotes trimestrals'


class Factura(models.Model):

    FORMA_PAGAMENT_EFECTIU = 'efectiu'
    FORMA_PAGAMENT_TRANSFERENCIA = 'transferencia'

    FORMES_DE_PAGAMENT = (
        (FORMA_PAGAMENT_EFECTIU, u"Cobrament al comptat"),
        (FORMA_PAGAMENT_TRANSFERENCIA, u"Ingrés al compte corrent de la cooperativa"),
    )

    projecte = models.ForeignKey(ProjecteAutoocupat)

    proveidor = models.ForeignKey(Empresa, related_name='factures_emeses')
    client = models.ForeignKey(Empresa, related_name='factures_rebudes')

    data = models.DateField()

    base_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    iva_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    import_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)

    data_venciment = models.DateField(null=True, blank=True)
    forma_de_pagament = models.CharField(default=FORMA_PAGAMENT_EFECTIU, max_length=16, choices=FORMES_DE_PAGAMENT)

    observacions = models.TextField(max_length=4096, blank=True)
    
    def _actualitzar_camps_calculats(self):
        """
        * actualitza els camps calculats de Factura.
        * no crida a save() en acabar, pq aquesta funció s'ha de 'overload' a
        les classes que en deriven, FacturaEmesa i FacturaRebuda, i es allà a
        on s'espera que es cridi el save()
        """

        client_es_cooperativa = Cooperativa.objects.filter(pk=self.client_id).exists()
        proveidor_es_cooperativa = Cooperativa.objects.filter(pk=self.proveidor_id).exists()

        if client_es_cooperativa and proveidor_es_cooperativa:
            raise Exception(u"La factura és invàlida: el client i el proveïdor són cooperatives")

        if not (client_es_cooperativa or proveidor_es_cooperativa):
            raise Exception(u"La factura és invàlida: no implica cap cooperativa, ni com a client ni com a proveïdor")

        if not self.pertany_a_trimestre_obert():
            raise Exception(u"La factura no es pot editar: correspon a un trimestre que no esta obert")

        self.base_total = Decimal('0.00')
        self.iva_total = Decimal('0.00')
        self.import_total = Decimal('0.00')

        for l in self.liniafactura_set.all():
            self.base_total += l.base_total
            self.iva_total += l.iva_total
            self.import_total += l.import_total

    def usuari_pot_editar(self, user):

        try:
            persona = PersonaUsuaria.objects.filter(usuari=user).first().persona
            self.projecte.membres_de_referencia.get(pk=persona.pk)
            return True

        except PersonaUsuaria.DoesNotExist, Persona.DoesNotExist:
            return False

    def pertany_a_trimestre_obert(self):

        trimestre = Trimestre.trobar_trimestre(self.data)
        assert trimestre  # no s'hauria d'haver pogut crear una factura en un trimestre que no existeix

        return trimestre.obert

    def clean(self):
        # Don't allow an invoice in the future and in trimester that doesn't exists
        try:
            if (self.data > now().date()) and not self.pertany_a_trimestre_obert():
                raise ValidationError("No está permés introdüir factures en trimestres futurs")
        except:
            raise ValidationError("No está permés introdüir factures en trimestres futurs")
        
    def save(self, *args, **kwargs):
        if (self.client and self.client.pendent_de_revisio) or (self.proveidor and self.proveidor.pendent_de_revisio):
            raise Exception(u"La factura és invàlida: no pot implicar una empresa que no ha estat validada per GE")
        super(Factura, self).save(*args, **kwargs)

    class Meta:
        verbose_name_plural = u"factures"


class SequenciaNumeroFacturaEmesa(models.Model):

    year = models.SmallIntegerField(primary_key=True)
    value = models.IntegerField()

    @staticmethod
    @transaction.atomic
    def incrementa_i_retorna_seguent_numero(data):
        sequencia = SequenciaNumeroFacturaEmesa.objects.select_for_update().get(year=data.year)
        sequencia.value += 1
        sequencia.save()
        return sequencia.value

    @staticmethod
    def crear_sequencies_necessaries():
        """
        inicialitza a 1 les seqüències per als propers anys;
        la idea és no haver d'inicialitzar la seqüència per cada any
        en el moment de generar la primera factura; com que els anys
        es creen per avançat, d'aquesta manera s'esquiva el problema
        de concurrència que requeriria un "lock" explícit que caldria
        en el supòsit que evitem d'aquesta manera.

        aquest mètode està pensat per ser cridat des d'un cron job.
        """
        years_in_advance = 5
        current_year = now().year
        farthest_year = SequenciaNumeroFacturaEmesa.objects.all().aggregate(Max('year'))['year__max']
        if farthest_year is None:
            farthest_year = current_year - 1
        for year in range(farthest_year + 1, max(current_year + years_in_advance, farthest_year) + 1):
            SequenciaNumeroFacturaEmesa.objects.create(year=year, value=0)


class FacturaEmesa(Factura):

    ESTAT_EMESA = 'emesa'
    ESTAT_COBRADA = 'cobrada'

    ESTATS = (
        (ESTAT_EMESA, u"Pendent de cobrar"),
        (ESTAT_COBRADA, u"Cobrada"),
    )

    numero = models.CharField(max_length=11, blank=True, null=True)
    estat = models.CharField(max_length=16, choices=ESTATS, default=ESTAT_EMESA)
    req_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)  # recàrrec d'equivalència

    @transaction.atomic
    def assignar_numero_sequencia(self):
        n = SequenciaNumeroFacturaEmesa.incrementa_i_retorna_seguent_numero(self.data)
        self.numero = '%d/%06d' % (self.data.year, n)
        self.save()

    def save(self, actualitzar_dades=True, *args, **kwargs):
        if actualitzar_dades:
            self._actualitzar_camps_calculats()
            self.req_total = Decimal('0.00')
            for l in self.liniafactura_set.all():
                self.req_total += l.req_total
        super(FacturaEmesa, self).save(*args, **kwargs)

    def calcular_resums(self):

        linies = self.liniafactura_set

        resum_iva = linies.values('tipus_iva').annotate(base_total=Sum('base_total'), iva_total=Sum('iva_total')).order_by('tipus_iva')
        resum_req = linies.values('tipus_req').annotate(base_total=Sum('base_total'), req_total=Sum('req_total')).order_by('tipus_req')
        resum_totals = linies.aggregate(Sum('base_total'), Sum('iva_total'), Sum('req_total'))

        import_total = sum([resum_totals[k] for k in resum_totals.keys()])
        base_total = resum_totals['base_total__sum']
        iva_total = resum_totals['iva_total__sum']
        req_total = resum_totals['req_total__sum']

        assert import_total == self.import_total
        assert base_total == self.base_total
        assert iva_total == self.iva_total
        assert req_total == self.req_total

        if len(resum_req) == 1:
            if resum_req[0]['tipus_req'] == Decimal('0.00'):
                resum_req = None

        return resum_iva, resum_req, resum_totals

    def __unicode__(self):
        return "FacturaEmesa %d (%s)" % (self.id, self.numero)

    class Meta:
        verbose_name_plural = u"factures emeses"


class FacturaRebuda(Factura):

    ESTAT_REBUDA = 'rebuda'        # s'ha creat la factura al portal del soci
    ESTAT_ACCEPTADA = 'acceptada'  # s'ha rebut la factura a GE (en paper) i s'ha acceptat per la comptabilitat
    ESTAT_REBUTJADA = 'rebutjada'  # no s'ha rebut a GE i/o s'ha considerat que no és cap factura vàlida

    ESTATS = (
        (ESTAT_REBUDA, u"Pendent de revisió"),
        (ESTAT_ACCEPTADA, u"Acceptada"),
        (ESTAT_REBUTJADA, u"Rebutjada"),
    )

    numero = models.CharField(max_length=64)

    irpf_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    tipus_irpf = models.DecimalField(default=0, max_digits=4, decimal_places=2,
                                     verbose_name=u"retenció per l'IRPF",
                                     help_text=u"en tant percent (%)")

    estat = models.CharField(max_length=16, choices=ESTATS, default=ESTAT_REBUDA)
    vist_i_plau_usuaria = models.ForeignKey(User, blank=True, null=True, related_name='+')
    vist_i_plau_data = models.DateTimeField(blank=True, null=True)
    vist_i_plau_observacions = models.TextField(blank=True)

    def save(self, *args, **kwargs):
        self._actualitzar_camps_calculats()
        self.irpf_total = ((self.base_total * self.tipus_irpf) / 100).quantize(Decimal('0.01'))
        self.import_total -= self.irpf_total
        super(FacturaRebuda, self).save(*args, **kwargs)

    def calcular_resums(self):

        linies = self.liniafactura_set

        resum_iva = linies.values('tipus_iva').annotate(base_total=Sum('base_total'), iva_total=Sum('iva_total')).order_by('tipus_iva')
        resum_totals = linies.aggregate(Sum('base_total'), Sum('iva_total'))

        import_total = sum([resum_totals[k] for k in resum_totals.keys()]) - self.irpf_total
        base_total = resum_totals['base_total__sum']
        iva_total = resum_totals['iva_total__sum']

        assert import_total == self.import_total
        assert base_total == self.base_total
        assert iva_total == self.iva_total

        return resum_iva, resum_totals

    class Meta:
        verbose_name_plural = u"factures rebudes"


class LiniaFactura(models.Model):

    IVA_0 = Decimal('0.00')
    IVA_4 = Decimal('4.00')
    IVA_10 = Decimal('10.00')
    IVA_21 = Decimal('21.00')

    TIPUS_IVA = ((IVA_0,   '0 %'),
                 (IVA_4,   '4 %'),
                 (IVA_10, '10 %'),
                 (IVA_21, '21 %')
                 )
    factura = models.ForeignKey(Factura)

    quantitat = models.DecimalField(max_digits=8, decimal_places=2)
    concepte = models.CharField(max_length=128)
    preu_unitari = models.DecimalField(max_digits=8, decimal_places=2)
    tipus_iva = models.DecimalField(max_digits=4, decimal_places=2,
                                    choices=TIPUS_IVA, default=IVA_21,
                                    verbose_name=u"tipus d'IVA")
    tipus_req = models.DecimalField(default=0, max_digits=4, decimal_places=2,
                                    verbose_name=u"tipus de recàrrec d'equivalència",
                                    help_text=u"en tant percent")

    # "calculated" fields that allow us to offset percentage and aggregation to the database layer
    base_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    iva_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    req_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    import_total = models.DecimalField(default=0, max_digits=8, decimal_places=2)

    def save(self, *args, **kwargs):

        self.base_total = self.quantitat * self.preu_unitari
        self.iva_total = ((self.base_total * self.tipus_iva) / 100).quantize(Decimal('0.01'))
        self.req_total = ((self.base_total * self.tipus_req) / 100).quantize(Decimal('0.01'))
        self.import_total = self.base_total + self.iva_total + self.req_total

        super(LiniaFactura, self).save(*args, **kwargs)

    class Meta:
        verbose_name = u"línia de factura"
        verbose_name_plural = u"línies de factura"


class Trimestre(models.Model):

    nom = models.CharField(max_length=10, unique=True)

    data_inici = models.DateField()
    data_final = models.DateField()
    data_limit_tancament = models.DateField()

    obert = models.BooleanField(default=False)

    @property
    def tancat(self):
        return not self.obert

    @staticmethod
    def trobar_trimestre(data):
        """
        Retorna el trimestre que conté la data indicada, o None si el trimestre encara no està donat d'alta
        """

        t = Trimestre.objects.filter(data_inici__lte=data).filter(data_final__gte=data)
        if t.count():
            return t.first()

        return None

    @staticmethod
    def trobar_per_any_i_numero(year, num):
        t = Trimestre.objects.filter(nom__exact=Trimestre.nom_trimestre(year, num))
        if t.exists():
            return t[0]
        return None

    @staticmethod
    def nom_trimestre(year, num):
        return '%dT%d' % (year, num)

    @staticmethod
    def crear_trimestre(data, obert=False):
        """
        Crea i retorna el Trimestre que hi ha "al voltant" (que inclou) la data que s'especifica
        """
        year, trimestre, inici, final = calcular_trimestre(data)
        limit_tancament = final + timedelta(days=10)
        return Trimestre.objects.create(
            nom=Trimestre.nom_trimestre(year, trimestre),
            data_inici=inici,
            data_final=final,
            data_limit_tancament=limit_tancament,
            obert=obert)

    @staticmethod
    def crear_trimestres_necessaris():
        """
        Crea si és necessari el trimestre actual i el següent.
        Retorna un text amb les accions que s'han executat.
        """

        creats = []

        avui = now().date()
        t_actual = Trimestre.trobar_trimestre(avui)
        if not t_actual:
            t_actual = Trimestre.crear_trimestre(data=avui, obert=True)
            creats.append(u"creat %s amb id %d" % (t_actual, t_actual.id))

        # la data límit de tancament d'un trimestre sempre cau dintre del següent trimestre
        d_seguent = t_actual.data_limit_tancament
        if not Trimestre.trobar_trimestre(d_seguent):
            t_seguent = Trimestre.crear_trimestre(data=d_seguent, obert=True)
            creats.append(u"creat %s amb id %d" % (t_seguent, t_seguent.id))

        if len(creats):
            return u", ".join(creats)
        else:
            return u"no s'ha fet res"

    def __unicode__(self):
        return self.nom


class Moviment(models.Model):
    """
    S'enten que:
        * els moviments són de diners (euro)
        * són sempre en el context d'un "soci" (ProjecteAutoocupat)
        * són moviments entre una Cooperativa i "una altra entitat":
            * Empresa: client o proveïdora del "soci" (ProjecteAutoocupat)
            * el mateix "soci" (ProjecteAutoocupat)
        * en qualsevol de les dues direccions:
            * import positiu: a favor de la cooperativa
            * import negatiu: a favor de l'altra entitat

    Els moviments que no fan referència a cap empresa són moviments entre
    el "soci" (ProjecteAutoocupat) i la cooperativa.
    """

    projecte = models.ForeignKey(ProjecteAutoocupat)
    cooperativa = models.ForeignKey(Cooperativa, related_name='+')
    empresa = models.ForeignKey(Empresa, null=True, blank=True, related_name='+')
    quantitat = models.DecimalField(default=0, max_digits=8, decimal_places=2)
    data = models.DateField()
    concepte = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, related_name='+')
    updated_by = models.ForeignKey(User, related_name='+')  # TODO replace useless junk with some kind of Audit Trail implementation

    def save(self, *args, **kwargs):

        assert Cooperativa.objects.filter(pk=self.cooperativa).exists()

        # qui actualitza ha de pertànyer al grup en el moment d'actualitzar
        if not self.updated_by.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
            raise Exception(u"L'usuari '%s' vol crear o modificar un moviment, però no té permís." % self.updated_by)

        # qui crea l'objecte ha de pertànyer al grup en el moment de la creació
        if not self.pk:
            if not self.created_by.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
                raise Exception(u"L'usuari '%s' vol crear un moviment, però no té permís." % self.updated_by)

        # si el moviment es cap a o des de una empresa, aquesta no pot ser una cooperativa
        if self.empresa is not None:
            assert not Cooperativa.objects.filter(pk=self.empresa).exists()

        super(Moviment, self).save(*args, **kwargs)

#     class Meta:
#
#         abstract = True
#
#
# class MovimentEmpresa(Moviment):
#     """
#     Moviments entre una Cooperativa i una Empresa (client o proveïdora d'un "soci" (ProjecteAutoocupat))
#     """
#     empresa = models.ForeignKey(Empresa)
#
#     def save(self, *args, **kwargs):
#         assert not Cooperativa.objects.filter(pk=self.empresa).exists()
#         super(MovimentEmpresa, self).save(*args, **kwargs)
#
#     class Meta:
#         verbose_name_plural = u"Moviments Empresa"
#
#
# class MovimentProjecteAutoocupat(Moviment):
#     """
#     Moviments entre una cooperativa i un "soci" (ProjecteAutoocupat)
#     """
#
#     class Meta:
#         verbose_name = u"Moviment ProjecteAutoocupat"
#         verbose_name_plural = u"Moviments ProjecteAutoocupat"


class LiquidacioTrimestre(models.Model):

    ESTAT_OBERT = 'obert'
    ESTAT_AUTOLIQUIDAT = 'autoliquidat'
    ESTAT_TANCAT = 'tancat'
    ESTATS = (
        (ESTAT_OBERT, u"Obert"),
        (ESTAT_AUTOLIQUIDAT, u"Auto-liquidat"),
        (ESTAT_TANCAT, u"Tancat"),
    )

    trimestre = models.ForeignKey(Trimestre, related_name='+')
    projecte = models.ForeignKey(ProjecteAutoocupat, related_name='+')

    # liquidació que fa el membre de referència
    data_auto_liquidacio = models.DateTimeField(null=True)
    usuari_auto_liquidacio = models.ForeignKey(User, null=True, related_name='+')

    # liquidació definitiva que fa GE
    data_liquidacio = models.DateTimeField(null=True)
    usuari_liquidacio = models.ForeignKey(User, null=True, related_name='+')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # balanç al tancament del trimestre
    balanc_final = models.DecimalField(default=0, max_digits=8, decimal_places=2)

    @property
    def estat(self):
        if self.data_liquidacio:
            return self.ESTAT_TANCAT
        if self.data_auto_liquidacio:
            return self.ESTAT_AUTOLIQUIDAT
        return self.ESTAT_OBERT

    @property
    def estat_display(self):
        return self.estat_display_name(self.estat)

    @staticmethod
    def estat_display_name(estat):
        noms = {k: v for k, v in LiquidacioTrimestre.ESTATS}
        return noms[estat]

    class Meta:
        unique_together = (('trimestre', 'projecte'), )
        verbose_name = u"Liquidació de trimestre"
        verbose_name_plural = u"Liquidacions de trimestres"


class QuotaTrimestral(models.Model):

    ESTAT_EMESA = 'emesa'
    ESTAT_COBRADA = 'cobrada'

    ESTATS = (
        (ESTAT_EMESA, u"Pendent de cobrar"),
        (ESTAT_COBRADA, u"Cobrada"),
    )

    projecte = models.ForeignKey(ProjecteAutoocupat, related_name='+')
    trimestre = models.ForeignKey(Trimestre, related_name='+')

    base_imposable_emesa = models.DecimalField(max_digits=8, decimal_places=2)
    base_imposable_rebuda = models.DecimalField(max_digits=8, decimal_places=2)
    import_quota = models.DecimalField(max_digits=8, decimal_places=2)

    estat = models.CharField(max_length=16, choices=ESTATS, default=ESTAT_EMESA)
    moviment = models.ForeignKey(Moviment)

    class Meta:
        unique_together = (('trimestre', 'projecte'), )
        verbose_name = u"Quota trimestral"
        verbose_name_plural = u"Quotes trimestrals"


class ExtracteBancari(models.Model):
    """
    It represents a snapshot of a bank statement import
    process.
    """
    nom_arxiu = models.CharField(max_length=128)
    usuaria_importadora = models.ForeignKey(User)
    data_importacio = models.DateTimeField(auto_now_add=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class MovimentExtracteBancari(models.Model):
    """
    It represents a single line of an imported bank statement
    """

    validator_compte_projecte = RegexValidator(regex=re.compile(r'^(COOP\d{4})?$'),
                                               message=u"el format del compte de projecte ha de ser COOP9999")
    validator_numero_factura = RegexValidator(regex=re.compile(r'^(\d{4}/\d{6})?$'),
                                              message=u"el número de factura ha de ser yyyy/999999")

    ESTAT_PENDENT = 'pendent'      # acabat d'importar; pendent de conciliació
    ESTAT_CONCILIAT = 'conciliat'  # conciliat (i.e. s'ha identificat la factura o quota a que correspon el moviment
    ESTAT_DESCARTAT = 'descartat'  # no es conciliarà i en tot cas ja es tornarà a importar si s'escau
    ESTAT_DELEGAT = 'delegat'      # els moviments pendents es canvies cap a aquest estat quan s'exporten cap a GE

    ESTATS = ((ESTAT_PENDENT,   u"Pendent de conciliació"),
              (ESTAT_CONCILIAT, u"Conciliat"),
              (ESTAT_DESCARTAT, u"Descartat"),
              (ESTAT_DELEGAT,   u"Delegat"))

    extracte_bancari = models.ForeignKey(ExtracteBancari)

    numero_moviment = models.IntegerField()
    compte = models.CharField(max_length=64)
    tipus = models.CharField(max_length=64)
    data = models.DateField()
    concepte = models.CharField(max_length=128)
    quantitat = models.DecimalField(max_digits=8, decimal_places=2)
    compte_projecte = models.CharField(max_length=16, blank=True, validators=[validator_compte_projecte])
    numero_factura = models.CharField(max_length=64, blank=True, validators=[validator_numero_factura])
    comentaris = models.TextField(max_length=256, blank=True)
    estat = models.CharField(max_length=16, default=ESTAT_PENDENT, choices=ESTATS)
    # TODO cal un 'conciliat_o_descartat_per', i una data?

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:

        unique_together = (('extracte_bancari', 'numero_moviment'),)


class ConciliacioMovimentExtracteBancari(models.Model):

    moviment_extracte = models.ForeignKey(MovimentExtracteBancari)
    moviment_balanc = models.ForeignKey(Moviment)
    comentaris = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, related_name='+')
    updated_by = models.ForeignKey(User, related_name='+')

    def save(self, *args, **kwargs):

        # qui actualitza ha de pertànyer al grup en el moment d'actualitzar
        if not self.updated_by.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
            raise Exception(u"L'usuari '%s' vol crear o modificar un moviment, però no té permís." % self.updated_by)

        # qui crea l'objecte ha de pertànyer al grup en el moment de la creació
        if not self.pk:
            if not self.created_by.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists():
                raise Exception(u"L'usuari '%s' vol crear un moviment, però no té permís." % self.updated_by)

        super(ConciliacioMovimentExtracteBancari, self).save(*args, **kwargs)


class ConciliacioMovimentExtracteBancariFactura(ConciliacioMovimentExtracteBancari):

    factura = models.ForeignKey(Factura)

    conciliacio_automatica = models.BooleanField(default=False)
    conciliacio_automatica_observacions = models.TextField()
    moviment_balanc_ajust = models.ForeignKey(Moviment, null=True)


# coding=utf-8
import calendar
import json
from datetime import date, timedelta

from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.contrib.messages.views import SuccessMessageMixin
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import Group
from django.contrib import messages
from django.core.exceptions import SuspiciousOperation
from django.forms import modelformset_factory, formset_factory, SplitDateTimeWidget
from django.forms.models import modelform_factory
from django.http import Http404
from django.shortcuts import render, get_object_or_404, redirect
from django.template import RequestContext
from django.conf import settings
from django.db import transaction
from django.db.models import Count
from django.http import HttpResponse
from django.template import loader, Context
from django.contrib.auth.models import User

from alta_socies.models import RegistreCanviProcesAltaAutoocupat
from gestioci.helpers import now
from .forms import GeneradorSessions, FormulariProjecte, FormulariDadesGeneralsProjecte, \
    FormulariDetallsMembreDeReferencia, FormulariAdrecaProjecteAutoocupat, \
    FormulariAltaProjecteAutoocupatSessioMoneda, FormulariAltaProjecteAutoocupatCessioUs, \
    FormulariAltaProjecteAutoocupatLloguer, FormulariAltresActivitatsProjecteAutoocupatFiraire, \
    FormulariAltresActivitatsProjecteAutoocupatNoFiraire, FormulariAltaConcessioActivitat, FormulariAltaAsseguranca, \
    FormulariAltaQuotes, FormulariFormaPagamentQuotesAutoocupat, FormulariCercaActualitzacioSeleccioPersones, \
    ForulariPas19, FormulariFotoProjecteAutoocupatFiraire, FormulariCercaProcesAltaAutoocupat, \
    FormulariReassignarProcesAltaAutoocupat, FormulariRebutjarProcesAltaAutoocupat, \
    FormulariDadesPersonalsSociaCooperativa, FormulariDadesAltaSociaCooperativa, \
    FormulariDadesGeneralsProjecteCollectiu, \
    FormulariDadesConcretesProjecteCollectiu, FormulariFiltreSociesPerHabilitat, \
    FormulariDadesAdrecaSociaCooperativa

from gestioci.settings import auth_groups
from .models import SessioAlta, SessioAvaluacio, ProcesAltaAutoocupat, SessioAcollida, \
    PeticioConcessioActivitat, PeticioAsseguranca, SessioMoneda, SociaCooperativa, SociaAfi, \
    ProjecteCollectiu
from socies.models import Persona, AdrecaProjecteAutoocupat, HabilitatsContribucionsPersona, \
    Habilitat, ContribucioCIC, Activitat, AdrecaProjecteCollectiu, PersonaUsuaria, \
    AdrecaSociaCooperativa
from empreses.models import Cooperativa
from gestioci.formats import dow_names
from gestioci.thumbnails import create_thumbnail
from projectes.models import FotoProjecteAutoocupatFiraire, ProjecteAutoocupat, PolissaAsseguranca


def _ordenar_queryset_sessions(queryset):
    """
    Ordena un queryset de Sessio (o classes derivades) amb l'objectiu de poder
    agrupar al template. Atenció que el queryset retornat té camps 'extra' que
    calen per fer la ordenació a Postgres.
    :param queryset: queryset de Sessio o derivats
    :return: queryset amb 'extras' ordenat de la següent manera:
        1. per any i mes (descendent)
        2. per dia del mes
        3. per ubicació
        4. per hora i minut (i segons etc.)
    """

    queryset = queryset.extra(
        select={
            'x_year_month': 'date_trunc(\'month\', data)',
            'x_dom':        'extract(day from data)',
            'x_time':       'extract(epoch from data)'
        }
    )

    queryset = queryset.order_by('-x_year_month', 'x_dom', 'ubicacio__nom', 'x_time')

    return queryset


def _buscar_processos_alta_autoocupat(criteri):

    resultats = ProcesAltaAutoocupat.objects

    responsable = criteri.get('per_responsable', None)
    if responsable:
        responsable = int(responsable)
    email = criteri.get('per_email', None)
    nom = criteri.get('per_nom', None)
    estat = criteri.get('per_estat', None)
    pas = None
    if estat == '*':
        estat = None
    elif estat:
        try:
            pas = int(estat)
            estat = None
        except ValueError:
            pass

    cerca = False  # per saber al template si s'ha executat una cerca o si acabem d'arribar a la "pàgina"

    if responsable:
        resultats = resultats.filter(responsable=responsable).order_by('nom')
        cerca = True

    if email:
        resultats = resultats.filter(email__icontains=email).order_by('email')
        cerca = True

    if nom:
        resultats = resultats.filter(nom__icontains=nom).order_by('nom')
        cerca = True

    if estat:
        resultats = resultats.filter(resolucio=estat)
        cerca = True

    if pas:
        resultats = resultats.filter(pas=pas)

    return resultats, cerca


@login_required
def rebutjar_proces_alta_autoocupat(request, id_proces):

    proces = ProcesAltaAutoocupat.objects.get(id=id_proces)

    if not request.user.id == proces.responsable.id and not is_user_supervisora_alta(request.user):

        messages.error(request, u"No tens permís per rebutjar aquest projecte autoocupat.")
        return redirect('inici')

    if request.method == 'POST':

        form = FormulariRebutjarProcesAltaAutoocupat(data=request.POST)

        if form.is_valid():
            proces.resolucio = ProcesAltaAutoocupat.RESOLUCIO_REBUTJAT
            proces.data_resolucio = now()
            proces.comentaris_resolucio = form.cleaned_data.get('motiu')
            proces.resolucio_usuaria = request.user
            proces.save()

            messages.success(request, u"Has rebutjat el projecte autoocupat.")

            return redirect('dashboard_gestio_economica')

    if request.method == 'GET':

        form = FormulariRebutjarProcesAltaAutoocupat()

    return render(
        request,
        'alta_socies/autoocupat/rebutjar.html',
        dict(
            form=form,
            proces=proces,
        )
    )


def is_user_responsable_alta(user):
    """
    function that returns True when user belongs
    to RESPONSABLE_ALTA group
    """
    return user.groups.filter(name=auth_groups.RESPONSABLES_ALTA).exists()


class FilterResponsablesAltaMixin(object):
    """
    Mixin per filtrar la llista d'opcions al widget del camp "responsable_alta"
    de manera que només ofereix usuaris membres del grup RESPONSABLES_ALTA
    """
    def get_context_data(self, **kwargs):
        context = super(FilterResponsablesAltaMixin, self).get_context_data(**kwargs)
        context['form'].fields['responsable_alta'].queryset = User.objects.filter(
            groups__name=auth_groups.RESPONSABLES_ALTA)
        return context


def is_user_exporter(user):
    return user.groups.filter(name=auth_groups.EXPORTADORES).exists()


# TODO this fully overlaps the checks in gci_tags; I bet there is something native in django th handle this.
def is_user_supervisora_alta(user):
    return user.groups.filter(name=auth_groups.SUPERVISORES_ALTA).exists()


@login_required
@user_passes_test(is_user_exporter)
def exportar_a_csv(request):
    """ 
    Vista per exportar els processos d'alta de projecte
    autoocupat en format CSV ("cap al transversal")
    """

    def trobar_activitats_procesaltaautoocupat(projecte):
        """
        Find the names of the Activitat that are "linked" to "projecte"
        :param projecte: either a ProcesAltaAutoocupat or a ProjecteAutoocupat
        :return: list with the names of the Activitat
        """
        resultat = projecte.altres_activitats.all()
        for a in projecte.adreces.all():
            resultat |= a.activitats.all()
        return list(resultat.distinct().values_list('nom', flat=True))

    def trobar_habilitats_procesaltaautoocupat(projecte):
        """
        Find the names of the Habilitat that are "linked" to "membres de referència" in "projecte"
        :param projecte: either a ProcesAltaAutoocupat or a ProjecteAutoocupat
        :return: list with the names of the Habilitat
        """
        resultat = Habilitat.objects.none()
        mr = projecte.membres_de_referencia.all()
        hcp = HabilitatsContribucionsPersona.objects.filter(persona__in=mr)
        for h in hcp:
            resultat |= h.habilitats.all()
        return list(resultat.distinct().values_list('nom', flat=True))

    d = now().date().isoformat()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="projectes-%s.csv"' % d

    header = [
        u"Data de resolució",
        u"Compte COOP",
        u"Cooperativa",
        u"Email",
        u"Telèfon",
        u"Adreça",
        u"CP",
        u"Població",
        u"Comarca",
        u"mrNom",
        u"mrCognoms",
        u"mrDNI",
        u"Tipus",
        u"Pas",  # TODO: quan tirem de ProjecteAutoocupat, aquest camp no tindrà sentit
        u"Activitats",
        u"Habilitats",
    ]

    csv_data = [header]

    for proces in ProcesAltaAutoocupat.objects.all().exclude(resolucio=ProcesAltaAutoocupat.RESOLUCIO_REBUTJAT).order_by('id'):

        #####################################################################################
        #
        # Farem servir la data de l'últim "pas 20" com a "data d'alta" si el procés d'alta
        # no s'ha "acceptat", donat que ara mateix a producció estan en aquest estat tots els
        # processos. Quan es desplegui la nova versió a producció i s'acceptin els processos,
        # ja no caldrà aquest bloc de codi.

        # de fet, ara mateix probablement tindria sentit treure de la llista el processos que
        # no han passat el pas 20 (no existeix un RegistreCanviProcesAltaAutoocupat del 20 al 20)
        # o inclús marcar com a "acceptat" els processos que estan en aquest estat.
        # TODO: proposar marcar com a "acceptat" els processos que tenen una transició 20 -> 20 i que estan al pas 20

        # select proces_id, max(updated_at) from alta_socies_registrecanviprocesaltaautoocupat
        #        where pas_origen = 20 and pas_origen=20 group by proces_id;

        data_alta = None
        if proces.data_resolucio:
            data_alta = proces.data_resolucio.date()
        elif proces.pas == 20:
            transicions_pas_20 = RegistreCanviProcesAltaAutoocupat.objects.filter(proces=proces,
                                                                                  pas_origen=20,
                                                                                  pas_destinacio=20)
            if transicions_pas_20.exists():
                data_alta = transicions_pas_20.order_by('-updated_at')[0].updated_at.date()

        #
        #######################################################################################

        linia = [
            data_alta,
            proces.compte_ces_assignat,
            proces.cooperativa_assignada,
            proces.email,
            proces.telefon]

        if proces.adreces.first() is not None:
            linia += [
                proces.adreces.first().adreca,
                proces.adreces.first().codi_postal,
                proces.adreces.first().poblacio,
                proces.adreces.first().comarca]
        else:
            linia += [
                None,
                None,
                None,
                None]

        if proces.membres_de_referencia.first() is not None:
            linia += [
                proces.membres_de_referencia.first().nom,
                proces.membres_de_referencia.first().cognom1 +
                " " + proces.membres_de_referencia.first().cognom2,
                proces.membres_de_referencia.first().dni]
        else:
            linia += [
                None,
                None,
                None]

        linia.append(proces.individual)
        linia.append(proces.pas)  # TODO: quan tirem de ProjecteAutoocupat, aquest camp no tindrà sentit
        linia.append(','.join(trobar_activitats_procesaltaautoocupat(proces)))
        linia.append(','.join(trobar_habilitats_procesaltaautoocupat(proces)))

        linia = ['' if camp is None else camp for camp in linia]
        csv_data.append(linia)

    """
    # How it should be when all projects are accepted
    csv_data = [[projecte.data_resolucio.date() if projecte.data_resolucio else None,
                 projecte.compte_ces_assignat,
                 projecte.cooperativa_assignada,
                 projecte.email,
                 projecte.telefon,
                 projecte.adreces.first().adreca,
                 projecte.adreces.first().codi_postal,
                 projecte.adreces.first().poblacio,
                 projecte.adreces.first().comarca,
                 projecte.membres_de_referencia.objects.first().nom,
                 projecte.membres_de_referencia.objects.first().cognom1 +
                 " " + projecte.membres_de_referencia.objects.first().cognom2,
                 projecte.membres_de_referencia.objects.first().dni,
                 projecte.individual,
                 projecte.pas,  # TODO: quan tirem de ProjecteAutoocupat, aquest camp no tindrà sentit
                 ','.join(trobar_activitats_procesaltaautoocupat(projecte)),
                 ','.join(trobar_habilitats_procesaltaautoocupat(projecte)),
                 ]
                for projecte in ProjecteAutoocupat.objects.all()]
    """

    # TODO: do we need a template to create a CSV file?
    t = loader.get_template("alta_socies/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    return response


class ProcesAltaAutoocupatDetailView(DetailView):
    """Vista resum per visualitzar els detalls
       d'un procés d'alta quan no es vol editar,
       o quan no es té permís per editar-lo
       p. ex. quan el vol consultar algú que no
       n'és el responsable d'alta"""

    model = ProcesAltaAutoocupat
    context_object_name = 'proces'
    template_name = 'alta_socies/autoocupat/detail.html'

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(ProcesAltaAutoocupatDetailView, self).dispatch(*args, **kwargs)


class SessioAcollidaUpdateView(SuccessMessageMixin, FilterResponsablesAltaMixin, UpdateView):
    """Vista per editar les sessions d'acollida"""

    model = SessioAcollida
    template_name = 'alta_socies/formulari_sessio.html'
    success_url = '/'
    success_message = "La sessió d'acollida s'ha actualitzat correctament"
    title = u"Editar sessió d'acollida"
    action_label = u"Desar"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioAcollidaUpdateView, self).dispatch(*args, **kwargs)


class SessioAcollidaListView(ListView):
    """Vista per llistar les sessions d'acollida"""

    context_object_name = 'sessions'
    queryset = _ordenar_queryset_sessions(SessioAcollida.objects.all())
    template_name = 'alta_socies/llistat_sessions.html'

    title = u"Llistat de sessions d'acollida"
    edit_url = 'alta_socies:editar_sessio_acollida'

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioAcollidaListView, self).dispatch(*args, **kwargs)


class SessioAcollidaCreateView(SuccessMessageMixin, FilterResponsablesAltaMixin, CreateView):
    """Vista per crear sessions d'acollida"""

    model = SessioAcollida
    template_name = 'alta_socies/formulari_sessio.html'
    form_class = modelform_factory(SessioAcollida,
                                   widgets={"data": SplitDateTimeWidget},
                                   fields=['data', 'ubicacio', 'duracio', 'responsable_alta'])
    success_url = '/'
    success_message = "La sessió d'acollida ha sigut creada correctament"
    title = u"Crear sessió d'acollida"
    action_label = u"Crear"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioAcollidaCreateView, self).dispatch(*args, **kwargs)


class SessioMonedaUpdateView(SuccessMessageMixin, FilterResponsablesAltaMixin, UpdateView):
    """Vista per editar les sessions de moneda social"""

    model = SessioMoneda
    template_name = 'alta_socies/formulari_sessio.html'
    success_url = '/'
    success_message = "La sessió de moneda social s'ha actualitzat correctament"
    title = u"Editar sessiò de moneda social"
    action_label = u"Desar"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioMonedaUpdateView, self).dispatch(*args, **kwargs)


class SessioMonedaListView(ListView):
    """Vista per llistar les sessions de moneda social"""

    context_object_name = 'sessions'
    queryset = _ordenar_queryset_sessions(SessioMoneda.objects.all())
    template_name = 'alta_socies/llistat_sessions.html'

    title = u"Llistat de sessions de moneda social"
    edit_url = 'alta_socies:editar_sessio_moneda'

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioMonedaListView, self).dispatch(*args, **kwargs)


class SessioMonedaCreateView(SuccessMessageMixin, FilterResponsablesAltaMixin, CreateView):
    """Vista per crear sessions de moneda social"""

    model = SessioMoneda
    template_name = 'alta_socies/formulari_sessio.html'
    form_class = modelform_factory(SessioMoneda,
                                   widgets={"data": SplitDateTimeWidget},
                                   fields=['data', 'ubicacio', 'duracio', 'responsable_alta'])
    success_url = '/'
    success_message = "La sessiò de moneda social ha sigut creada correctament"
    title = u"Crear sessiò de moneda social"
    action_label = u"Crear"

    @method_decorator(login_required)
    @method_decorator(user_passes_test(is_user_responsable_alta))
    def dispatch(self, *args, **kwargs):
        return super(SessioMonedaCreateView, self).dispatch(*args, **kwargs)


@login_required
def generar_sessions(request):

    if request.method == 'POST':
        form = GeneradorSessions(data=request.POST)

        sessions_creades = []

        if form.is_valid():
            tipus_sessio = form.cleaned_data.get('tipus_sessio')
            responsables_alta = form.cleaned_data.get('responsables_alta')
            inici_jornada = form.cleaned_data.get('data_i_hora_inici')
            duracio = form.cleaned_data.get('duracio')
            num_sessions = form.cleaned_data.get('num_sessions')
            ubicacio = form.cleaned_data.get('ubicacio')

            # TODO començar una transacció en aquest punt per poder fer ROLLBACK si es troben sessions ja creades
            # un mínim acceptable penso que seria considerar duplicada una sessió per mateixa data i responsable_alta
            # independentment d'ubicació

            for responsable_alta in responsables_alta:

                sessions_del_responsable_alta = []

                for n in range(0, num_sessions):

                    if tipus_sessio == form.TIPUS_SESSIO_AVALUACIO:
                        sessio = SessioAvaluacio()
                    elif tipus_sessio == form.TIPUS_SESSIO_ALTA:
                        sessio = SessioAlta()
                    else:
                        raise NotImplementedError(u"Tipus de sessió desconeguda: %s" % tipus_sessio)

                    sessio.data = inici_jornada + timedelta(minutes=duracio * n)
                    sessio.ubicacio = ubicacio
                    sessio.responsable_alta = responsable_alta
                    sessio.duracio = duracio
                    sessio.save()

                    sessions_del_responsable_alta.append(sessio)

                sessions_creades.append(
                    {'responsable_alta': responsable_alta, 'sessions': sessions_del_responsable_alta})

    else:
        form = GeneradorSessions()
        sessions_creades = None

    return render(request, 'alta_socies/generador_sessions.html', {
        'form': form,
        'sessions_creades': sessions_creades,
    })


@login_required
@user_passes_test(is_user_responsable_alta)
def proces_alta_projecte_autoocupat_llista_de_tasques(request, id_proces):

    etiqueta_sense_comentaris = u"—"

    proces = get_object_or_404(ProcesAltaAutoocupat, pk=id_proces)

    if proces.pas < 9:
        raise Http404(u"Aquest procés d'alta de projecte autoocupat no està al pas 9!")

    # adreces amb traspas (lloguers i cessions d'ús)
    adreces_amb_traspas = proces.adreces.exclude(traspas=AdrecaProjecteAutoocupat.TIPUS_TRASPAS_CAP)
    traspassos = []
    for a in adreces_amb_traspas:
        if a.traspas == a.TIPUS_TRASPAS_LLOGUER:
            tipus = u"Lloguer"
        elif a.traspas == a.TIPUS_TRASPAS_CESSIO:
            tipus = u"Cessió&nbsp;d'ús"
        else:
            raise SuspiciousOperation()

        traspassos.append(dict(
            tipus=tipus,
            adreca=a,
            comentaris=a.traspas_comentaris_avaluacio or etiqueta_sense_comentaris
        ))

    concessions_activitat = []

    # concessions d'activitats associades a una adreça
    for adreca in proces.adreces.all():
        peticions = adreca.peticions_concessio_activitat.values_list('activitat', 'comentaris_avaluacio')
        peticions = {aid: com for (aid, com) in peticions}
        for activitat_id, activitat_nom in adreca.activitats.values_list('id', 'nom'):
            if activitat_id in peticions:
                concessions_activitat.append({
                    'adreca': adreca,
                    'activitat': activitat_nom,
                    'comentaris': peticions[activitat_id] or etiqueta_sense_comentaris
                })

    # concessions d'activitats no vinculades a cap adreça
    peticions = proces.peticions_concessio_activitat
    peticions = peticions.filter(adreca__isnull=True).values_list('activitat', 'comentaris_avaluacio')
    peticions = {aid: com for (aid, com) in peticions}
    for activitat in proces.altres_activitats.all():
        if activitat.id in peticions:
            concessions_activitat.append({
                'adreca': None,
                'activitat': activitat.nom,
                'comentaris': peticions[activitat.id] or etiqueta_sense_comentaris
            })

    # assegurances d'adreces i activitats vinculades a adreça
    assegurances_adreca = []
    for adreca in proces.adreces.all():
        peticio = proces.peticions_asseguranca.filter(adreca=adreca, activitat__isnull=True)
        if peticio.exists():
            assegurances_adreca.append({
                'adreca': adreca,
                'activitat': None,
                'comentaris': peticio[0].comentaris_avaluacio or etiqueta_sense_comentaris
            })

        for activitat in adreca.activitats.all():
            peticio = proces.peticions_asseguranca.filter(adreca=adreca, activitat=activitat)
            if peticio.exists():
                assegurances_adreca.append({
                    'adreca': adreca,
                    'activitat': activitat,
                    'comentaris': peticio[0].comentaris_avaluacio or etiqueta_sense_comentaris
                })

    # assegurances d'activitat no vinculades a cap adreça
    assegurances_altres_activitats = []
    for activitat in proces.altres_activitats.all():
        peticio = proces.peticions_asseguranca.filter(adreca__isnull=True, activitat=activitat)
        if peticio.exists():
            assegurances_altres_activitats.append({
                'adreca': None,
                'activitat': activitat,
                'comentaris': peticio[0].comentaris_avaluacio or etiqueta_sense_comentaris
            })

    # tots els elements que calen per emplenar la plantilla, recollits a "data"
    data = {
        'proces': proces,
        'traspassos': traspassos,
        'concessions': concessions_activitat,
        'assegurances_adreca': assegurances_adreca,
        'assegurances_altres_activitats': assegurances_altres_activitats,
        'NO_APLICA': etiqueta_sense_comentaris,
    }

    return render(
        request,
        'alta_socies/autoocupat/document-llista_de_tasques.html',
        data
    )


@login_required
@user_passes_test(is_user_responsable_alta)
def sessions_del_mes(request, tipus, year=None, month=None):

    # TODO refactor treure els tipus de sessió d'aqui :)
    # TODO — keep in sync with alta_socies.forms.GeneradorSessions
    TIPUS_SESSIO_AVALUACIO = 'avaluacio'
    TIPUS_SESSIO_ALTA = 'alta'
    TIPUS_DE_SESSIONS = {
        TIPUS_SESSIO_AVALUACIO: u"Sessions d'avaluació",
        TIPUS_SESSIO_ALTA: u"Sessions d'alta",
    }

    cal = calendar.Calendar()

    if year and month:
        try:
            today = date(int(year), int(month), 1)
        except:
            raise Http404
    else:
        today = date.today()

    base = None

    if tipus == TIPUS_SESSIO_AVALUACIO:
        base = SessioAvaluacio
    elif tipus == TIPUS_SESSIO_ALTA:
        base = SessioAlta
    else:
        # TODO perquè haig d'explicitar que l'string és unicode??
        raise Http404(u"Tipus de sessió desconeguda: %s" % tipus)

    year = today.year
    month = today.month
    month_name = date(year, month, 1).strftime('%B')
    plain_weeks = cal.monthdatescalendar(year, month)

    weeks = []

    for w in plain_weeks:
        week = []
        for d in w:
            events = base.objects.filter(data__gte=d).filter(data__lt=d + timedelta(days=1))
            week.append(dict(date=d, events=events))
        weeks.append(week)

    return render(request, 'alta_socies/sessions_del_mes.html', {
        'tipus':      TIPUS_DE_SESSIONS[tipus],
        'year':       year,
        'month':      month,
        'month_name': month_name,
        'weeks':      weeks,
        'dow_names':  dow_names
    })


@login_required
@user_passes_test(is_user_responsable_alta)
def sessions_acollida_recents(request, des_de=7):

    fins = now()
    des_de = fins - timedelta(days=des_de)

    sessions = SessioAcollida.objects.filter(data__gt=des_de, data__lte=fins)

    return render(request, 'alta_socies/sessions_acollida_recents.html', {
        'sessions': sessions,
        'des_de':   des_de,
        'fins':     fins,
    })


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_proces_alta_autoocupat(request):

    if len(request.GET):
        criteri = request.GET
    else:
        criteri = dict(
                    per_responsable=request.user.id,
                    per_estat=ProcesAltaAutoocupat.RESOLUCIO_EN_CURS
        )

    form = FormulariCercaProcesAltaAutoocupat(initial=criteri)
    resultats, cerca = _buscar_processos_alta_autoocupat(criteri)

    return render(
        request,
        'alta_socies/autoocupat/llistat.html',
        dict(
            form=form,
            resultats=resultats,
            id_usuari=request.user.id
        )
    )


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_proces_alta_projecte_autoocupat_revisio_descripcions(request):

    processos = ProcesAltaAutoocupat.objects.filter(pas__gte=2)

    return render(
        request,
        'alta_socies/autoocupat/llistat_revisio_descripcions.html',
        {
            'processos': processos,
        }
    )


@login_required
@user_passes_test(is_user_responsable_alta)
def estadistiques_proces_alta_projecte_autoocupat(request):

    # fem un recompte dels processos d'alta "en curs" que hi ha a cada pas
    recomptes = ProcesAltaAutoocupat.objects.filter(resolucio=ProcesAltaAutoocupat.RESOLUCIO_EN_CURS)
    recomptes = recomptes.values('pas').annotate(recompte=Count('pas')).order_by('pas')
    recomptes = {pas: recompte for (pas, recompte) in recomptes.values_list('pas', 'recompte')}

    rebutjats = ProcesAltaAutoocupat.objects.filter(resolucio=ProcesAltaAutoocupat.RESOLUCIO_REBUTJAT).count()
    acceptats = ProcesAltaAutoocupat.objects.filter(resolucio=ProcesAltaAutoocupat.RESOLUCIO_ACCEPTAT).count()

    recompte_per_pas = []
    for p in range(1, max(ProcesAltaAutoocupat.PASSOS.keys()) + 1):
        r = None
        if p in recomptes:
            r = recomptes[p]
        recompte_per_pas.append({'pas': p, 'recompte': r})

    recompte_per_pas.append(dict(pas=ProcesAltaAutoocupat.RESOLUCIO_REBUTJAT, recompte=rebutjats))
    recompte_per_pas.append(dict(pas=ProcesAltaAutoocupat.RESOLUCIO_ACCEPTAT, recompte=acceptats))

    # llistat de responsables i/o usuaries que tenen algun procés d'alta, amb recompte
    responsables = Group.objects.get(name=auth_groups.RESPONSABLES_ALTA).user_set.annotate(
        recompte=Count('procesaltaautoocupat'))

    id_responsables = responsables.values_list('id', flat=True)

    no_responsables = User.objects.all().exclude(id__in=id_responsables)
    no_responsables = no_responsables.annotate(recompte=Count('procesaltaautoocupat'))
    no_responsables = no_responsables.filter(recompte__gt=0)

    from operator import attrgetter
    from itertools import chain
    usuaries_amb_recompte = sorted(
        chain(responsables, no_responsables),
        key=attrgetter('recompte'))

    usuaries_amb_recompte_classificades = []
    for u in usuaries_amb_recompte:
        u.es_responsable = u.id in id_responsables
        usuaries_amb_recompte_classificades.append(u)

    return render(
        request,
        'alta_socies/autoocupat/estadistiques.html',
        {
            'recompte_per_pas': recompte_per_pas,
            'responsables_amb_recompte': usuaries_amb_recompte_classificades,
        },
        context_instance=RequestContext(request))


# TODO Això no és cap "view"
def extreure_peticions_concessio_activitat(post):

    peticions = {}
    comentaris = {}
    for k, v in post.iteritems():
        if k.startswith('demana_concessio_') and v == u'on':
            key = k[17:]
            (adr_id, act_id) = map(int, key.split('_'))
            if adr_id == -1:
                adr_id = None
            peticions[key] = {
                'adreca_id': adr_id,
                'activitat_id': act_id,
            }
        elif k.startswith('comentaris_concessio_'):
            key = k[21:]
            comentaris[key] = v

    for k, v in comentaris.iteritems():
        if v and k in peticions:
            peticions[k]['comentaris'] = v

    return peticions.values()


# TODO Això no és cap "view"
def extreure_peticions_asseguranca(post):

    peticions = {}
    comentaris = {}
    for k, v in post.iteritems():
        if k.startswith('peticio_') and v == u'on':
            key = k[8:]
            (adr_id, act_id) = map(int, key.split('_'))
            if adr_id == -1:
                adr_id = None
            if act_id == -1:
                act_id = None
            peticions[key] = {
                'adreca_id': adr_id,
                'activitat_id': act_id,
            }
        elif k.startswith('comentaris_'):
            key = k[11:]
            comentaris[key] = v

    for k, v in comentaris.iteritems():
        if v and k in peticions:
            peticions[k]['comentaris'] = v

    return peticions.values()


@login_required
@user_passes_test(is_user_responsable_alta)
def proces_alta_projecte_autoocupat(request, id_proces=None, pas=None):

    user = request.user
    comanda = request.POST.get('anem_a') # retorna None si request.method=='GET' però en aquest cas no es fa servir
    extra_template_variables = {}

    if id_proces:

        proces = get_object_or_404(ProcesAltaAutoocupat, pk=id_proces)

        if user != proces.responsable:
            # TODO use the messaging framework (thanks @tati!) to inform of the redirection
            return redirect('alta_socies:resum_proces_alta_projecte_autoocupat', pk=proces.id)

        if not pas:
            pas = proces.pas
        else:
            # no deixem anar a passos posteriors, en cap cas! :)
            pas = int(pas)
            if pas > proces.pas:
                # TODO use the messaging framework (thanks @tati!) to inform of the redirection
                # TODO again, raise the issue of 'id' vs 'pk'
                return redirect('alta_socies:resum_proces_alta_projecte_autoocupat', pk=proces.id)

    else:
        proces = ProcesAltaAutoocupat()
        pas = proces.pas

    passos = ProcesAltaAutoocupat.PASSOS
    breadcrumbs = []
    for n, p in passos.iteritems():
        c = 'bc-futur'
        if n < proces.pas:
            c = 'bc-completat'
        elif n <= proces.pas_maxim_assolit:
            c = 'bc-reculat'

        if n == pas:
            m = u"\u25B2"
        else:
            m = ''

        breadcrumbs.append({
            'n': n,
            'nom': p['nom'],
            'css': c,
            'mark': m
        })

    destinacio_aqui = 'aqui'
    destinacio_seguent = 'seguent'
    destinacio_anterior = 'anterior'
    destinacio_nou = 'nou'

    def transicio():

        if comanda not in (destinacio_aqui, destinacio_seguent,
                           destinacio_anterior, destinacio_nou):
            raise SuspiciousOperation()

        # aquest procés ha avançat, encara que ens volguem quedar
        # al mateix pas, ir al anterior o marxar a un procés nou
        pas_destinacio = pas + 1

        proces.enregistra_canvi_i_salta_i_desa(user, pas, pas_destinacio)

        if comanda == destinacio_nou:
            return redirect('alta_socies:proces_alta_projecte_autoocupat')

        if comanda == destinacio_aqui:
            pas_destinacio -= 1
        elif comanda == destinacio_anterior:
            pas_destinacio -= 2

        return redirect(
            'alta_socies:proces_alta_projecte_autoocupat',
            id_proces=proces.id,
            pas=pas_destinacio
        )

    def vista_seleccio_persones(accio, rol_a_assignar, seleccio_inicial,
                                persones_a_excloure=[], camps_obligatoris=None,
                                escollir_minim=0, escollir_maxim=0):
        """
        Mecanisme per gestionar l'assignacio de persones a un rol; De moment serveix per sòcies afins, sòcia
        mentora, i aviat servirà també per membres de referència.t
        :param rol_a_assignar: text que ha de sortir al títol de la llista de persones seleccionades
        :param seleccio_inicial: dict {Persona.id: Persona. tots_els_detalls, ...} de persones inicialment incloses
        al rol; Nomès es té en compte aquest paràmetre quan la vista s'executa per atendre una petició HTTP GET
        :param persones_a_excloure: Array de Persona.id que no es poden seleccionar
        :param camps_obligatoris: llista de noms dels camps del formulari que han d'estar «informats» per poder
        afegir una persona a la llista.
        :param escollir_minim: mínim de persones que han d'estar assignades al rol
        :param escollir_maxim: màxim de persones que poden estar assignades al rol (0 vol dir que "no hi ha límit")
        :return: dict amb els elements que calen per muntar el template, aka "extra_template_variables", o None si s'ha
        acabat el procés de selecció, i per tant el «caller» ha de fer "return transicio()" per anar a on calgui.
        """
        # TODO comprovar si es fa servir encara alguna cosa de l'outer scope que s'hauria de passar com a paràmetre!
        accio_modificar_persona = 'modificar_persona'
        accio_crear_persona = 'crear_persona'
        accio_cercar_persona = 'cercar_persona'
        error = None
        _persona = None

        if request.method == 'POST':

            escollides = request.POST.get('persones_escollides')  # TODO aquest id hauria de venir del form
            if escollides:
                escollides = map(int, escollides.split(','))
            else:
                escollides = []

            if accio == accio_modificar_persona:
                _persona = get_object_or_404(Persona, id=request.POST.get('id_persona'))  # TODO aquest id hauria de venir del form

            elif accio == accio_crear_persona:
                _persona = Persona()

            if _persona:
                _form = FormulariCercaActualitzacioSeleccioPersones(data=request.POST, instance=_persona, camps_obligatoris=camps_obligatoris)
                if _form.is_valid():
                    _persona = _form.save()
                    # manera fàcil de buidar formulari; despres de crear o actualitzar s'afegeix i tornem a mode cerca
                    _form = FormulariCercaActualitzacioSeleccioPersones(camps_obligatoris=camps_obligatoris)
                    accio = accio_cercar_persona # això serveix per que es torni a mostrar el form de cercar

                else:
                    _persona = None

            else:

                if escollir_maxim and len(escollides) > escollir_maxim:
                    if escollir_maxim == 1:
                        error = u"No es pot escollir més d'una persona per aquest rol."
                    else:
                        error = u"No es poden escollir més de %d persones per aquest rol." % escollir_maxim

                elif len(escollides) < escollir_minim:
                    if escollir_minim == 1:
                        error = u"Com a mínim s'ha d'escollir una persona per aquest rol"
                    else:
                        error = u"Com a mínim s'han d'escollir %d persones per aquest rol" % escollir_minim

                if not error:

                    nova_llista = []
                    for _id_persona in escollides:
                        nova_llista.append(Persona.objects.get(pk=_id_persona))

                    return dict(seleccio_definitiva=nova_llista)

                else:

                    _form = FormulariCercaActualitzacioSeleccioPersones(data=request.POST, camps_obligatoris=camps_obligatoris)

            # si el POST no ens treu d'aqui (hi havia errors) hem de preservar la selecció
            # turns result into dict {id: value, id:value, ... }
            seleccio_inicial = {
                _id_persona: Persona.objects.get(id=_id_persona).tots_els_detalls
                for _id_persona in escollides}

            if _persona:
                assert(_persona.id not in escollides)
                seleccio_inicial[_persona.id] = _persona.tots_els_detalls

        else:

            _form = FormulariCercaActualitzacioSeleccioPersones(camps_obligatoris=camps_obligatoris)

        _extra_template_variables = {
            'f':                   _form,
            'persones_a_excloure': json.dumps(persones_a_excloure),
            'persones_inicials':   json.dumps(seleccio_inicial),  # TODO això hauria de passar dins de 'form'
            'MODIFICAR_PERSONA':   accio_modificar_persona,
            'CREAR_PERSONA':       accio_crear_persona,
            'comanda':             accio,
            'error':               error,
            'rol_a_assignar':      rol_a_assignar,
            'escollir_minim':      escollir_minim,
            'escollir_maxim':      escollir_maxim,
        }

        return dict(extra_template_variables=_extra_template_variables)

    def assignar_cooperativa():
        if proces.es_inclus_lleugerament_firaire():
            proces.cooperativa_assignada = Cooperativa.objects.get(pk=2)  # XIPU
        else:
            proces.cooperativa_assignada = Cooperativa.objects.get(pk=1)  # Interprofessionals

    if pas == 0:

        if request.method == 'POST':
            form = FormulariProjecte(data=request.POST, instance=proces)
            if form.is_valid():
                proces = form.save(commit=False)
                return transicio()
        else:
            form = FormulariProjecte(instance=proces)

        extra_template_variables = {
            'form': form,
        }

    elif pas == 1:

        membres_de_referencia = None
        if request.method == 'GET':
            membres_de_referencia = proces.membres_de_referencia.values_list('id', 'tots_els_detalls')
            # turns result into dict {id: value, id:value, ... }
            membres_de_referencia = {pk: text for (pk, text) in membres_de_referencia}

        resultat = vista_seleccio_persones(
            accio=comanda,
            rol_a_assignar=u"Membres de referència",
            seleccio_inicial=membres_de_referencia,
            escollir_minim=1,
            camps_obligatoris=['nom', 'cognom1', 'telefon'])

        if 'seleccio_definitiva' in resultat:
            proces.membres_de_referencia = resultat['seleccio_definitiva']
            return transicio()
        else:
            assert('extra_template_variables' in resultat)
            extra_template_variables = resultat['extra_template_variables']

    elif pas == 2:

        if request.method == 'POST':

            form = FormulariDadesGeneralsProjecte(data=request.POST, instance=proces)
            if form.is_valid():
                proces = form.save(commit=False)
                assignar_cooperativa()

                return transicio()

        else:

            form = FormulariDadesGeneralsProjecte(instance=proces)

        extra_template_variables = {
            'form': form,
            'proces': proces
        }

    elif pas == 3:

        MembresFormSet = formset_factory(FormulariDetallsMembreDeReferencia, extra=0)

        if request.method == 'POST':

            formset = MembresFormSet(data=request.POST)
            if formset.is_valid():

                membres_queryset = proces.membres_de_referencia

                for form in formset:

                    try:
                        m = membres_queryset.get(pk=form.cleaned_data.get('id'))
                    except:
                        raise SuspiciousOperation

                    m.email = form.cleaned_data.get('email')

                    m.tipus_document_dni = form.cleaned_data.get('tipus_document_dni')
                    m.dni = form.cleaned_data.get('dni')
                    m.compte_integralces = form.cleaned_data.get('compte_integralces')

                    contribucions = []
                    fc = form.cleaned_data.get('contribucions')
                    for c in fc:
                        contribucions.append(get_object_or_404(ContribucioCIC, pk=int(c)))

                    habilitats = []
                    fh = form.cleaned_data.get('habilitats')
                    for h in fh:
                        habilitats.append(get_object_or_404(Habilitat, pk=int(h)))

                    hc = m.habilitats_contribucions
                    if not hc:
                        hc = HabilitatsContribucionsPersona(persona=m)
                        hc.save()

                    hc.habilitats = habilitats
                    hc.contribucions = contribucions
                    hc.detalls_habilitats = form.cleaned_data.get('detalls_habilitats')
                    hc.save()
                    m.save()

                return transicio()

        else:

            membres = proces.membres_de_referencia.all()
            initial = []
            for m in membres:

                detalls = {
                    'id': m.id,
                    'nom_sencer': m.nom_sencer,
                    'email': m.email,
                    'dni': m.dni,
                    'tipus_document_dni': m.tipus_document_dni,
                    'compte_integralces': m.compte_integralces,
                }

                hc = m.habilitats_contribucions
                if hc:
                    detalls['habilitats'] = [k[0] for k in hc.habilitats.all().values_list('id')]
                    detalls['contribucions'] = [k[0] for k in hc.contribucions.all().values_list('id')]
                    detalls['detalls_habilitats'] = hc.detalls_habilitats

                initial.append(detalls)

            formset = MembresFormSet(initial=initial)

        extra_template_variables = {
            'formset_membres_referencia': formset,
        }

    elif pas == 4:

        AdrecesFormSet = modelformset_factory(AdrecaProjecteAutoocupat,
                                              form=FormulariAdrecaProjecteAutoocupat,
                                              extra=1,
                                              min_num=1,
                                              can_delete=True)

        if request.method == 'POST':

            formset = AdrecesFormSet(data=request.POST)

            if formset.is_valid():

                formset.save()

                proces.adreces.add(*formset.new_objects)
                proces.adreces.remove(*formset.deleted_objects)

                return transicio()

        else:

            adreces = proces.adreces.all()
            formset = AdrecesFormSet(queryset=adreces)

        extra_template_variables = {
            'formset': formset,
        }

    elif pas == 5:

        if proces.tipus == ProcesAltaAutoocupat.TIPUS_AUTOOCUPAT_FIRAIRE:
            FormClass = FormulariAltresActivitatsProjecteAutoocupatFiraire
        else:
            FormClass = FormulariAltresActivitatsProjecteAutoocupatNoFiraire

        if request.method == 'POST':

            form = FormClass(data=request.POST, instance=proces)

            if form.is_valid():
                proces = form.save(commit=False)
                assignar_cooperativa()
                form.save_m2m()

                return transicio()

        else:

            form = FormClass(instance=proces)

        extra_template_variables = {
            'form': form,
        }

    elif pas == 6:

        activitats_per_adreca = []

        # activitats associades a una adreça
        for adreca in proces.adreces.all():
            peticions = {}
            if request.method == 'GET':
                peticions = adreca.peticions_concessio_activitat.values_list('activitat', 'comentaris_avaluacio')
                peticions = {aid: com for (aid, com) in peticions}
            for activitat_id, activitat_nom in adreca.activitats.values_list('id', 'nom'):
                fila = {
                    'adreca_id': adreca.id,
                    'adreca_designacio': adreca.designacio,
                    'activitat_id': activitat_id,
                    'activitat_nom': activitat_nom
                }
                if activitat_id in peticions:
                    fila['checked'] = True
                    fila['comentaris'] = peticions[activitat_id] or u""
                activitats_per_adreca.append(fila)

        # activitats sense adreça
        peticions = {}
        if request.method == 'GET':
            peticions = proces.peticions_concessio_activitat
            peticions = peticions.filter(adreca__isnull=True).values_list('activitat', 'comentaris_avaluacio')
            peticions = {aid: com for (aid, com) in peticions}
        for activitat in proces.altres_activitats.all():
            fila = {
                'adreca_id': -1,
                'adreca_designacio': u"(cap)",
                'activitat_id': activitat.id,
                'activitat_nom': activitat.nom
            }
            if activitat.id in peticions:
                fila['checked'] = True
                fila['comentaris'] = peticions[activitat.id] or u""
            activitats_per_adreca.append(fila)

        if request.method == 'POST':

            proces.peticions_concessio_activitat.all().delete()
            peticions = extreure_peticions_concessio_activitat(request.POST)
            for peticio in peticions:
                act = Activitat.objects.get(pk=peticio['activitat_id'])
                if peticio['adreca_id']:
                    adr = AdrecaProjecteAutoocupat.objects.get(pk=peticio['adreca_id'])
                else:
                    adr = None
                if 'comentaris' in peticio:
                    com = peticio['comentaris']
                else:
                    com = None
                PeticioConcessioActivitat(
                    proces_alta_autoocupat=proces,
                    activitat=act,
                    adreca=adr,
                    comentaris_avaluacio=com
                ).save()

            return transicio()

        extra_template_variables = {
            'activitats_per_adreca': activitats_per_adreca,
        }

    elif pas == 7:

        if request.method == 'POST':

            proces.peticions_asseguranca.all().delete()
            peticions = extreure_peticions_asseguranca(request.POST)
            for peticio in peticions:
                adreca = None
                if peticio['adreca_id']:
                    adreca = proces.adreces.get(pk=peticio['adreca_id'])
                activitat = None
                if peticio['activitat_id']:
                    activitat = Activitat.objects.get(pk=peticio['activitat_id'])
                com = None
                if 'comentaris' in peticio:
                    com = peticio['comentaris']

                PeticioAsseguranca(
                    proces_alta_autoocupat=proces,
                    activitat=activitat,
                    adreca=adreca,
                    comentaris_avaluacio=com
                ).save()

            return transicio()

        adreces = []
        for adreca in proces.adreces.all():
            adr_item = {'id': adreca.id, 'designacio': adreca.designacio}
            peticio = proces.peticions_asseguranca.filter(adreca=adreca, activitat__isnull=True)
            if peticio.exists():
                adr_item['peticio'] = True
                adr_item['comentaris'] = peticio[0].comentaris_avaluacio or u""

            activitats = []
            for activitat in adreca.activitats.all():
                act_item = {'id': activitat.id, 'nom': activitat.nom}
                peticio = proces.peticions_asseguranca.filter(adreca=adreca, activitat=activitat)
                if peticio.exists():
                    act_item['peticio'] = True
                    act_item['comentaris'] = peticio[0].comentaris_avaluacio or u""
                activitats.append(act_item)

            adr_item['activitats'] = activitats
            adreces.append(adr_item)

        altres_activitats = []
        for activitat in proces.altres_activitats.all():
            act_item = {'id': activitat.id, 'nom': activitat.nom, };
            peticio = proces.peticions_asseguranca.filter(adreca__isnull=True, activitat=activitat)
            if peticio.exists():
                act_item['peticio'] = True
                act_item['comentaris'] = peticio[0].comentaris_avaluacio or u""

            altres_activitats.append(act_item)

        extra_template_variables = {
            'adreces': adreces,
            'altres_activitats': altres_activitats,
        }

    elif pas == 8:

        if request.method == 'POST':

            form = FormulariFormaPagamentQuotesAutoocupat(data=request.POST, instance=proces)
            if form.is_valid():
                proces = form.save(commit=False)
                return transicio()

        else:

            form = FormulariFormaPagamentQuotesAutoocupat(instance=proces)

        extra_template_variables = {
            'form': form,
        }

    elif pas == 9:

        if request.method == 'POST':
            return transicio()

        membres = proces.membres_de_referencia.values('id', 'nom_sencer', 'email')

        extra_template_variables = {
            'membres': membres,
            'proces': proces,
        }

    elif pas == 10:

        membres = proces.membres_de_referencia.values('id', 'nom_sencer', 'email')

        if proces.sessio_moneda_realitzada:
            for m in membres:
                # aqui s'assumeix que no es modifica la llista de membres de referencia
                # despres d'haver passat aquest pas; Si fem 'readonly' els passos completats
                # no caldria aquesta feina
                m['checked'] = True

        if request.method == 'POST':

            form_projecte = FormulariAltaProjecteAutoocupatSessioMoneda(data=request.POST, instance=proces)
            if form_projecte.is_valid():
                c = 0
                for m in membres:
                    m_id = m['id']
                    if request.POST.get('assistent_%d' % m_id, None):
                        m['checked'] = True
                        c += 1

                proces.sessio_moneda_realitzada = (c == len(membres))

                if proces.sessio_moneda_realitzada:
                    return transicio()

        else:

            form_projecte = FormulariAltaProjecteAutoocupatSessioMoneda(instance=proces)

        extra_template_variables = {
            'form_projecte': form_projecte,
            'membres':       membres,
            'hi_son_tots':   proces.sessio_moneda_realitzada,
        }

    elif pas == 11:

        AdrecesFormSet = modelformset_factory(AdrecaProjecteAutoocupat,
                                              form=FormulariAltaProjecteAutoocupatCessioUs,
                                              extra=0)

        if request.method == 'POST':

            formset = AdrecesFormSet(data=request.POST)

            if formset.is_valid():
                formset.save()
                return transicio()

        else:

            adreces = proces.adreces.filter(traspas=AdrecaProjecteAutoocupat.TIPUS_TRASPAS_CESSIO)
            formset = AdrecesFormSet(queryset=adreces)

        extra_template_variables = {
            'formset': formset,
        }

    elif pas == 12:

        AdrecesFormSet = modelformset_factory(AdrecaProjecteAutoocupat,
                                              form=FormulariAltaProjecteAutoocupatLloguer,
                                              extra=0)

        if request.method == 'POST':

            formset = AdrecesFormSet(data=request.POST)

            if formset.is_valid():
                formset.save()
                return transicio()

        else:

            adreces = proces.adreces.filter(traspas=AdrecaProjecteAutoocupat.TIPUS_TRASPAS_LLOGUER)
            formset = AdrecesFormSet(queryset=adreces)

        extra_template_variables = {
            'formset': formset,
        }

    elif pas == 13:

        PeticionsFormSet = modelformset_factory(PeticioConcessioActivitat,
                                                form=FormulariAltaConcessioActivitat,
                                                extra=0)

        if request.method == 'POST':

            formset = PeticionsFormSet(data=request.POST)

            if formset.is_valid():
                formset.save()
                return transicio()

        else:

            peticions = proces.peticions_concessio_activitat.all().order_by('adreca', 'activitat')
            formset = PeticionsFormSet(queryset=peticions)

        extra_template_variables = {
            'formset': formset,
        }

    elif pas == 14:

        PeticionsFormSet = modelformset_factory(PeticioAsseguranca,
                                                form=FormulariAltaAsseguranca,
                                                extra=0)

        if request.method == 'POST':

            formset = PeticionsFormSet(data=request.POST)

            if formset.is_valid():
                formset.save()
                return transicio()

        else:

            peticions = proces.peticions_asseguranca.all().order_by('adreca', 'activitat')
            formset = PeticionsFormSet(queryset=peticions)

        extra_template_variables = {
            'formset': formset,
        }

    elif pas == 15:

        if request.method == 'POST':

            form = FormulariAltaQuotes(data=request.POST, instance=proces)

            if form.is_valid():
                proces = form.save(commit=False)
                proces.quota_alta_quantitat = proces.get_quota(proces.QUOTA_ALTA, proces.quota_alta_forma_pagament)
                proces.quota_avancada_quantitat = proces.get_quota(proces.QUOTA_TRIMESTRAL_AVANCADA, proces.quota_avancada_forma_pagament)
                proces.quota_alta_data_hora_pagament = proces.quota_avancada_data_hora_pagament = now()

                return transicio()

        else:

            form = FormulariAltaQuotes(instance=proces)

        algun_pagament_en_ecos = (proces.quota_alta_forma_pagament == proces.FORMA_PAGAMENT_ECO or
                                  proces.quota_avancada_forma_pagament == proces.FORMA_PAGAMENT_ECO)

        extra_template_variables = {
            'form': form,
            'algun_pagament_en_ecos': algun_pagament_en_ecos,
        }

    elif pas == 16:

        membres_de_referencia = proces.membres_de_referencia.values_list('id', flat=True)
        # turn django.db.models.query.ValuesListQuerySet into simple array of integers:
        membres_de_referencia = [x for x in membres_de_referencia]

        socies_afins_addicionals = None
        if request.method == 'GET':
            socies_afins_addicionals = proces.socies_afins_addicionals.values_list('id', 'tots_els_detalls')
            # turns result into dict {id: value, id:value, ... }
            socies_afins_addicionals = {pk: text for (pk, text) in socies_afins_addicionals}

        resultat = vista_seleccio_persones(
            accio=comanda,
            rol_a_assignar=u"Sòcies afins addicionals",
            seleccio_inicial=socies_afins_addicionals,
            persones_a_excloure=membres_de_referencia,
            camps_obligatoris=['nom', 'cognom1', 'dni'])

        if 'seleccio_definitiva' in resultat:
            proces.socies_afins_addicionals = resultat['seleccio_definitiva']
            return transicio()
        else:
            assert('extra_template_variables' in resultat)
            extra_template_variables = resultat['extra_template_variables']

    elif pas == 17:

        membres_de_referencia = proces.membres_de_referencia.values_list('id', flat=True)
        # turn django.db.models.query.ValuesListQuerySet into simple array of integers:
        membres_de_referencia = [x for x in membres_de_referencia]

        mentora = None
        if request.method == 'GET':
            mentora = proces.socia_mentora
            if mentora:
                mentora = {mentora.id: mentora.tots_els_detalls}
            else:
                mentora = {}

        resultat = vista_seleccio_persones(
            accio=comanda,
            rol_a_assignar=u"Sòcia mentora",
            seleccio_inicial=mentora,
            persones_a_excloure=membres_de_referencia,
            camps_obligatoris=['nom', 'cognom1', 'dni'],
            escollir_maxim=1,
            escollir_minim=0)

        if 'seleccio_definitiva' in resultat:
            if len(resultat['seleccio_definitiva']):
                proces.socia_mentora = resultat['seleccio_definitiva'][0]
            else:
                proces.socia_mentora = None

            return transicio()

        else:
            assert('extra_template_variables' in resultat)
            extra_template_variables = resultat['extra_template_variables']

    elif pas == 18:

        error_falten_fotos = False

        if request.method == 'POST':

            volen_marxar = comanda != destinacio_aqui
            form = FormulariFotoProjecteAutoocupatFiraire(request.POST, request.FILES)
            formulari_valid = form.is_valid()
            formulari_buit = not (form.cleaned_data.get('nom') or form.cleaned_data.get('detalls') or request.FILES)

            firaire = proces.tipus == proces.TIPUS_AUTOOCUPAT_FIRAIRE

            if not formulari_buit and formulari_valid:

                foto = form.save(commit=False)
                foto.projecte = proces
                foto.save()

                filename = foto.imatge.file.name
                if create_thumbnail(filename):
                    elements = foto.imatge.url.split('/')
                    elements.insert(len(elements) - 1, settings.THUMBNAILS_SUBDIR)
                    foto.url_miniatura = '/'.join(elements)
                    foto.save()

            fotos = proces.fotos_parada_firaire.all()

            if formulari_buit or formulari_valid:
                eliminar = form.cleaned_data.get('imatges_a_eliminar')
                if eliminar:
                    for fid in eliminar.split(','):
                        proces.fotos_parada_firaire.get(pk=fid).delete();

                if not volen_marxar or (not firaire or len(fotos) >= 2):
                    return transicio()

            error_falten_fotos = firaire and volen_marxar and len(fotos) < 2

            if formulari_buit or formulari_valid:
                form = FormulariFotoProjecteAutoocupatFiraire()

        else:

            fotos = proces.fotos_parada_firaire.all()
            form = FormulariFotoProjecteAutoocupatFiraire()

        extra_template_variables = {
            'form': form,
            'fotos': fotos,
            'error_falten_fotos': error_falten_fotos,
            'amplada_marc': settings.THUMBNAIL_MAX_SIZE[0],
            'alcada_marc': settings.THUMBNAIL_MAX_SIZE[1],
        }

    elif pas == 19:

        if request.method == 'POST':

            form = ForulariPas19(data=request.POST, instance=proces)

            if form.is_valid():
                proces = form.save()
                return transicio()

        else:

            form = ForulariPas19(instance=proces)

        extra_template_variables = {
            'form': form,
        }

    elif pas == 20:

        if request.method == 'POST':

            proces.resolucio = ProcesAltaAutoocupat.RESOLUCIO_ACCEPTAT
            proces.data_resolucio = now()
            proces.resolucio_usuaria = user
            proces.save()


            try:
                projecte = ProjecteAutoocupat.objects.create_projecte(proces=proces)
            except Exception as e:
                messages.error(request, e.message)
                return redirect('dashboard_gestio_economica')

            for peticio in proces.peticions_asseguranca.all():
                PolissaAsseguranca.objects.create_polissa(peticio=peticio, projecte=projecte)

            for foto in proces.fotos_parada_firaire.all():
                FotoProjecteAutoocupatFiraire.objects.create(projecte=projecte,
                                                             nom=foto.nom,
                                                             detalls=foto.detalls,
                                                             imatge=foto.imatge,
                                                             url_miniatura=foto.url_miniatura)

            # TODO aquí caldria un resum d'algunes coses; probablement el botó "do it" hauria d'estar al pas 19
            # i el pas 20 seria un resum "imprimible" (+ correu) amb numero COOP + usuaris que poden entrar al portal.

            messages.success(request, "El projecte s'ha creat correctament")

            return redirect('dashboard_gestio_economica')

    # -----------------------------------

    template_variables = dict({

        'id_proces':     id_proces,  # TODO canviar a tots els templates per proces.id i eliminar aquesta variable
        'proces':        proces,
        'pas':           pas,
        'nom_pas':       passos[pas]['nom'],
        'breadcrumbs':   breadcrumbs,

        'ANEM_AQUI':     destinacio_aqui,
        'ANEM_SEGUENT':  destinacio_seguent,
        'ANEM_ANTERIOR': destinacio_anterior,
        'ANEM_NOU':      destinacio_nou,

    }.items() + extra_template_variables.items())

    return render(
        request,
        'alta_socies/autoocupat/%02d-%s.html' % (pas, passos[pas]['template']),
        template_variables
    )


@login_required
@user_passes_test(is_user_supervisora_alta)
def reassignacio_proces_alta_autoocupat(request):

    destinataris = Group.objects.get(name=auth_groups.RESPONSABLES_ALTA).user_set
    form_reassignar = FormulariReassignarProcesAltaAutoocupat(destinataris=destinataris)

    if request.method == 'POST':

        destinatari = User.objects.get(pk=request.POST.get('destinatari'))
        if destinatari.id not in destinataris.values_list('id', flat=True):
            raise u"what the fuck?"  # TODO add proper error handling

        canvis = 0
        for pk in request.POST.getlist('projectes_triats'):
            p = ProcesAltaAutoocupat.objects.get(pk=pk)
            if p.responsable.id != destinatari.id:
                p.responsable = destinatari
                p.save()
                canvis += 1

        if canvis == 1:
            messages.success(request, u"S'ha reassignat un procés d'alta a %s" % destinatari.username)
        elif canvis > 1:
            messages.success(request, u"S'han reassignat %d processos d'alta a %s" % (canvis, destinatari.username))
        else:
            messages.error(request, u"No s'ha hagut de reassignar cap procés d'alta")

    form_cerca = FormulariCercaProcesAltaAutoocupat(data=request.GET)

    resultats, cerca = _buscar_processos_alta_autoocupat(request.GET)

    return render(
        request,
        'alta_socies/autoocupat/reassignacio.html',
        dict(
            form_cerca=form_cerca,
            cerca=cerca,
            projectes=resultats,
            form_reassignar=form_reassignar,
        )
    )


def proces_alta_projecte_collectiu_dades_generals(request):
    """
    View that renders the 1st step of the process to submit
    a registration request for a new ProjecteCollectiu
    instance.
    Step 0: General information.
    Step 1: Specific information.
    Step 2: Affinity Members.
    """

    title = "Dades Generals"
    descripcio = u"Introdueix les dades generals del projecte que vols constituir"
    
    form = FormulariDadesGeneralsProjecteCollectiu(request.POST or None)
    
    if request.method == 'POST':
        if form.is_valid():
            request.session['nom'] = form.cleaned_data['nom']
            request.session['website'] = form.cleaned_data['website']
            request.session['descripcio'] = form.cleaned_data['descripcio']
            request.session['email'] = form.cleaned_data['email']
            request.session['telefon'] = form.cleaned_data['telefon']
            request.session['pais'] = form.cleaned_data['pais']
            request.session['provincia'] = form.cleaned_data['provincia']
            request.session['comarca'] = form.cleaned_data['comarca']
            request.session['poblacio'] = form.cleaned_data['poblacio']
            request.session['codi_postal'] = form.cleaned_data['codi_postal']
            request.session['adreca'] = form.cleaned_data['adreca']
            return redirect('alta_socies:collectiu_dades_concretes')
    return render(request,
                  'alta_socies/formulari_projecte_collectiu_dades_generals.html',
                  { 'form': form,
                    'title': title,
                    'descripcio': descripcio
                })


def proces_alta_projecte_collectiu_dades_concretes(request):
    """
    View that renders the 2nd step of the process to submit
    a registration request for a new ProjecteCollectiu
    instance.
    Step 0: General information.
    Step 1: Specific information.
    Step 2: Affinity members.
    """

    title = "Dades Concretes"
    descripcio = u"Introdueix les dades concretes del projecte que vols constituir"
    
    form = FormulariDadesConcretesProjecteCollectiu(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            request.session['tipus_projecte'] = form.cleaned_data['tipus_projecte']
            request.session['formes_pagament_quota_alta'] = form.cleaned_data['formes_pagament_quota_alta']
            
            return redirect('alta_socies:collectiu_socies_afins')
    return render(request,
                  'alta_socies/formulari_projecte_collectiu_dades_concretes.html',
                  { 'form': form,
                    'title': title,
                    'descripcio': descripcio
                })


def proces_alta_projecte_collectiu_socies_afins(request):
    """
    View that renders the 3rd step of the process to submit
    a registration request for a new ProjecteCollectiu
    instance.
    Step 0: General information.
    Step 1: Specific information.
    Step 2: Affinity members.
    """

    title = "Socies afins"
    descripcio = u"Introdueix les dades de les socies afins del projecte"
    formset_class = modelformset_factory(SociaAfi, extra=9, min_num=1, validate_min=True,
                                         exclude=["usuaria_que_invita",
                                                  "created_at",
                                                  "esta_pendent_de_validacio",
                                                  "validated_at",
                                                  "usuaria_que_valida",
                                                  "projecte"])
                                         
    formset = formset_class(request.POST or None, queryset=SociaAfi.objects.none())

    if request.method == 'POST':
        if formset.is_valid():
            socies_afins = formset.save(commit=False)

            adreca = AdrecaProjecteCollectiu(pais=request.session['pais'],
                                             provincia=request.session['provincia'],
                                             comarca=request.session['comarca'],
                                             poblacio=request.session['poblacio'],
                                             codi_postal=request.session['codi_postal'],
                                             adreca=request.session['adreca'],
                                             ubicacio_especifica="")

            adreca.save()
            projecte = ProjecteCollectiu(nom=request.session['nom'],
                                         website=request.session['website'],
                                         descripcio=request.session['descripcio'],
                                         email=request.session['email'],
                                         telefon=request.session['telefon'],
                                         adreca=adreca,
                                         tipus_projecte=request.session['tipus_projecte'],
                                         quota_alta_forma_pagament=request.session['formes_pagament_quota_alta'],
            )
            projecte.save()
            for socia in socies_afins:
                socia.projecte = projecte
                socia.usuaria_que_invita = request.user
                socia.save()

            messages.success(request, "El projecte s'ha creat correctament, queda pendent de validacio per part d'un rensponsable d'alta")
            return redirect('inici')
    return render(request,
                  'alta_socies/formulari_projecte_collectiu_socies_afins.html',
                  {'formset': formset,
                   'title': title,
                   'descripcio': descripcio
               })


def proces_alta_socia_cooperativa_dades_personals(request):
    """
    View that renders the 1st step of the process to register
    a new SociaCooperativa instance for validation.
    Step 0: Personal Information.
    Step 1: Address Information
    Step 2: Register fee details.
    """
    title = "Dades personals"
    descripcio = "Insereix les teves dades personals"

    form = FormulariDadesPersonalsSociaCooperativa(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            request.session['nom'] = form.cleaned_data['nom']
            request.session['cognom1'] = form.cleaned_data['cognom1']
            request.session['cognom2'] = form.cleaned_data['cognom2']
            request.session['pseudonim'] = form.cleaned_data['pseudonim']
            request.session['email'] = form.cleaned_data['email']
            request.session['telefon'] = form.cleaned_data['telefon']
            request.session['document'] = form.cleaned_data['document']
            request.session['tipus_document'] = form.cleaned_data['tipus_document']
            request.session['te_compte_integral_ces'] = form.cleaned_data['te_compte_integral_ces']
            request.session['compte_integral_ces'] = form.cleaned_data['compte_integral_ces']
            return redirect('alta_socies:alta_socia_cooperativa_dades_adreca')

    return render(request,
                  'alta_socies/formulari_socia_cooperativa_dades_personals.html',
                  {
                      'form': form,
                      'title': title,
                      'descripcio': descripcio,
                  })



def proces_alta_socia_cooperativa_dades_adreca(request):
    """
    View that renders the 2nd step of the process to register
    a new SociaCooperativa instance for validation.
    Step 0: Personal Information.
    Step 1: Address Information
    Step 2: Register fee details.
    """
    title = "Dades de l'Adreça"
    descripcio = "Ompleix les dades relatives a l'adreça"

    form = FormulariDadesAdrecaSociaCooperativa(request.POST or None)
    
    if request.method == 'POST':
        if form.is_valid():
            request.session['pais'] = form.cleaned_data['pais']
            request.session['provincia'] = form.cleaned_data['provincia']
            request.session['comarca'] = form.cleaned_data['comarca']
            request.session['poblacio'] = form.cleaned_data['poblacio']
            request.session['codi_postal'] = form.cleaned_data['codi_postal']
            request.session['adreca'] = form.cleaned_data['adreca']
            return redirect('alta_socies:alta_socia_cooperativa_dades_alta')

    return render(request,
                  'alta_socies/formulari_socia_cooperativa_dades_adreca.html',
                  {
                      'form': form,
                      'title': title,
                      'descripcio': descripcio,
                  })


def proces_alta_socia_cooperativa_dades_alta(request):
    """
    View that renders the 3rd step of the process to register
    a new SociaCooperativa instance for validation.
    Step 0: Personal Information.
    Step 1: Address Information
    Step 2: Register fee details.
    """
    title = "Pagament quota d'alta"
    descripcio = "Tria com vols pagar la quota d'alta"
            
    initial = {'te_compte_integral_ces': request.session.get('te_compte_integral_ces')}
    form = FormulariDadesAltaSociaCooperativa(request.POST or None, initial=initial)

    if request.method == 'POST':
        if form.is_valid():
            request.session['quota_alta'] = form.cleaned_data['quota_alta']
            request.session['habilitats_ids'] = [habilitat.id for habilitat in form.cleaned_data['habilitats']]
            request.session['detalls_habilitats'] = form.cleaned_data['detalls_habilitats']
            request.session['contribucions_ids'] = [contribucio.id for contribucio in form.cleaned_data['contribucions']]

            try:
                with transaction.atomic():

                    persona = Persona(nom=request.session.get('nom'),
                                      cognom1=request.session.get('cognom1'),
                                      cognom2=request.session.get('cognom2'),
                                      pseudonim=request.session.get('pseudonim'),
                                      email=request.session.get('email'),
                                      telefon=request.session.get('telefon'),
                                      dni=request.session.get('document'),
                                      tipus_document_dni=request.session.get('tipus_document'),
                                      compte_integralces=request.session.get('compte_integral_ces'))
                    persona.save()
                    
                    habilitats = [Habilitat.objects.get(id=habilitat_id) for habilitat_id in request.session.get('habilitats_ids')]
                    contribucions = [ContribucioCIC.objects.get(id=contribucio_id) for contribucio_id in request.session.get('contribucions_ids')]
                    detalls_habilitats = request.session.get('detalls_habilitats')
                    
                    h_c_p = HabilitatsContribucionsPersona.objects.create(persona=persona, detalls_habilitats=detalls_habilitats)
                    for habilitat in habilitats:
                        h_c_p.habilitats.add(habilitat)

                    for contribucio in contribucions:
                        h_c_p.contribucions.add(contribucio)

                    adreca = AdrecaSociaCooperativa.objects.create(
                        pais=request.session.get('pais'),
                        provincia=request.session.get('provincia'),
                        comarca=request.session.get('comarca'),
                        poblacio=request.session.get('poblacio'),
                        codi_postal=request.session.get('codi_postal'),
                        adreca=request.session.get('adreca'),
                    )
                        
                    socia = SociaCooperativa(
                        socia=persona,
                        quota_alta_forma_pagament=request.session.get('quota_alta'),  # TODO quota_alta_forma_pagament = quota_alta, really?
                        quota_alta=request.session.get('quota_alta'),
                        adreca=adreca
                    )
                    socia.save()
                    request.session.flush()
                    messages.success(request, "T'has enregistrat correctament, quan haguem confirmat el "
                                              "pagament rebràs una notificació.")
                    return redirect("inici")
            except Exception:
                messages.error(request, "S'ha prodüit un error, és posible que la persona ja existeixi")
                return redirect("inici")

    return render(request,
                  'alta_socies/formulari_socia_cooperativa_dades_alta.html',
                  {
                      'form': form,
                      'title': title,
                      'descripcio': descripcio,
                  })


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_socies_pendents_euro(request):
    """
    View that renders a list of members that
    needs to be validated and that should have paid
    in euros
    """
    title = u"Llistat de aspirants a socies que volen pagar amb euros"
    
    socies = SociaCooperativa.objects.filter(es_validada=False).filter(quota_alta_forma_pagament='euro')

    return render(request,
                  "alta_socies/llistat_socies_pendents.html",
                  { 'title': title,
                    'socies': socies
                })


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_socies_pendents_eco(request):
    """
    View that renders a list of members that
    needs to be validated and that should have paid
    in euros
    """
    title = u"Llistat de aspirants a socies que volen pagar amb ecos"
    
    socies = SociaCooperativa.objects.filter(es_validada=False).filter(quota_alta_forma_pagament='eco')

    return render(request,
                  "alta_socies/llistat_socies_pendents.html",
                  { 'title': title,
                    'socies': socies
                })


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_socies_pendents_temp(request):
    """
    View that renders a list of members that
    needs to be validated and that should have paid
    in time hours
    """
    title = u"Llistat de aspirants a socies que volen pagar amb hores"
    
    socies = SociaCooperativa.objects.filter(es_validada=False).filter(quota_alta_forma_pagament='h')

    return render(request,
                  "alta_socies/llistat_socies_pendents.html",
                  { 'title': title,
                    'socies': socies
                })


@login_required
@user_passes_test(is_user_responsable_alta)
def validar_socia_pendent(request, id_socia=None):
    """
    View that renders the details of a member
    and a mini-form for completing registration process
    """
    title = u"Detalls i validació d'una socia"

    socia = get_object_or_404(SociaCooperativa, pk=id_socia)

    form_class = modelform_factory(SociaCooperativa, fields=("compte_ces_assignat",))

    form = form_class(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            try:
                with transaction.atomic():
                    socia.pagament_rebut = True
                    socia.compte_ces_assignat = form.cleaned_data["compte_ces_assignat"]
                    socia.es_validada = True
                    socia.dada_validacio = now()
                    socia.usuaria_que_valida = request.user
                    socia.save()
                    messages.success(request, "S'ha validat la socia correctament")
                    return redirect('inici')
            except:
                messages.error(request, u"Es imposible registrar la socia per inconsistencies greus en les dades insertades")
                return redirect('inici')
    return render(request,
                  'alta_socies/detalls_i_validacio_socies_pendents.html',
                  {'title': title,
                   'socia': socia,
                   'form': form
               })


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_projectes_pendents_euro(request):
    """
    View that renders a list of projects that
    needs to be validated and that should have paid
    in euros
    """
    title = u"Llistat de aspirants a projectes que volen pagar amb euros"
    
    projectes = ProjecteCollectiu.objects.filter(es_validat=False).filter(quota_alta_forma_pagament='euro')

    return render(request,
                  "alta_socies/llistat_projectes_pendents.html",
                  { 'title': title,
                    'projectes': projectes
                })


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_projectes_pendents_eco(request):
    """
    View that renders a list of projects that
    needs to be validated and that should have paid
    in euros
    """
    title = u"Llistat de aspirants a projectes que volen pagar amb ecos"
    
    projectes = ProjecteCollectiu.objects.filter(es_validat=False).filter(quota_alta_forma_pagament='eco')

    return render(request,
                  "alta_socies/llistat_projectes_pendents.html",
                  { 'title': title,
                    'projectes': projectes
                })


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_projectes_pendents_temp(request):
    """
    View that renders a list of projects that
    needs to be validated and that should have paid
    in time hours
    """
    title = u"Llistat de aspirants a projectes que volen pagar amb hores"
    
    projectes = ProjecteCollectiu.objects.filter(es_validat=False).filter(quota_alta_forma_pagament='h')

    return render(request,
                  "alta_socies/llistat_projectes_pendents.html",
                  { 'title': title,
                    'projectes': projectes
                })


@login_required
@user_passes_test(is_user_responsable_alta)
def validar_projecte_pendent(request, id_projecte=None):
    """
    View that renders the details of a project
    and a mini-form for completing registration process
    """
    title = u"Detalls i validació d'una socia"

    projecte = get_object_or_404(ProjecteCollectiu, pk=id_projecte)

    form_class = modelform_factory(ProjecteCollectiu, fields=("compte_ces_assignat",))

    form = form_class(request.POST or None)

    if request.method == 'POST':
        if form.is_valid():
            
            projecte.pagament_rebut = True
            projecte.compte_ces_assignat = form.cleaned_data["compte_ces_assignat"]
            projecte.es_validat = True
            projecte.dada_validacio = now()
            projecte.usuaria_que_valida = request.user
            projecte.save()
            messages.success(request, "S'ha validat el projecte correctament")
            return redirect('inici')
    return render(request,
                  'alta_socies/detalls_i_validacio_projectes_pendents.html',
                  {'title': title,
                   'projecte': projecte,
                   'form': form
               })


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_socia_cooperativa(request):
    """
    View that renders a member's book cooked
    with some ajax
    """
    title = u"LLibre de Sòcies"

    socies = SociaCooperativa.objects.all().filter(es_validada=True).order_by('compte_ces_assignat')

    if request.is_ajax():
        coop = request.GET.get('coop', None)
        nom = request.GET.get('nom', None)
        
        if coop:
            socies = socies.filter(compte_ces_assignat__icontains=coop)

        if nom:
            socies = socies.filter(socia__nom_sencer__icontains=nom)
        
        return render(request,
                      'alta_socies/filtre_socies.html',
                      {'socies': socies})
    
    return render(request,
                  'alta_socies/llistat_socia_cooperativa.html',
                  {'title': title,
                   'socies': socies})


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_projecte_collectiu(request):
    """
    View that renders a projects' book cooked
    with some ajax
    """
    title = u"Llibre de Projectes Col·lectius"

    projectes = ProjecteCollectiu.objects.all().filter(es_validat=True).order_by('compte_ces_assignat')

    if request.is_ajax():
        comarca = request.GET.get('comarca', None)
        codi_postal = request.GET.get('codi_postal', None)
        
        if comarca:
            projectes = projectes.filter(adreca__comarca__icontains=comarca)

        if codi_postal:
            projectes = projectes.filter(adreca__codi_postal__icontains=codi_postal)
        
        return  render(request,
                       'alta_socies/filtre_projectes.html',
                       {'projectes': projectes})
    
    return render(request,
                  'alta_socies/llistat_projecte_collectiu.html',
                  {'title': title,
                   'projectes': projectes,
            })


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_filtre_habilitats(request):
    """
    View that renders  members filtered by
    abilities
    """
    title = u"Buscador de Sòcies per Habilitat"

    form = FormulariFiltreSociesPerHabilitat()
    
    if request.is_ajax():
        habilitat = request.GET.get('habilitat', None)
        socies = []
        
        if habilitat:
            habilitat = Habilitat.objects.get(pk=habilitat)
            socies = [h.persona.sociacooperativa for h in habilitat.habilitatscontribucionspersona_set.all() if hasattr(h.persona, 'sociacooperativa') and h.persona.sociacooperativa.es_validada]
        
        return render(request,
                       'alta_socies/filtre_habilitats.html',
                       {'socies': socies})
    
    return render(request,
                  'alta_socies/llistat_socies_habilitats.html',
                  {'title': title,
                   'form': form,
            })


@login_required
@user_passes_test(is_user_responsable_alta)
def llistat_socies_afins(request):
    """
    List view for SociaAfi instances cooked with
    some ajax for filtering.
    """
    title = u"Llibre Sòcies Afins"

    socies = SociaAfi.objects.all().order_by('nom')

    if request.is_ajax():
        projecte = request.GET.get('projecte', None)

        if projecte:
            socies = socies.filter(projecte__nom__icontains=projecte)

        return render(request, 'alta_socies/filtre_sociesafins.html',
                      {'socies': socies})
    
    return render(request, 'alta_socies/llistat_sociesafins.html',
                  {'title': title})


@login_required
@user_passes_test(is_user_responsable_alta)
def dades_socia_cooperativa(request, id_socia=None):
    """
    Renders all the info of a SociaCooperativa instance
    """
    socia = get_object_or_404(SociaCooperativa, pk=id_socia)

    title = u"Detalls de la sòcia: " + socia.socia.nom_sencer

    return render(request, 'alta_socies/dades_socia_cooperativa.html',
                  {'title': title,
                   'socia': socia,
                   })


@login_required
@user_passes_test(is_user_responsable_alta)
def editar_socia_cooperativa(request, id_socia=None):
    """
    Update view for a SociaCooperativa instance
    """

    socia = get_object_or_404(SociaCooperativa, pk=id_socia)
    title = u"Editar dades de la sòcia: " + socia.socia.nom_sencer
    form_class = modelform_factory(Persona,
                                   fields=[
                                       'nom',
                                       'cognom1',
                                       'cognom2',
                                       'pseudonim',
                                       'email',
                                       'telefon',
                                       'dni',
                                       ])
    initial = {'nom': socia.socia.nom,
               'cognom1': socia.socia.cognom1,
               'cognom2': socia.socia.cognom2,
               'pseudonim': socia.socia.pseudonim,
               'email': socia.socia.email,
               'telefon': socia.socia.telefon,
               'dni': socia.socia.dni
               }

    form = form_class(request.POST or None, initial=initial)

    if request.method == 'POST':
        if form.is_valid():
            persona = Persona.objects.get(id=socia.socia.id)
            persona.nom = form.cleaned_data['nom']
            persona.cognom1 = form.cleaned_data['cognom1']
            persona.cognom2 = form.cleaned_data['cognom2']
            persona.pseudonim = form.cleaned_data['pseudonim']
            persona.email = form.cleaned_data['email']
            persona.telefon = form.cleaned_data['telefon']
            persona.dni = form.cleaned_data['dni']
            persona.save()

            messages.success(request, "Les dades de la sòcia s'han modificat correctament")
            return redirect('alta_socies:llistat_socia_cooperativa')

    return render(request,
                  'alta_socies/editar_socia_cooperativa.html',
                  {
                      'title': title,
                      'form': form
                  })


@login_required
@user_passes_test(is_user_responsable_alta)
def dades_projecte_collectiu(request, id_projecte=None):
    """
    Detail view for Projecte Collectiu instances
    """
    
    projecte = get_object_or_404(ProjecteCollectiu, pk=id_projecte)

    title = u"Detalls del projecte: " + projecte.nom

    return render(request, 'alta_socies/dades_projecte_collectiu.html',
                  {'title': title,
                   'projecte': projecte,
                   })


@login_required
@user_passes_test(is_user_responsable_alta)
def editar_dades_generals_projecte(request, id_projecte=None):
    """
    Update view for ProjecteCollectiu instance
    """

    projecte = get_object_or_404(ProjecteCollectiu, pk=id_projecte)
    title = u"Editar dades del projecte: " + projecte.nom
    form_class = modelform_factory(ProjecteCollectiu,
                                   fields=[
                                       'nom',
                                       'website',
                                       'descripcio',
                                       'email',
                                       'telefon',
                                       'tipus_projecte',
                                   ])
    form = form_class(request.POST or None, instance=projecte)

    if request.method == 'POST':
        if form.is_valid():
            form.save()

            messages.success(request, u"Les dades del projecte s'han modificat correctament")
            return redirect('alta_socies:dades_projecte_collectiu', id_projecte=projecte.id)

    return render(request,
                  'alta_socies/editar_dades_generals_projecte_collectiu.html',
                  {
                      'title': title,
                      'form': form
                  })


@login_required
@user_passes_test(is_user_responsable_alta)
def editar_dades_adreca_projecte(request, id_projecte=None):
    """
    Update view for AdrecaProjecteCollectiu instance
    """

    projecte = get_object_or_404(ProjecteCollectiu, pk=id_projecte)
    title = u"Editar adreça del projecte: " + projecte.nom
    form_class = modelform_factory(AdrecaProjecteCollectiu,
                                   fields=[
                                       'pais',
                                       'provincia',
                                       'comarca',
                                       'poblacio',
                                       'adreca',
                                       'codi_postal',
                                   ])
    form = form_class(request.POST or None, instance=projecte.adreca)

    if request.method == 'POST':
        if form.is_valid():
            form.save()

            messages.success(request, u"Les dades de l'adreça del projecte s'han modificat correctament")
            return redirect('alta_socies:dades_projecte_collectiu', id_projecte=projecte.id)

    return render(request,
                  'alta_socies/editar_dades_adreca_projecte_collectiu.html',
                  {
                      'title': title,
                      'form': form
                  })


@login_required
@user_passes_test(is_user_responsable_alta)
def editar_socies_afins_projecte(request, id_projecte=None):
    """
    Update view for SociaAfi instances
    """

    projecte = get_object_or_404(ProjecteCollectiu, pk=id_projecte)
    title = u"Editar socies afins del projecte: " + projecte.nom
    formset_class = modelformset_factory(SociaAfi, extra=9, min_num=1, validate_min=True,
                                         exclude=["usuaria_que_invita",
                                                  "created_at",
                                                  "esta_pendent_de_validacio",
                                                  "validated_at",
                                                  "usuaria_que_valida",
                                                  "projecte"])
    
    formset = formset_class(request.POST or None, queryset=SociaAfi.objects.none())

    if request.method == 'POST':
        if formset.is_valid():
            
            socies_afins = formset.save(commit=False)

            for socia in socies_afins:
                socia.projecte = projecte
                socia.save()
                
            messages.success(request, u"Les socies afins s'han afegit correctament al projecte")
            return redirect('alta_socies:dades_projecte_collectiu', id_projecte=projecte.id)
        
    return render(request,
                  'alta_socies/formulari_projecte_collectiu_socies_afins.html',
                  {
                      'title': title,
                      'formset': formset,              
                  },
    )


@login_required
@user_passes_test(is_user_responsable_alta)
def exportar_llibre_integral_socies(request):
    """
    Export all cooperative members to csv
    """
    d = now().date().isoformat()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=llibre_integral_socies-%s.csv' % (d)

    header = [
        u"Nom",
        u"Primer Cognom",
        u"Segon Cognom",
        u"Pseudonim",
        u"Email",
        u"DNI",
        u"Compte COOP",
    ]

    # check for all Persona's that already have a COOP number
    queryset = [socia for socia in Persona.objects.filter(compte_integralces__startswith="COOP")]
    # add the remainings
    queryset += [socia for socia in Persona.objects.exclude(compte_integralces__startswith="COOP") 
                 if hasattr(socia, "sociacooperativa")
                 and socia.sociacooperativa.es_validada
                 and "COOP" in socia.sociacooperativa.compte_ces_assignat]
    queryset.sort(
        key=lambda x: x.compte_integralces
        if x.compte_integralces
        else x.sociacooperativa.compte_ces_assignat
    )
    csv_data = [header]
    
    for socia in queryset:
        compte_coop = socia.compte_integralces \
                      if "COOP" in socia.compte_integralces \
                         else socia.sociacooperativa.compte_ces_assignat
        csv_data += [[
            socia.nom,
            socia.cognom1,
            socia.cognom2,
            socia.pseudonim,
            socia.email,
            socia.dni,
            compte_coop,
        ]]

    t = loader.get_template("alta_socies/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response


@login_required
@user_passes_test(is_user_responsable_alta)
def exportar_llibre_integral_socies_afins(request):
    """
    Export all cooperative members to csv
    """
    d = now().date().isoformat()
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=llibre_integral_socies_afins-%s.csv' % (d)

    header = [
        u"Nom",
        u"Primer Cognom",
        u"Segon Cognom",
        u"Pseudonim",
        u"Email",
        u"DNI",
    ]

    # check for all Persona's that already have a COOP number
    queryset = [socia for socia in Persona.objects.exclude(compte_integralces__startswith="COOP")]
    # add the remainings
    queryset += [socia for socia in SociaAfi.objects.all()]

    csv_data = [header]
    
    for socia in queryset:
        csv_data += [[
            socia.nom,
            socia.cognom1,
            socia.cognom2,
            socia.pseudonim,
            socia.email,
            socia.dni,
        ]]

    t = loader.get_template("alta_socies/export.txt")
    c = Context({'data': csv_data})
    response.write(t.render(c))
    
    return response



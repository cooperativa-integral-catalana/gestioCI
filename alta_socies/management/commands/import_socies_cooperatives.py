# -*- coding: utf-8 -*-
# import_socies_cooperatives.py
# 1. Read the mess
# 2. Catalogue the data
# 3. Output dirty.csv
# 4. Output clean.csv
# 5. Read clean.csv
# 6. Write single.csv and multiple.csv
# 7. Create single.csv objects:
# 7.1. Check for at least two names
# 7.2. Check for COOP existence
# 7.3. Create if neccessary
# 8. Create multiple.csv objects:
# 8.1. Check for COOP existence
# 8.2. Create if necessary
# 9. Read relatives.csv
# 10. Check for COOP relation
# 11. Create objects as necessary

from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from alta_socies.models import SociaCooperativa, ProjecteCollectiu, SociaAfi
from socies.models import AdrecaSociaCooperativa, AdrecaProjecteCollectiu, Persona
from gestioci.helpers import now

import unicodecsv as csv
import re


class Command(BaseCommand):
    help = 'Import members from old csv'

    def handle(self, *args, **options):
        SC_SUCCESSES = 0
        SC_FAILURES = 0
        PC_SUCCESSES = 0
        PC_FAILURES = 0
        SA_SUCCESSES = 0
        SA_FAILURES = 0

        coop_re = re.compile(r"^COOP\d{4}")
        email_re = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")

        # 1. Read the mess
        with open('altes_cooperatives.csv', 'rb') as csvfile:
            reader = csv.reader(csvfile, encoding='utf-8')
            header = reader.next()
            
            # Step 2. Catalogue the data
            dirty_lines = (line for line in reader \
                           if not coop_re.match(line[11]) \
                           or not email_re.match(line[13]) \
                           or not "ACTIU" in line[5] \
                           or not ("individual" in line[19] \
                                   or u"col·lectiu" in line[19]))
            
            # Step 3. Output dirty.csv
            with open ('dirty.csv', 'wb') as dirties:
                writer = csv.writer(dirties)
                writer.writerow(header)
                for line in dirty_lines:
                    writer.writerow(line)

        # Step 1. Read the mess
        with open('altes_cooperatives.csv', 'rb') as csvfile:
            reader = csv.reader(csvfile, encoding='utf-8')
            header = reader.next()
            
            # Step 2. Catalogue the data
            clean_lines =  (line for line in reader \
                            if coop_re.match(line[11]) \
                            and email_re.match(line[13]) \
                            and "ACTIU" in line[5] \
                            and ("individual" in line[19] \
                                 or u"col·lectiu" in line[19]))

            # Step 4. Output clean.csv
            with open ('clean.csv', 'wb') as cleanies:
                writer = csv.writer(cleanies)
                writer.writerow(header)
                for line in clean_lines:
                    writer.writerow(line)

        # Step 5. Read clean.csv
        with open('clean.csv', 'rb') as cleanies:
            reader = csv.reader(cleanies, encoding='utf-8')
            header = reader.next()
            
            singles = (line for line in reader if "individual" in line[19])
            
            # Step 6. Write single.csv
            with open ('single.csv', 'wb') as singlesfile:
                writer = csv.writer(singlesfile)
                writer.writerow(header)
                for line in singles:
                    writer.writerow(line)

        # Step 5. Read clean.csv
        with open('clean.csv', 'rb') as cleanies:
            reader = csv.reader(cleanies, encoding='utf-8')
            header = reader.next()
            
            multiples = (line for line in reader if u"col·lectiu" in line[19])

            # Step 6. Write multiple.csv
            with open ('multiple.csv', 'wb') as multiplesfile:
                writer = csv.writer(multiplesfile)
                writer.writerow(header)
                for line in multiples:
                    writer.writerow(line)
                                                
        # Beginning step 7. Create single.csv objects
        with open('single.csv', 'rb') as singles:
            reader = csv.reader(singles, encoding='utf-8')
            
            for line in reader:
                try:
                    with transaction.atomic():

                        coop = line[11]
                        names = line[12]

                        # Step 7.1. Check for at least two names
                        if len(names.split()) < 2:
                            SC_FAILURES += 1
                            self.stdout.write("%s has been dropped because doesn't fulfill name requirements" % (coop))
                            continue

                        # Step 7.2. Check for COOP existence
                        if SociaCooperativa.objects.filter(compte_ces_assignat=coop).exists():
                            SC_FAILURES += 1
                            self.stdout.write("%s already exists" % (coop))
                            continue

                        # Discarding 'autoocupats'
                        if "autoocupat" in line[20]:
                            PC_FAILURES += 1
                            self.stdout.write("%s is autoocupat" % (coop))
                            continue
                        
                        # Step 7.3. Create objects if necessary
                        persona = Persona(email=line[13], telefon=line[14])

                        if len(names) == 2:
                            persona.nom = names[0]
                            persona.cognom1 = names[1]

                        elif len(names) == 3:
                            persona.nom = names[0]
                            persona.cognom1 = names[1]
                            persona.cognom2 = names[2]

                        elif len(names) == 4:
                            persona.nom = names[0]
                            persona.pseudonim = names[1]
                            persona.cognom1 = names[2]
                            persona.cognom2 = names[3]

                        else:
                            persona.nom = names[0] + names[1]
                            persona.pseudonim = names[2]
                            persona.cognom1 = names[3]
                            persona.cognom2 = names[4]

                        persona.save()

                        comarca = line[18] or u"-_-"
                        codi_postal = u"0"+line[16]
                        adreca = AdrecaSociaCooperativa.objects.create(
                            pais=u"España",
                            provincia=u"-_-",
                            comarca=comarca,
                            poblacio=line[17],
                            codi_postal=codi_postal,
                            adreca=line[15],
                        )
                        
                        if "ECO" in line[22]:
                            forma_pagament = SociaCooperativa.FORMA_PAGAMENT_ECO
                            quota_alta = SociaCooperativa.FORMA_PAGAMENT_ECO
                        elif "EUR" in line[22]:
                            forma_pagament = SociaCooperativa.FORMA_PAGAMENT_EURO
                            quota_alta = SociaCooperativa.FORMA_PAGAMENT_EURO
                        else:
                            forma_pagament = SociaCooperativa.FORMA_PAGAMENT_HORA
                            quota_alta = SociaCooperativa.FORMA_PAGAMENT_HORA

                        SociaCooperativa.objects.create(
                            socia=persona,
                            quota_alta_forma_pagament=forma_pagament,
                            quota_alta=quota_alta,
                            pagament_rebut=True,
                            compte_ces_assignat=coop,
                            adreca=adreca,
                            es_validada=True,
                        )
                        
                        SC_SUCCESSES += 1
                        self.stdout.write("%s was successfully created" % (coop))
                except Exception as e:
                    SC_FAILURES += 1
                    self.stdout.write("An error occurred with member %s: %s" % (line[11], str(e)))
                    
        # Beginning step 8. Create multiple.csv objects
        with open('multiple.csv', 'rb') as multiples:
            reader = csv.reader(multiples, encoding='utf-8')

            for line in reader:
                try:
                    with transaction.atomic():
                        coop = line[11]
                        tipus = line[20]
                        
                        # Step 8.1. Check for COOP existence
                        if ProjecteCollectiu.objects.filter(compte_ces_assignat=coop).exists():
                            PC_FAILURES += 1
                            self.stdout.write("%s already exists" % (coop))
                            continue

                        # Discarding 'autoocupats'
                        if "autoocupat" in tipus:
                            PC_FAILURES += 1
                            self.stdout.write("%s is autoocupat" % (coop))
                            continue
                        
                        # Step 8.2. Create objects if necessary
                        comarca = line[18] or "-_-"
                        codi_postal = "0"+line[16]
                        adreca = AdrecaProjecteCollectiu.objects.create(
                            pais="España",
                            provincia="-_-",
                            comarca=comarca,
                            poblacio=line[17],
                            codi_postal=codi_postal,
                            adreca=line[15],
                        )
                        
                        if 'serveis' in tipus:
                            tipus_projecte = ProjecteCollectiu.TIPUS_SERVEIS
                        elif 'sumidor' in tipus:
                            tipus_projecte = ProjecteCollectiu.TIPUS_CONSUM
                        elif 'cooperatiu' in tipus:
                            tipus_projecte = ProjecteCollectiu.TIPUS_COLLECTIU
                        else:
                            tipus_projecte = ProjecteCollectiu.TIPUS_PAIC

                        if "ECO" in line[22]:
                            forma_pagament = SociaCooperativa.FORMA_PAGAMENT_ECO    
                        elif "EUR" in line[22]:
                            forma_pagament = SociaCooperativa.FORMA_PAGAMENT_EURO
                        else:
                            forma_pagament = SociaCooperativa.FORMA_PAGAMENT_HORA
                            
                        ProjecteCollectiu.objects.create(
                            nom=line[12],
                            email=line[13],
                            telefon=line[14],
                            adreca=adreca,
                            tipus_projecte=tipus_projecte,
                            quota_alta_forma_pagament=forma_pagament,
                            pagament_rebut=True,
                            compte_ces_assignat=coop,
                            es_validat=True
                        )

                        PC_SUCCESSES += 1
                        self.stdout.write("%s was successfully created" % (coop))
                except Exception as e:
                    PC_FAILURES += 1
                    self.stdout.write("An error occurred creating project %s: %s" % (line[11], e))

        # Step 9. Read relatives.csv
        with open('relatives.csv', 'rb') as relatives:
            reader = csv.reader(relatives, encoding='utf-8')

            for line in reader:
                try:
                    with transaction.atomic():
                        coop = line[7]

                        # Step 10. Check COOP relations
                        if not coop:
                            SA_FAILURES += 1
                            self.stdout.write("COOP not foound")
                            continue

                        if len(coop) == 3:
                            coop = "COOP0"+coop

                        if len(coop) == 4:
                            coop = "COOP"+coop

                        if not ProjecteCollectiu.objects.filter(compte_ces_assignat=coop).exists():
                            SA_FAILURES += 1
                            self.stdout.write("No relative project found")
                            continue

                        # Step 11. Create objects if necessary
                        projecte = ProjecteCollectiu.objects.get(compte_ces_assignat=coop)
                        
                        SociaAfi.objects.create(
                            nom=line[2],
                            cognom1=line[3],
                            email=line[5],
                            tipus_dni=SociaAfi.TIPUS_DOCUMENT_ID_DNI,
                            dni=line[4],
                            esta_pendent_de_validacio=False,
                            projecte=projecte,
                        )

                        SA_SUCCESSES += 1
                        self.stdout.write("SociaAfi successfully created")
                        
                except Exception as e:
                    SC_FAILURES += 1
                    self.stdout.write("An error occurred with socia afi: %s" % (e))

        # Output statistics
        self.stdout.write("%s members were successfully created" % str(SC_SUCCESSES))
        self.stdout.write("%s errors occurred while importing members" % str(SC_FAILURES))
        self.stdout.write("%s projects were successfully created" % str(PC_SUCCESSES))
        self.stdout.write("%s errors occurred while importing projects" % str(PC_FAILURES))
        self.stdout.write("%s relatives were successfully created" % str(SA_SUCCESSES))
        self.stdout.write("%s errors occurred while importing relatives" % str(SA_FAILURES))




    

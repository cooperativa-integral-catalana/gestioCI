from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from alta_socies.models import ProcesAltaAutoocupat
from gestioci.helpers import now
from projectes.models import ProjecteAutoocupat, FotoProjecteAutoocupatFiraire, PolissaAsseguranca


class Command(BaseCommand):
    help = 'Accept all processes that arrived at step 20 and create relative projects'

    def handle(self, *args, **options):
        successes = 0
        failures = 0
        for proces in ProcesAltaAutoocupat.objects.filter(pas=20).filter(resolucio=ProcesAltaAutoocupat.RESOLUCIO_EN_CURS):
            try:
                with transaction.atomic():

                    self.stdout.write('Starting to migrate process %d - %s' % (proces.id, proces.nom))

                    proces.resolucio = ProcesAltaAutoocupat.RESOLUCIO_ACCEPTAT
                    proces.data_resolucio = now()
                    proces.save()

                    self.stdout.write('Process status -> "acceptat"')

                    projecte = ProjecteAutoocupat.objects.create_projecte(proces=proces)

                    self.stdout.write('Created relative project')

                    for peticio in proces.peticions_asseguranca.all():
                        PolissaAsseguranca.objects.create_polissa(peticio=peticio,
                                                                  projecte=projecte)

                    self.stdout.write('Migrated all PolissaAsseguranca')
                    
                    for foto in proces.fotos_parada_firaire.all():
                        FotoProjecteAutoocupatFiraire.objects.create(
                            projecte=projecte,
                            nom=foto.nom,
                            detalls=foto.detalls,
                            imatge=foto.imatge,
                            url_miniatura=foto.url_miniatura)

                    self.stdout.write('Migrated all FotoProjecteAutoocupat')

                    successes += 1
                    
                    self.stdout.write('Proces %d - %s has been fully migrated to project %d - %s' % (proces.id, proces.nom, projecte.id, projecte.nom))

            except Exception as e:
                failures += 1
                self.stdout.write("Proces %d - %s it's not possible to be migrated")
                self.stdout.write("Reason: %s" % e)
                
        self.stdout.write("%d processes have been migrated!" % successes)
        self.stdout.write("%d processes have been kept back!" % failures)
        


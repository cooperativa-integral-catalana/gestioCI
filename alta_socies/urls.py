from django.conf.urls import patterns, include, url

from .views import SessioMonedaListView, SessioMonedaCreateView
from .views import SessioAcollidaListView, SessioAcollidaCreateView, \
    SessioAcollidaUpdateView, SessioMonedaUpdateView
from .views import ProcesAltaAutoocupatDetailView

urlpatterns = patterns(

    'alta_socies.views',


    # ------------
    # ALTRES ALTES
    # ------------

    url(r'^sociaafi/llistat/$', 'llistat_socies_afins', name='llistat_socies_afins'),
    
    url(r'^cooperativa/habilitats/$', 'llistat_filtre_habilitats', name='llistat_socies_habilitats'),
    
    url(r'^cooperativa/llistat/$', 'llistat_socia_cooperativa', name='llistat_socia_cooperativa'),
    
    url(r'^cooperativa/llistat_pendents/euros/$', 'llistat_socies_pendents_euro', name='llistat_socies_pendents_euro'),

    url(r'^cooperativa/llistat_pendents/ecos/$', 'llistat_socies_pendents_eco', name='llistat_socies_pendents_eco'),

    url(r'^cooperativa/llistat_pendents/hores/$', 'llistat_socies_pendents_temp', name='llistat_socies_pendents_temp'),

    url(r'^cooperativa/validar/(?P<id_socia>\d+)/$', 'validar_socia_pendent', name='validar_socia_pendent'),
    
    url(r'^cooperativa/dades_personals/$', 'proces_alta_socia_cooperativa_dades_personals', name='alta_socia_cooperativa_dades_personals'),

    url(r'cooperativa/info/(?P<id_socia>\d+)/$', 'dades_socia_cooperativa', name='dades_socia_cooperativa'),

    url(r'cooperativa/editar/(?P<id_socia>\d+)/$', 'editar_socia_cooperativa', name='editar_socia_cooperativa'),

    url(r'^cooperativa/dades_alta/$', 'proces_alta_socia_cooperativa_dades_alta', name='alta_socia_cooperativa_dades_alta'),

    url(r'^cooperativa/dades_adreca/$', 'proces_alta_socia_cooperativa_dades_adreca', name='alta_socia_cooperativa_dades_adreca'),
    
    url(r'^collectiu/dades_generals/$', 'proces_alta_projecte_collectiu_dades_generals', name='collectiu_dades_generals'),

    url(r'^collectiu/dades_concretes/$', 'proces_alta_projecte_collectiu_dades_concretes', name='collectiu_dades_concretes'),

    url(r'^collectiu/socies_afins/$', 'proces_alta_projecte_collectiu_socies_afins', name='collectiu_socies_afins'),

    url(r'^collectiu/llistat/$', 'llistat_projecte_collectiu', name='llistat_projecte_collectiu'),
    
    url(r'^collectiu/llistat_pendents/euros/$', 'llistat_projectes_pendents_euro', name='llistat_projectes_pendents_euro'),

    url(r'^collectiu/llistat_pendents/ecos/$', 'llistat_projectes_pendents_eco', name='llistat_projectes_pendents_eco'),

    url(r'^collectiu/llistat_pendents/hores/$', 'llistat_projectes_pendents_temp', name='llistat_projectes_pendents_temp'),

    url(r'^collectiu/validar/(?P<id_projecte>\d+)/$', 'validar_projecte_pendent', name='validar_projecte_pendent'),

    url(r'^collectiu/editar/dades_generals/(?P<id_projecte>\d+)/$', 'editar_dades_generals_projecte', name='editar_dades_generals_projecte'),

    url(r'^collectiu/editar/dades_adreca/(?P<id_projecte>\d+)/$', 'editar_dades_adreca_projecte', name='editar_dades_adreca_projecte'),

    url(r'^collectiu/editar/socies_afins/(?P<id_projecte>\d+)/$', 'editar_socies_afins_projecte', name='editar_socies_afins_projecte'),
    
    url(r'^collectiu/info/(?P<id_projecte>\d+)/$', 'dades_projecte_collectiu', name='dades_projecte_collectiu'),

    url(r'^llibre/socies/$', 'exportar_llibre_integral_socies', name='exportar_llibre_integral_socies'),

    url(r'^llibre/socies_afins/$', 'exportar_llibre_integral_socies_afins', name='exportar_llibre_integral_socies_afins'),

    
    # -----------------
    # SOCIS AUTOOCUPATS
    # -----------------

    url(r'^rebutjar_proces_alta/(?P<id_proces>\d+)/$', 'rebutjar_proces_alta_autoocupat', name='rebutjar_proces'),

    url(r'^exportar_a_csv/$', 'exportar_a_csv', name='exportar_a_csv'),

    url(r'^autoocupat/resum/(?P<pk>\d+)/$', ProcesAltaAutoocupatDetailView.as_view(), name='resum_proces_alta_projecte_autoocupat'),

    url(r'^editar_sessio_moneda/(?P<pk>\d+)/$', SessioMonedaUpdateView.as_view(), name='editar_sessio_moneda'),

    url(r'^editar_sessio_acollida/(?P<pk>\d+)/$', SessioAcollidaUpdateView.as_view(), name='editar_sessio_acollida'),

    url(r'^llistat_sessio_acollida', SessioAcollidaListView.as_view(), name='llistat_sessio_acollida'),

    url(r'^crear_sessio_acollida', SessioAcollidaCreateView.as_view(), name='crear_sessio_acollida'),

    url(r'^llistat_sessio_moneda', SessioMonedaListView.as_view(), name='llistat_sessio_moneda'),

    url(r'^crear_sessio_moneda', SessioMonedaCreateView.as_view(), name='crear_sessio_moneda'),

    url(r'^generador_sessions/$',
        'generar_sessions',
        name='generador_sessions'),

    url(r'^sessions_del_mes/(?P<tipus>\w+)(/(?P<year>\d\d\d\d)/(?P<month>\d\d?))?$',
        'sessions_del_mes',
        name='sessions_del_mes'),

    url(r'^autoocupat/$',
        'proces_alta_projecte_autoocupat',
        name='proces_alta_projecte_autoocupat'),

    url(r'^autoocupat/(?P<id_proces>\d+)/$',
        'proces_alta_projecte_autoocupat',
        name='proces_alta_projecte_autoocupat'),

    url(r'^autoocupat/(?P<id_proces>\d+)/(?P<pas>\d+)/$',
        'proces_alta_projecte_autoocupat',
        name='proces_alta_projecte_autoocupat'),

    url(r'^autoocupat/llista-tasques/(?P<id_proces>\d+)/$',
        'proces_alta_projecte_autoocupat_llista_de_tasques',
        name='proces_alta_projecte_autoocupat-llista_de_tasques'),

    url(r'^autoocupat/llistat_proces_alta_autoocupat/$',
        'llistat_proces_alta_autoocupat',
        name='llistat_proces_alta_autoocupat'),

    url(r'^autoocupat/control-qualitat/$',
        'llistat_proces_alta_projecte_autoocupat_revisio_descripcions',
        name='llistat_proces_alta_projecte_autoocupat_revisio_descripcions'),

    url(r'^estadistiques_proces_alta_projecte_autoocupat/?$',
        'estadistiques_proces_alta_projecte_autoocupat',
        name='estadistiques_proces_alta_projecte_autoocupat'),

    url(r'^reassignacio_proces_alta_autoocupat/?$',
        'reassignacio_proces_alta_autoocupat',
        name='reassignacio_proces_alta_autoocupat'),

)

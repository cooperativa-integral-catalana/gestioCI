# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0007_adrecaprojectecollectiu'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('alta_socies', '0012_auto_20150822_1600'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjecteCollectiu',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(unique=True, max_length=64)),
                ('website', models.URLField(null=True, verbose_name='lloc web', blank=True)),
                ('descripcio', models.TextField(verbose_name='descripci\xf3')),
                ('email', models.EmailField(max_length=254)),
                ('telefon', models.CharField(max_length=24, null=True, blank=True)),
                ('tipus_projecte', models.CharField(default=b'cooperatiu_collectiu', max_length=64, choices=[(b'cooperatiu_collectiu', 'Projecte cooperatiu col\xb7lectiu'), (b'de_serveis', 'Projecte de serveis'), (b'grup_de_consum', 'Grup de consum'), (b'paic_sense_facturacio', 'PAIC sense facturaci\xf3')])),
                ('quota_alta_forma_pagament', models.CharField(default=b'euro', max_length=6, choices=[(b'eco', 'ecos'), (b'euro', 'euros'), (b'h', 'hores')])),
                ('necessita_cobertura_legal', models.BooleanField(default=False)),
                ('pagament_rebut', models.BooleanField(default=False)),
                ('compte_CES_assignat', models.CharField(max_length=16, null=True)),
                ('es_validat', models.BooleanField(default=False)),
                ('dada_validacio', models.DateTimeField(null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('adreca', models.ForeignKey(to='socies.AdrecaProjecteCollectiu', null=True)),
                ('membre_de_referencia', models.ForeignKey(verbose_name='membre de referencia', to='socies.Persona')),
                ('socies_afins_adicionals', models.ManyToManyField(related_name='_projectecollectiu_socies_afins_adicionals_+', to='socies.Persona', blank=True)),
                ('usuaria_que_valida', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'Projecte col\xb7lectiu',
                'verbose_name_plural': 'Projectes col\xb7lectius',
            },
        ),
        migrations.CreateModel(
            name='SociaAfi',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=64, null=True, blank=True)),
                ('cognom1', models.CharField(max_length=64, null=True, blank=True)),
                ('cognom2', models.CharField(max_length=64, null=True, blank=True)),
                ('pseudonim', models.CharField(max_length=64, null=True, blank=True)),
                ('email', models.EmailField(max_length=254, null=True, blank=True)),
                ('telefon', models.CharField(max_length=24, null=True, blank=True)),
                ('tipus_dni', models.CharField(default=b'dni', max_length=6, choices=[(b'dni', b'dni'), (b'nie', b'nie'), (b'passaport', b'passaport')])),
                ('dni', models.CharField(max_length=16)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('esta_pendent_de_validacio', models.BooleanField(default=True)),
                ('validated_at', models.DateTimeField(null=True)),
                ('projecte', models.ForeignKey(to='alta_socies.ProjecteCollectiu')),
                ('usuaria_que_invita', models.ForeignKey(related_name='socia_afin_pendent_invitada', to=settings.AUTH_USER_MODEL, null=True)),
                ('usuaria_que_valida', models.ForeignKey(related_name='socia_afin_pendent_validada', to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'Socia Af\xed Pendent de Revisi\xf3',
                'verbose_name_plural': 'Socies Afins Pendents de Revisi\xf3',
            },
        ),
        migrations.CreateModel(
            name='SociaCooperativa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quota_alta_forma_pagament', models.CharField(default=b'euro', max_length=6, choices=[(b'eco', 'ecos'), (b'euro', 'euros'), (b'h', 'hores')])),
                ('pagament_rebut', models.BooleanField(default=False)),
                ('compte_CES_assignat', models.CharField(max_length=16, null=True)),
                ('es_validada', models.BooleanField(default=False)),
                ('dada_validacio', models.DateTimeField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('socia', models.OneToOneField(to='socies.Persona')),
                ('usuaria_que_valida', models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True)),
            ],
            options={
                'verbose_name': 'Socia Cooperativa',
                'verbose_name_plural': 'Socies Cooperatives',
            },
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0004_persona_dni_i_regeneracio_personacercable'),
        ('alta_socies', '0005_procesaltaautoocupat_socies_afins_addicionals'),
    ]

    operations = [
        migrations.AddField(
            model_name='procesaltaautoocupat',
            name='socia_mentora',
            field=models.OneToOneField(related_name='+', null=True, blank=True, to='socies.Persona'),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0019_auto_lowercase_compte_CES_assignat'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='projectecollectiu',
            name='membre_de_referencia',
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0009_adrecasociacooperativa'),
        ('alta_socies', '0020_remove_projectecollectiu_membre_de_referencia'),
    ]

    operations = [
        migrations.AddField(
            model_name='sociacooperativa',
            name='adreca',
            field=models.ForeignKey(to='socies.AdrecaSociaCooperativa', null=True),
        ),
    ]

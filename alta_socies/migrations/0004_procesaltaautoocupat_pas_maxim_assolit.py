# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def actualitzar_pas_maxim_asolit(apps, schema_editor):
    ProcesAltaAutoocupat = apps.get_model('alta_socies', 'ProcesAltaAutoocupat')
    for p in ProcesAltaAutoocupat.objects.all():
        p.pas_maxim_assolit = p.pas
        p.save()


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0003_registrecanviprocesaltaautoocupat'),
    ]

    operations = [
        migrations.AddField(
            model_name='procesaltaautoocupat',
            name='pas_maxim_assolit',
            field=models.SmallIntegerField(default=1, verbose_name="pas m\xe9s avan\xe7at que s'ha assolit"),
            preserve_default=True,
        ),
        migrations.RunPython(actualitzar_pas_maxim_asolit),
    ]

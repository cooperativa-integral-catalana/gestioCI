# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('alta_socies', '0002_procesaltaautoocupat_afegir_responsable'),
    ]

    operations = [
        migrations.CreateModel(
            name='RegistreCanviProcesAltaAutoocupat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pas_origen', models.SmallIntegerField()),
                ('pas_actual', models.SmallIntegerField()),
                ('pas_destinacio', models.SmallIntegerField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('proces', models.ForeignKey(to='alta_socies.ProcesAltaAutoocupat')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]

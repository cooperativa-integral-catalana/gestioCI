# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('alta_socies', '0006_procesaltaautoocupat_socia_mentora'),
    ]

    operations = [
        migrations.AddField(
            model_name='procesaltaautoocupat',
            name='iva_assignat',
            field=models.IntegerField(default=18),
            preserve_default=True,
        ),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('empreses', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PeticioAsseguranca',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comentaris_avaluacio', models.TextField(null=True, blank=True)),
                ('numero_polissa', models.CharField(max_length=128, null=True, verbose_name='n\xfamero de p\xf2lissa', blank=True)),
                ('data_inici_polissa', models.DateField(null=True, verbose_name="data d'inici de la cobertura", blank=True)),
                ('data_final_polissa', models.DateField(null=True, verbose_name="data d'acabament de la cobertura", blank=True)),
                ('import_polissa', models.DecimalField(null=True, verbose_name='import total periode cobertura', max_digits=8, decimal_places=2, blank=True)),
                ('comentaris_alta', models.TextField(null=True, blank=True)),
                ('activitat', models.ForeignKey(blank=True, to='socies.Activitat', null=True)),
                ('adreca', models.ForeignKey(blank=True, to='socies.AdrecaProjecteAutoocupat', null=True)),
                ('companyia_asseguradora', models.ForeignKey(blank=True, to='empreses.EmpresaAsseguradora', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PeticioConcessioActivitat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comentaris_avaluacio', models.TextField(null=True, blank=True)),
                ('numero_llicencia', models.CharField(help_text="Al document de llic\xe8ncia que dona l'Ajuntament", max_length=64, null=True, verbose_name='n\xfamero de llic\xe8ncia', blank=True)),
                ('data_inici_llicencia', models.DateField(help_text="data d'inici del contracte amb l'Ajuntament", null=True, verbose_name="data d'inici del document original", blank=True)),
                ('data_final_llicencia', models.DateField(help_text="data d'acabament del contracte amb l'Ajuntament", null=True, verbose_name='data final del document original', blank=True)),
                ('data_inici_concessio', models.DateField(help_text="data d'inici del contracte que signa el projecte amb la CIC", null=True, verbose_name="data d'inici de la concessi\xf3 a la CIC", blank=True)),
                ('data_final_concessio', models.DateField(help_text="data d'acabament del contracte que signa el projecte amb la CIC", null=True, verbose_name='data final de la concessi\xf3 a la CIC', blank=True)),
                ('comentaris_alta', models.TextField(null=True, blank=True)),
                ('activitat', models.ForeignKey(to='socies.Activitat')),
                ('adreca', models.ForeignKey(related_name='peticions_concessio_activitat', blank=True, to='socies.AdrecaProjecteAutoocupat', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProcesAltaAutoocupat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=256)),
                ('email', models.EmailField(max_length=75)),
                ('telefon', models.CharField(max_length=32)),
                ('website', models.URLField(null=True, verbose_name='lloc web', blank=True)),
                ('descripcio', models.TextField(null=True, verbose_name='descripci\xf3', blank=True)),
                ('sessio_acollida_realitzada', models.BooleanField(default=False)),
                ('sessio_avaluacio_realitzada', models.BooleanField(default=False)),
                ('sessio_moneda_realitzada', models.BooleanField(default=False)),
                ('sessio_alta_realitzada', models.BooleanField(default=False)),
                ('pas', models.SmallIntegerField(default=1, help_text="punt del proc\xe9s d'alta en el que es troba el projecte", verbose_name="pas al proc\xe9s d'alta")),
                ('resolucio', models.CharField(default=b'en_curs', max_length=16, choices=[(b'en_curs', 'en curs'), (b'acceptat', 'acceptat'), (b'rebutjat', 'rebutjat')])),
                ('data_resolucio', models.DateTimeField(null=True, blank=True)),
                ('tipus', models.CharField(default=b'autoocupat', max_length=20, choices=[(b'autoocupat', 'autoocupat'), (b'autoocupat_firaire', 'autoocupat firaire'), (b'paic_amb_facturacio', 'PAIC amb facturaci\xf3')])),
                ('individual', models.CharField(default=b'individual', max_length=16, choices=[(b'individual', 'individual'), (b'col.lectiu', 'col\xb7lectiu')])),
                ('es_paic', models.BooleanField(default=False)),
                ('te_comerc_electronic', models.BooleanField(default=False, verbose_name='t\xe9 comer\xe7 electr\xf2nic')),
                ('mercat_virtual', models.BooleanField(default=False)),
                ('te_productes_ecologics', models.BooleanField(default=False, verbose_name='t\xe9 productes ecol\xf2gics')),
                ('tipus_parada_firaire', models.CharField(default=b'no', max_length=16, choices=[(b'no', 'no en t\xe9'), (b'fusta', 'de fusta'), (b'metall', 'met\xe0l\xb7lica')])),
                ('expositors', models.BooleanField(default=False, verbose_name='t\xe9 expositors a AureaSocial')),
                ('compte_CES_assignat', models.CharField(max_length=16, null=True, blank=True)),
                ('quota_alta_quantitat', models.DecimalField(null=True, max_digits=8, decimal_places=2, blank=True)),
                ('quota_alta_forma_pagament', models.CharField(default=b'euro', max_length=6, choices=[(b'eco', 'ecos'), (b'euro', 'euros'), (b'h', 'hores')])),
                ('quota_alta_data_hora_pagament', models.DateTimeField(null=True, blank=True)),
                ('quota_avancada_quantitat', models.DecimalField(null=True, max_digits=8, decimal_places=2, blank=True)),
                ('quota_avancada_forma_pagament', models.CharField(default=b'euro', max_length=6, choices=[(b'eco', 'ecos'), (b'euro', 'euros')])),
                ('quota_avancada_data_hora_pagament', models.DateTimeField(null=True, blank=True)),
                ('compte_CES_que_paga_les_quotes', models.CharField(max_length=16, null=True, blank=True)),
                ('pagament_quotes_numero_transaccio', models.CharField(max_length=128, null=True, blank=True)),
                ('comentaris_pagament_quotes', models.TextField(max_length=1024, null=True, verbose_name='Comentaris al moment del pagament de quotes', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('adreces', models.ManyToManyField(related_name='+', null=True, to='socies.AdrecaProjecteAutoocupat', blank=True)),
                ('altres_activitats', models.ManyToManyField(help_text='Altres activitats no vinculades a cap adre\xe7a', to='socies.Activitat', null=True, blank=True)),
                ('cooperativa_assignada', models.ForeignKey(blank=True, to='empreses.Cooperativa', null=True)),
                ('membres_de_referencia', models.ManyToManyField(related_name='altes_projecte_autoocupat', null=True, verbose_name='membres de refer\xe8ncia', to='socies.Persona', blank=True)),
            ],
            options={
                'verbose_name': "proc\xe9s d'alta de projecte autoocupat",
                'verbose_name_plural': "processos d'alta de projecte autoocupat",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sessio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data', models.DateTimeField()),
                ('duracio', models.PositiveSmallIntegerField(help_text='minuts', verbose_name='duraci\xf3')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SessioAcollida',
            fields=[
                ('sessio_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='alta_socies.Sessio')),
            ],
            options={
                'verbose_name': "sessi\xf3 d'acollida",
                'verbose_name_plural': "sessions d'acollida",
            },
            bases=('alta_socies.sessio',),
        ),
        migrations.CreateModel(
            name='SessioAlta',
            fields=[
                ('sessio_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='alta_socies.Sessio')),
            ],
            options={
                'verbose_name': "sessi\xf3 d'alta",
                'verbose_name_plural': "sessions d'alta",
            },
            bases=('alta_socies.sessio',),
        ),
        migrations.CreateModel(
            name='SessioAvaluacio',
            fields=[
                ('sessio_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='alta_socies.Sessio')),
            ],
            options={
                'verbose_name': "sessi\xf3 d'avaluaci\xf3",
                'verbose_name_plural': "sessions d'avaluaci\xf3",
            },
            bases=('alta_socies.sessio',),
        ),
        migrations.CreateModel(
            name='SessioMoneda',
            fields=[
                ('sessio_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='alta_socies.Sessio')),
            ],
            options={
                'verbose_name': 'sessi\xf3 de moneda social',
                'verbose_name_plural': 'sessions de moneda social',
            },
            bases=('alta_socies.sessio',),
        ),
        migrations.CreateModel(
            name='Ubicacio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pais', models.CharField(max_length=128, verbose_name='pa\xeds')),
                ('provincia', models.CharField(max_length=128, verbose_name='prov\xedncia')),
                ('comarca', models.CharField(max_length=128)),
                ('poblacio', models.CharField(max_length=128, verbose_name='poblaci\xf3')),
                ('codi_postal', models.CharField(max_length=16)),
                ('adreca', models.CharField(max_length=128, verbose_name='adre\xe7a')),
                ('ubicacio_especifica', models.CharField(max_length=256, null=True, verbose_name='ubicaci\xf3 espec\xedfica', blank=True)),
                ('nom', models.CharField(unique=True, max_length=256)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'ubicaci\xf3',
                'verbose_name_plural': 'ubicacions',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='sessio',
            name='responsable_alta',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='sessio',
            name='ubicacio',
            field=models.ForeignKey(to='alta_socies.Ubicacio'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='procesaltaautoocupat',
            name='sessio_acollida',
            field=models.ForeignKey(related_name='altes_projecte_autoocupat', verbose_name="sessi\xf3 d'acollida", to='alta_socies.SessioAcollida'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='procesaltaautoocupat',
            name='sessio_alta',
            field=models.ForeignKey(related_name='alta_projecte_autoocupat', blank=True, to='alta_socies.SessioAlta', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='procesaltaautoocupat',
            name='sessio_avaluacio',
            field=models.ForeignKey(related_name='alta_projecte_autoocupat', blank=True, to='alta_socies.SessioAvaluacio', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='procesaltaautoocupat',
            name='sessio_moneda',
            field=models.ForeignKey(related_name='altes_projecte_autoocupat', blank=True, to='alta_socies.SessioMoneda', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='peticioconcessioactivitat',
            name='proces_alta_autoocupat',
            field=models.ForeignKey(related_name='peticions_concessio_activitat', to='alta_socies.ProcesAltaAutoocupat'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='peticioasseguranca',
            name='proces_alta_autoocupat',
            field=models.ForeignKey(related_name='peticions_asseguranca', to='alta_socies.ProcesAltaAutoocupat'),
            preserve_default=True,
        ),
    ]

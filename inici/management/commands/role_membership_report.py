from gestioci.settings import auth_groups
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):

    help = u"Dumps a list of auth.Group and associated auth.User"

    def add_arguments(self, parser):
        parser.add_argument('--all', dest='all', action='store_true', default=False, help=u"Show all auth.Group instead of just GestioCI's")

    def handle(self, *args, **options):

        if options['all']:
            groups = Group.objects.all()
        else:
            groups = Group.objects.filter(name__in=auth_groups.grups)

        for g in groups.order_by('name'):
            users = g.user_set.all()
            self.stdout.write(u"%s (%d members)" % (g.__unicode__(), users.count()))

            for u in users.order_by('username'):
                self.stdout.write(u'\t%s <%s>' % (u.__unicode__(), u.email))
            self.stdout.write('')

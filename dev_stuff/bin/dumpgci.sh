#!/bin/bash

python manage.py dumpdata --indent 4 --exclude auth.permission --exclude contenttypes --exclude sessions.session --exclude admin.logentry --exclude auth.user --exclude alta_socies.sessio --exclude alta_socies.sessioacollida --exclude alta_socies.sessioavaluacio --exclude alta_socies.sessiomoneda --exclude alta_socies.sessioalta --exclude alta_socies.procesaltaautoocupat --exclude alta_socies.registrecanviprocesaltaautoocupat --exclude alta_socies.peticioconcessioactivitat --exclude alta_socies.peticioasseguranca --exclude alta_socies.fotoprojecteautoocupatfiraire --exclude socies.personacercable --exclude socies.persona --exclude socies.habilitatscontribucionspersona --exclude socies.adrecaprojecteautoocupat > cic_stuff/initial_data/base-exclude.json

# una altra opció per generar els fitxers json seria fer la inversa definir quines taules es volen exportar:

python manage.py dumpdata auth.group alta_socies.ubicacio socies.activitat socies.gruphabilitat socies.habilitat socies.contribuciocic empreses.empresa empreses.cooperativa empreses.empresaasseguradora --indent 4 > cic_stuff/initial_data/base-include.json

python manage.py dumpdata socies --indent 4 > cic_stuff/initial_data/socies-v2.json
python manage.py dumpdata auth.user --indent 4 > cic_stuff/initial_data/users.json
python manage.py dumpdata empreses.empresa --indent 4 > cic_stuff/initial_data/empreses.json
python manage.py dumpdata alta_socies.ubicacio --indent 4 > cic_stuff/initial_data/ubicacions.json

function calcular_lletra_dni(numero) {
    // https://es.wikibooks.org/wiki/Algoritmo_para_obtener_la_letra_del_NIF#JavaScript
    return 'TRWAGMYFPDXBNJZSQVHLCKE'.charAt(numero % 23);
}

// retorna:
//   1 si el text correspon a un DNI de 8 digits numèrics i una lletra, i la lletra encaixa amb el número
//   0 si el text no són 8 dígits numèrics i una lletra
//   -1 si el text correspon a un DNI segons s'explica al primer cas, però la lletra no encaixa
function comprovar_dni(dni) {
    if (/^\d{8}[A-Za-z]$/.test(dni)) {
        var numero = parseInt(dni.substring(0, 8));
        var lletra = dni.substring(8, 9).toUpperCase();

        if (lletra == calcular_lletra_dni(numero)) {
            return 1;
        }

        return -1;
    }

    return 0;
}

// per posar com a event handler dels camps de text:
//   onkeyup='feedback_dni(event.target)'
//   onchange='feedback_dni(event.target)'
// pinta el fons del camp:
// * verd si el DNI és vàlid;
// * de groc si no és un DNI amb lletra;
// * de vermell si és un DNI però la lletra no quadra.
function feedback_dni(target) {
    var text  = target.value.trim().toUpperCase();
    var color = 'white';
    var resultat;

    if (text.length) {
        resultat = comprovar_dni(text);
        color    = 'yellow';

        if (resultat == 1) {
            color = 'lightgreen';
            target.value = text;
        } else if (resultat == -1) {
            color = 'red';
        }
    }

    target.style.backgroundColor = color;
}

// per posar a l'onkeypress dels camps de text: onkeypress='completar_dni(event)'
// si desprès d'escriure els 8 dígits numèrics es prem un asterisc,
// això completa el DNI amb la lletra que li correspon.
//   onkeypress='return completar_dni(event)'
function completar_dni(event) {
    var asterisc = 42;
    var el, lletra;

    if (event.charCode == asterisc) {
        el = event.target;

        if (/^\d{8}$/.test(el.value)) {
            lletra = calcular_lletra_dni(parseInt(el.value));
            el.value += lletra;

            return false;
        }
    }
}

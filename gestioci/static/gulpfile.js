'use strict';

var browserSync = require('browser-sync');
var concat      = require('gulp-concat');
var del         = require('del');
var gulp        = require('gulp');
var imagemin    = require('gulp-imagemin');
var uglify      = require('gulp-uglify');
var reload      = browserSync.reload;
var sass        = require('gulp-sass');

gulp.task('clean', (function () {
    var cleaned = false;

    return function (cb) {
        if (cleaned) {
            cb();
        } else {
            cleaned = true;
            del(['build/**/*'], cb);
        }
    }
})());

gulp.task('assets', ['clean'], function () {
    return gulp.src('assets/**/*')
        .pipe(imagemin({ optimizationLevel: 5 }))
        .pipe(gulp.dest('build/assets'));
});

gulp.task('fonts', ['clean'], function () {
    return gulp.src('bower_components/font-awesome/fonts/*')
        .pipe(gulp.dest('build/fonts'));
});

gulp.task('sass', ['clean'], function () {
    return gulp.src('scss/*.scss')
        .pipe(sass({
            outputStyle:  'compressed',
            includePaths: [
                'bower_components/font-awesome/scss',
                'bower_components/foundation/scss',
                'bower_components/selectize-scss/src',
                'bower_components/foundation-datepicker/stylesheets',
            ]
        }, {
            errLogToConsole: true
        }))
        .pipe(gulp.dest('build/css'))
        .pipe(reload({ stream: true }));
});

function uglifyErrHandler(err) {
    console.error('\x07', err.message);
    browserSync.notify('Javascript error', 5000);
    return this.end();
}

gulp.task('js-head', ['clean'], function () {
    return gulp.src([
        'bower_components/foundation/js/vendor/modernizr.js',
    ])
        .pipe(concat('gestioci-head.js'))
        .pipe(uglify().on('error', uglifyErrHandler))
        .pipe(gulp.dest('build/js'));
});

gulp.task('js', ['js-head'], function () {
    return gulp.src([
        'bower_components/foundation/js/vendor/jquery.js',
        'bower_components/foundation/js/vendor/fastclick.js',
        'bower_components/foundation/js/foundation/foundation.js',
        'bower_components/foundation/js/foundation/foundation.alert.js',
        'bower_components/foundation/js/foundation/foundation.clearing.js',
        'bower_components/foundation/js/foundation/foundation.tooltip.js',
        'bower_components/foundation/js/foundation/foundation.topbar.js',
        'bower_components/selectize/dist/js/standalone/selectize.js',
        'bower_components/imagesloaded/imagesloaded.pkgd.js',
        'bower_components/masonry/dist/masonry.pkgd.js',
        'bower_components/foundation-datepicker/js/foundation-datepicker.js',
        'js/eines-dni.js', // <- TODO: Refactor
        'js/gestioci.js'
    ])
        .pipe(concat('gestioci.js'))
        .pipe(uglify().on('error', uglifyErrHandler))
        .pipe(gulp.dest('build/js'));
});

gulp.task('browser-sync', function () {
    browserSync({
        proxy: 'http://localhost:8000',
        port:  4000
    });
});

gulp.task('build', ['assets', 'fonts', 'sass', 'js']);

gulp.task('default', ['build', 'browser-sync'], function () {
    gulp.watch('js/**/*', ['js'], reload);
    gulp.watch('assets/**/*', ['assets'], reload);
    gulp.watch('scss/**/*', ['sass'], reload);
    gulp.watch('../**/*.html', reload);
});

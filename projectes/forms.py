from django import forms

from empreses.models import Cooperativa
from socies.models import AdrecaProjecteAutoocupat
from .models import ProjecteAutoocupat


class FormulariEditarCooperativa(forms.Form):

    cooperativa = forms.ModelChoiceField(
        queryset=Cooperativa.objects.all(),
        label=u"Cooperativa a assignat"
    )


class FormulariEditarContactePAO(forms.ModelForm):

    class Meta:
        model = ProjecteAutoocupat
        fields = ['email', 'telefon', 'website']


class FormulariEditarDescripcioPAO(forms.ModelForm):

    class Meta:
        model = ProjecteAutoocupat
        fields = ['descripcio']


class FormulariEditarAdrecaPAO(forms.ModelForm):

    class Meta:
        model = AdrecaProjecteAutoocupat
        fields = ['provincia', 'comarca', 'poblacio', 'codi_postal', 'adreca']



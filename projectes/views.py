# coding=utf-8
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from facturacio.helpers import trobar_projectes_usuari
from gestioci.settings import auth_groups
from projectes.models import ProjecteAutoocupat
from socies.models import AdrecaProjecteAutoocupat

from .forms import FormulariEditarCooperativa, FormulariEditarContactePAO, \
    FormulariEditarDescripcioPAO, FormulariEditarAdrecaPAO


@login_required
def resum_projecte_autoocupat(request, id_projecte):

    projecte = ProjecteAutoocupat.objects.get(id=id_projecte)

    is_membre_de_referencia = projecte in trobar_projectes_usuari(request.user)
    is_responsable_facturacio = request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists()

    if not is_membre_de_referencia and not is_responsable_facturacio:
        messages.success(request, u"No tens permís per accedir a aquesta informació")
        return redirect('inici')

    return render(
        request,
        'projectes/resum_projecte_autoocupat_base.html',
        dict(
            p=projecte,
        )
    )


@login_required
def editar_adreca_projecte(request, id_projecte, id_adreca):

    projecte = ProjecteAutoocupat.objects.get(id=id_projecte)

    is_responsable_facturacio = request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists()
    
    if not is_responsable_facturacio:
        messages.success(request, u"No tens permís per accedir a aquesta informació")
        return redirect('inici')

    adreca = AdrecaProjecteAutoocupat.objects.get(id=id_adreca)
    
    if request.method == 'POST':
        form = FormulariEditarAdrecaPAO(instance=adreca, data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, u"S'ha actualitzat l'adreça correctament")
            return redirect('projectes:resum_projecte_autoocupat', id_projecte=projecte.id)

    form = FormulariEditarAdrecaPAO(instance=adreca)

    return render(
        request,
        'projectes/editar_adreca_projecte.html',
        {
            'p': projecte,
            'form': form,
        }
    )


@login_required
def editar_descripcio_projecte(request, id_projecte):

    projecte = ProjecteAutoocupat.objects.get(id=id_projecte)

    is_responsable_facturacio = request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists()
    
    if not is_responsable_facturacio:
        messages.success(request, u"No tens permís per accedir a aquesta informació")
        return redirect('inici')

    if request.method == 'POST':
        form = FormulariEditarDescripcioPAO(instance=projecte, data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, u"S'ha actualitzat la descripció correctament")
            return redirect('projectes:resum_projecte_autoocupat', id_projecte=projecte.id)

    form = FormulariEditarDescripcioPAO(instance=projecte)

    return render(
        request,
        'projectes/editar_descripcio_projecte.html',
        {
            'p': projecte,
            'form': form,
        }
    )


@login_required
def editar_contacte_projecte(request, id_projecte):

    projecte = ProjecteAutoocupat.objects.get(id=id_projecte)

    is_responsable_facturacio = request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists()
    
    if not is_responsable_facturacio:
        messages.success(request, u"No tens permís per accedir a aquesta informació")
        return redirect('inici')

    if request.method == 'POST':
        form = FormulariEditarContactePAO(instance=projecte, data=request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, u"S'ha actualitzat el contacte correctament")
            return redirect('projectes:resum_projecte_autoocupat', id_projecte=projecte.id)

    form = FormulariEditarContactePAO(instance=projecte)

    return render(
        request,
        'projectes/editar_contacte_projecte.html',
        {
            'p': projecte,
            'form': form,
        }
    )


@login_required
def editar_cooperativa_projecte(request, id_projecte):

    projecte = ProjecteAutoocupat.objects.get(id=id_projecte)

    is_responsable_facturacio = request.user.groups.filter(name=auth_groups.RESPONSABLES_FACTURACIO).exists()
    
    if not is_responsable_facturacio:
        messages.success(request, u"No tens permís per accedir a aquesta informació")
        return redirect('inici')

    if request.method == 'POST':
        form = FormulariEditarCooperativa(data=request.POST)
        if form.is_valid():
            projecte.cooperativa_assignada = form.cleaned_data['cooperativa']
            projecte.save()
            messages.success(request, u"S'ha actualitzat la cooperativa correctament")
            return redirect('projectes:resum_projecte_autoocupat', id_projecte=projecte.id)

    form = FormulariEditarCooperativa(
        initial={
            'cooperativa': projecte.cooperativa_assignada,
        }
    )

    return render(
        request,
        'projectes/editar_cooperativa_projecte.html',
        {
            'p': projecte,
            'form': form,
        }
    )
   

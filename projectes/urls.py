from django.conf.urls import patterns, url

urlpatterns = patterns(

    'projectes.views',

    url(r'^resum_projecte_autoocupat/(?P<id_projecte>\d+)$', 'resum_projecte_autoocupat', name='resum_projecte_autoocupat'),

    url(r'^editar_cooperativa_projecte/(?P<id_projecte>\d+)$', 'editar_cooperativa_projecte', name='editar_cooperativa_projecte'),

    url(r'^editar_contacte_projecte/(?P<id_projecte>\d+)$', 'editar_contacte_projecte', name='editar_contacte_projecte'),

    url(r'^editar_descripcio_projecte/(?P<id_projecte>\d+)$', 'editar_descripcio_projecte', name='editar_descripcio_projecte'),

    url(r'^editar_adreca_projecte/(?P<id_projecte>\d+)/(?P<id_adreca>\d+)$', 'editar_adreca_projecte', name='editar_adreca_projecte'),
)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime

from pytz import timezone


class Migration(migrations.Migration):

    epoch = datetime.datetime(1970, 1, 1, hour=0, minute=0, second=0, microsecond=0, tzinfo=timezone('UTC'))

    dependencies = [
        ('projectes', '0003_auto_20160213_1257'),
    ]

    operations = [
        migrations.AddField(
            model_name='projecteautoocupat',
            name='created_at',
            field=models.DateTimeField(default=epoch, auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='projecteautoocupat',
            name='transversal',
            field=models.BooleanField(default=False, help_text="es va importar [autom\xe0ticament] del sistema anterior, i est\xe0pendent d'una revisi\xf3 exhaustiva?"),
        ),
        migrations.AddField(
            model_name='projecteautoocupat',
            name='updated_at',
            field=models.DateTimeField(default=epoch, auto_now=True),
            preserve_default=False,
        ),
    ]

from django.contrib import admin
from .models import EmpresaAsseguradora, Cooperativa, Empresa

for model in (Cooperativa,
              EmpresaAsseguradora,
              Empresa):

    admin.site.register(model)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('empreses', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CodisComptables',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('relacio', models.CharField(max_length=16, choices=[(b'proveidor', 'Prove\xefdor'), (b'client', 'Client')])),
                ('codi_comptable', models.IntegerField(help_text='comen\xe7a per 430 si \xe9s client de la cooperativa, per 400 si \xe9s prove\xefdor')),
                ('cooperativa', models.ForeignKey(related_name='+', to='empreses.Cooperativa')),
            ],
        ),
        migrations.CreateModel(
            name='CompteCorrentCooperativa',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('iban', models.CharField(help_text='Codi IBAN (si no el sabeu, comenceu per ES77 i afegiu el CCC a continuaci\xf3', max_length=34, null=True, blank=True)),
                ('tipus', models.CharField(max_length=16, choices=[(b'per_cobrar', 'Per rebre ingresos'), (b'per_pagar', 'Per fer o domiciliar pagaments')])),
                ('comentaris', models.TextField(null=True, blank=True)),
                ('cooperativa', models.ForeignKey(to='empreses.Cooperativa')),
            ],
        ),
        migrations.CreateModel(
            name='IAE',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('codi', models.CharField(max_length=6)),
                ('nom', models.CharField(max_length=256)),
            ],
        ),
        migrations.AddField(
            model_name='empresa',
            name='compte_corrent',
            field=models.CharField(help_text='Codi IBAN (si no el sabeu, comenceu per ES77 i afegiu el CCC a continuaci\xf3', max_length=34, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='empresa',
            name='email',
            field=models.EmailField(max_length=254, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='empresa',
            name='pendent_de_revisio',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='empresa',
            name='telefon',
            field=models.CharField(max_length=64, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='empresa',
            name='usuaria_que_demana_revisio',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='empresa',
            name='vist_i_plau_data',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='empresa',
            name='vist_i_plau_usuaria',
            field=models.ForeignKey(related_name='+', blank=True, to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='codiscomptables',
            name='empresa',
            field=models.ForeignKey(related_name='+', to='empreses.Empresa'),
        ),
    ]

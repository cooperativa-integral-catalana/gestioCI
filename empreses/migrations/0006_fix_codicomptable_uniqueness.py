# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('empreses', '0005_rename_codicomptable'),
    ]

    operations = [

        migrations.AlterField(
            model_name='codicomptable',
            name='codi_comptable',
            field=models.IntegerField(help_text='comen\xe7a per 430 si \xe9s client de la cooperativa, o per 400 si \xe9s prove\xefdor'),
        ),
        migrations.AlterUniqueTogether(
            name='codicomptable',
            unique_together=set([('cooperativa', 'codi_comptable'), ('cooperativa', 'empresa', 'relacio')]),
        ),
    ]

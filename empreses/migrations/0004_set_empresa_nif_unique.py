# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('empreses', '0003_empresa_adreca_fiscal'),
    ]

    operations = [
        migrations.AlterField(
            model_name='empresa',
            name='compte_corrent',
            field=models.CharField(help_text='Codi IBAN', max_length=34, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='empresa',
            name='nif',
            field=models.CharField(unique=True, max_length=64, verbose_name=b'NIF'),
        ),
    ]

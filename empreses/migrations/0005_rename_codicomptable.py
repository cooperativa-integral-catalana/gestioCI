# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('empreses', '0004_set_empresa_nif_unique'),
    ]

    operations = [
        migrations.RenameModel('CodisComptables', 'CodiComptable'),
    ]

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0005_auto_20150629_1815'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdrecaFiscal',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pais', models.CharField(max_length=128, verbose_name='pa\xeds')),
                ('provincia', models.CharField(max_length=128, verbose_name='prov\xedncia')),
                ('comarca', models.CharField(max_length=128)),
                ('poblacio', models.CharField(max_length=128, verbose_name='poblaci\xf3')),
                ('codi_postal', models.CharField(max_length=16)),
                ('adreca', models.CharField(max_length=128, verbose_name='adre\xe7a')),
                ('ubicacio_especifica', models.CharField(max_length=256, null=True, verbose_name='ubicaci\xf3 espec\xedfica', blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Adre\xe7a fiscal',
                'verbose_name_plural': 'Adreces fiscals',
            },
        ),
    ]

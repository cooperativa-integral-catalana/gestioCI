# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Activitat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(unique=True, max_length=256)),
                ('firaire', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AdrecaProjecteAutoocupat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pais', models.CharField(max_length=128, verbose_name='pa\xeds')),
                ('provincia', models.CharField(max_length=128, verbose_name='prov\xedncia')),
                ('comarca', models.CharField(max_length=128)),
                ('poblacio', models.CharField(max_length=128, verbose_name='poblaci\xf3')),
                ('codi_postal', models.CharField(max_length=16)),
                ('adreca', models.CharField(max_length=128, verbose_name='adre\xe7a')),
                ('ubicacio_especifica', models.CharField(max_length=256, null=True, verbose_name='ubicaci\xf3 espec\xedfica', blank=True)),
                ('designacio', models.CharField(help_text='nom per identificar aquesta adre\xe7a, especialment si n\'hi ha m\xe9s d\'una al projecte; p.ex. es podrien dir "restaurant" i "botiga".', max_length=128, verbose_name='designaci\xf3')),
                ('traspas', models.CharField(default=b'cap', max_length=10, choices=[(b'cap', 'no cal cap mena de trasp\xe0s'), (b'cessio', "cal cessi\xf3 d'\xfas a la CIC"), (b'lloguer', 'cal el lloguer a nom de la CIC')])),
                ('traspas_comentaris_avaluacio', models.TextField(help_text='Comentaris que es fan als membres de refer\xe8ncia en el moment de demanar-los la cessi\xf3', max_length=1024, null=True, blank=True)),
                ('traspas_inici', models.DateField(null=True, blank=True)),
                ('traspas_final', models.DateField(null=True, blank=True)),
                ('traspas_comentaris_alta', models.TextField(help_text="Observacions i particularitats recollides durant l'alta", max_length=1024, null=True, blank=True)),
                ('traspas_lloguer_import_mensual', models.DecimalField(null=True, verbose_name='import mensual del lloguer', max_digits=8, decimal_places=2, blank=True)),
                ('activitats', models.ManyToManyField(to='socies.Activitat', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'adre\xe7a de projecte autoocupat',
                'verbose_name_plural': 'adreces de projectes autoocupats',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ContribucioCIC',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=128)),
            ],
            options={
                'verbose_name': 'Tipus de contribuci\xf3 a la CIC',
                'verbose_name_plural': 'Tipus de contribucions a la CIC',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='GrupHabilitat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=128)),
            ],
            options={
                'verbose_name': "Grup d'habilitats",
                'verbose_name_plural': "Grups d'habilitats",
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Habilitat',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=128)),
                ('grup', models.ForeignKey(to='socies.GrupHabilitat')),
            ],
            options={
                'verbose_name': 'Habilitat',
                'verbose_name_plural': 'Habilitats',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HabilitatsContribucionsPersona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('detalls_habilitats', models.TextField(null=True, blank=True)),
                ('contribucions', models.ManyToManyField(to='socies.ContribucioCIC', null=True, blank=True)),
                ('habilitats', models.ManyToManyField(to='socies.Habilitat', null=True, blank=True)),
            ],
            options={
                'verbose_name': 'habilitats i contribucions de persona',
                'verbose_name_plural': 'habilitats contribucions de persones',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=64, null=True, blank=True)),
                ('cognom1', models.CharField(max_length=64, null=True, blank=True)),
                ('cognom2', models.CharField(max_length=64, null=True, blank=True)),
                ('pseudonim', models.CharField(max_length=64, null=True, blank=True)),
                ('email', models.EmailField(max_length=75, null=True, blank=True)),
                ('telefon', models.CharField(max_length=64, null=True, blank=True)),
                ('dni', models.CharField(max_length=64)),
                ('tipus_document_dni', models.CharField(default=b'dni', max_length=16, choices=[(b'dni', b'dni'), (b'nie', b'nie'), (b'passaport', b'passaport')])),
                ('compte_integralces', models.CharField(max_length=16, null=True, verbose_name='n\xfamero de compte a IntegralCES', blank=True)),
                ('tots_els_detalls', models.CharField(max_length=512, blank=True)),
                ('nom_sencer', models.CharField(unique=True, max_length=360, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'persona',
                'verbose_name_plural': 'persones',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PersonaCercable',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nom', models.CharField(max_length=64, null=True, blank=True)),
                ('cognom1', models.CharField(max_length=64, null=True, blank=True)),
                ('cognom2', models.CharField(max_length=64, null=True, blank=True)),
                ('pseudonim', models.CharField(max_length=64, null=True, blank=True)),
                ('email', models.EmailField(max_length=75, null=True, blank=True)),
                ('telefon', models.CharField(max_length=64, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PersonaUsuaria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('persona', models.OneToOneField(related_name='usuaria', to='socies.Persona')),
                ('usuari', models.OneToOneField(related_name='persona_cooperativa', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Prefer\xe8ncies de persona usuaria',
                'verbose_name_plural': 'Pref\xe8ncies de persones usuaries',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PreferenciaPersonaUsuaria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('clau', models.CharField(max_length=128)),
                ('valor', models.CharField(max_length=128)),
                ('persona', models.ForeignKey(related_name='preferencies', to='socies.PersonaUsuaria')),
            ],
            options={
                'verbose_name': 'Prefer\xe8ncia de persona usuaria',
                'verbose_name_plural': 'Prefer\xe8ncies de persones usu\xe0ries',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='persona',
            name='_persona_cercable',
            field=models.OneToOneField(null=True, blank=True, to='socies.PersonaCercable'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='habilitatscontribucionspersona',
            name='persona',
            field=models.OneToOneField(related_name='_habilitats_contribucions', to='socies.Persona'),
            preserve_default=True,
        ),
    ]

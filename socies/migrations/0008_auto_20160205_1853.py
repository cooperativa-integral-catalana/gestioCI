# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0007_adrecaprojectecollectiu'),
    ]

    operations = [
        migrations.AlterField(
            model_name='adrecafiscal',
            name='comarca',
            field=models.CharField(max_length=128, blank=True),
        ),
        migrations.AlterField(
            model_name='adrecafiscal',
            name='pais',
            field=models.CharField(max_length=128, verbose_name='pa\xeds', blank=True),
        ),
        migrations.AlterField(
            model_name='adrecafiscal',
            name='provincia',
            field=models.CharField(max_length=128, verbose_name='prov\xedncia', blank=True),
        ),
        migrations.AlterField(
            model_name='adrecafiscal',
            name='ubicacio_especifica',
            field=models.CharField(default='', max_length=256, verbose_name='ubicaci\xf3 espec\xedfica', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='adrecaprojecteautoocupat',
            name='comarca',
            field=models.CharField(max_length=128, blank=True),
        ),
        migrations.AlterField(
            model_name='adrecaprojecteautoocupat',
            name='pais',
            field=models.CharField(max_length=128, verbose_name='pa\xeds', blank=True),
        ),
        migrations.AlterField(
            model_name='adrecaprojecteautoocupat',
            name='provincia',
            field=models.CharField(max_length=128, verbose_name='prov\xedncia', blank=True),
        ),
        migrations.AlterField(
            model_name='adrecaprojecteautoocupat',
            name='ubicacio_especifica',
            field=models.CharField(default='', max_length=256, verbose_name='ubicaci\xf3 espec\xedfica', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='adrecaprojectecollectiu',
            name='comarca',
            field=models.CharField(max_length=128, blank=True),
        ),
        migrations.AlterField(
            model_name='adrecaprojectecollectiu',
            name='pais',
            field=models.CharField(max_length=128, verbose_name='pa\xeds', blank=True),
        ),
        migrations.AlterField(
            model_name='adrecaprojectecollectiu',
            name='provincia',
            field=models.CharField(max_length=128, verbose_name='prov\xedncia', blank=True),
        ),
        migrations.AlterField(
            model_name='adrecaprojectecollectiu',
            name='ubicacio_especifica',
            field=models.CharField(default='', max_length=256, verbose_name='ubicaci\xf3 espec\xedfica', blank=True),
            preserve_default=False,
        ),
    ]

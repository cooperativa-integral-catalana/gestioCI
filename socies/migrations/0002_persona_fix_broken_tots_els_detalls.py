# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.db import connection


def actualitzar_camps_calculats(apps, schema_editor):

    Persona = apps.get_model('socies', 'Persona')

    # des de les migracions no es poden cridar mètodes del model
    # com ara p.actualitza_camps_calculats(), p._nom_sencer() o
    # p._tots_els_detalls() — Hem de copiar aqui el codi que cal
    # per fer aquestes operacions:

    # còpia del mètode del mateix nom a socies.Persona
    def _tots_els_detalls(persona):
        telefon = u""
        if persona.telefon:
            telefon = u" — tel:%s" % persona.telefon
        dni = u""
        if persona.dni:
            dni = u" — %s:%s" % (persona.get_tipus_document_dni_display(), persona.dni)
        result = persona.nom_sencer + telefon + dni
        return result

    # còpia del mètode del mateix nom a socies.Persona
    def _nom_sencer(persona):
        pseudonim = u''
        if persona.pseudonim:
            pseudonim = u'"%s"' % persona.pseudonim
        email = ''
        if persona.email:
            email = '<%s>' % persona.email
        return u' '.join(u' '.join((persona.nom,
                                    pseudonim,
                                    persona.cognom1,
                                    persona.cognom2,
                                    email)).split())

    for p in Persona.objects.all():

        last_updated = p.updated_at

        p.nom_sencer = _nom_sencer(p)
        p.tots_els_detalls = _tots_els_detalls(p)

        p.save()

        connection.cursor().execute(
            "UPDATE socies_persona SET updated_at = %s WHERE id = %s",
            [last_updated, p.id])


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(actualitzar_camps_calculats),
    ]

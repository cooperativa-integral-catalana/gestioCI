# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0009_adrecasociacooperativa'),
    ]

    operations = [
        migrations.AlterField(
            model_name='adrecafiscal',
            name='comarca',
            field=models.CharField(max_length=128),
        ),
        migrations.AlterField(
            model_name='adrecaprojecteautoocupat',
            name='comarca',
            field=models.CharField(max_length=128),
        ),
        migrations.AlterField(
            model_name='adrecaprojectecollectiu',
            name='comarca',
            field=models.CharField(max_length=128),
        ),
        migrations.AlterField(
            model_name='adrecasociacooperativa',
            name='comarca',
            field=models.CharField(max_length=128),
        ),
    ]

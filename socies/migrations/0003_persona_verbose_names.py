# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('socies', '0002_persona_fix_broken_tots_els_detalls'),
    ]

    operations = [
        migrations.AlterField(
            model_name='persona',
            name='pseudonim',
            field=models.CharField(max_length=64, null=True, verbose_name='Pseud\xf2nim', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='persona',
            name='telefon',
            field=models.CharField(max_length=64, null=True, verbose_name='Tel\xe8fon', blank=True),
            preserve_default=True,
        ),
    ]

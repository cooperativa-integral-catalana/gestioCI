# coding=utf-8
from django.contrib.auth.models import User
from django.db import models, transaction

from helpers import normalitzacio_per_cerca, normalitzacio_per_cerca_email, normalitzacio_per_cerca_telefon


class PersonaCercable(models.Model):
    """
    Classe per ús intern; conté els camps de l'objecte Persona "normalitzats" sense
    accents, dièresi, etc. Es crea i s'actualitza des de persona.save().
    """
    nom = models.CharField(max_length=64, blank=True, null=True)
    cognom1 = models.CharField(max_length=64, blank=True, null=True)
    cognom2 = models.CharField(max_length=64, blank=True, null=True)
    pseudonim = models.CharField(max_length=64, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    telefon = models.CharField(max_length=64, blank=True, null=True)
    dni = models.CharField(max_length=64, blank=True, null=True)
    compte_integralces = models.CharField(max_length=16, blank=True, null=True)


class Persona(models.Model):

    TIPUS_DOCUMENT_ID_DNI = 'dni'
    TIPUS_DOCUMENT_ID_NIE = 'nie'
    TIPUS_DOCUMENT_ID_PASSAPORT = 'passaport'

    TIPUS_DOCUMENT_ID = (
        (TIPUS_DOCUMENT_ID_DNI, "dni"),
        (TIPUS_DOCUMENT_ID_NIE, "nie"),
        (TIPUS_DOCUMENT_ID_PASSAPORT, "passaport"),
    )

    nom = models.CharField(max_length=64, blank=True, null=True)
    cognom1 = models.CharField(max_length=64, blank=True, null=True)
    cognom2 = models.CharField(max_length=64, blank=True, null=True)
    pseudonim = models.CharField(max_length=64, blank=True, null=True, verbose_name=u"Pseudònim")

    email = models.EmailField(blank=True, null=True)
    telefon = models.CharField(max_length=64, blank=True, null=True, verbose_name=u"Telèfon")

    # TODO create a custom DNIField? combining "tipus_document_dni" and "dni" in a single field plus a custom widget?
    dni = models.CharField(max_length=64, blank=True, null=True)
    tipus_document_dni = models.CharField(max_length=16, choices=TIPUS_DOCUMENT_ID, default=TIPUS_DOCUMENT_ID_DNI)
    compte_integralces = models.CharField(max_length=16, blank=True, null=True,
                                          verbose_name=u"número de compte a IntegralCES")

    # aquests camps són calculats a partir dels altres
    # potser això farà més complexes els forms basats en Persona
    tots_els_detalls = models.CharField(max_length=512, blank=True)
    nom_sencer = models.CharField(max_length=360, blank=True, unique=True)
    _persona_cercable = models.OneToOneField(PersonaCercable, blank=True, null=True)  # save() en creara un, si cal

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u"persona"
        verbose_name_plural = u"persones"

    @transaction.atomic
    def save(self, *args, **kwargs):
        # TODO convertir el punt en middot per l'ela geminada...
        # TODO figure out a better way to avoid leding and trailing spaces
        if self.nom:
            self.nom = self.nom.strip()
        if self.cognom1:
            self.cognom1 = self.cognom1.strip()
        if self.cognom2:
            self.cognom2 = self.cognom2.strip()
        if self.pseudonim:
            self.pseudonim = self.pseudonim.strip()
        if self.email:
            self.email = self.email.strip()
        if self.telefon:
            self.telefon = self.telefon.strip()
        if self.dni:
            self.dni = self.dni.strip().upper()
        if self.compte_integralces:
            self.compte_integralces = self.compte_integralces.strip().upper()

        self.actualitza_camps_calculats()

        super(Persona, self).save(*args, **kwargs)

    def actualitza_camps_calculats(self):
        self.nom_sencer = self._nom_sencer()
        self.tots_els_detalls = self._tots_els_detalls()
        self._persona_cercable = self._actualitza_persona_cercable()

    @property
    def habilitats_contribucions(self):
        try:
            return self._habilitats_contribucions
        except HabilitatsContribucionsPersona.DoesNotExist:
            return None

    def __unicode__(self):
        return self.nom_sencer

    @property
    def nom_sencer_sense_email(self):
        return self._nom_sencer(sense_email=True)

    def _actualitza_persona_cercable(self):
        pc = self._persona_cercable or PersonaCercable()
        pc.nom = normalitzacio_per_cerca(self.nom)
        pc.cognom1 = normalitzacio_per_cerca(self.cognom1)
        pc.cognom2 = normalitzacio_per_cerca(self.cognom2)
        pc.pseudonim = normalitzacio_per_cerca(self.pseudonim)
        pc.email = normalitzacio_per_cerca_email(self.email)
        pc.telefon = normalitzacio_per_cerca_telefon(self.telefon)
        pc.dni = normalitzacio_per_cerca(self.dni)
        pc.compte_integralces = normalitzacio_per_cerca(self.compte_integralces)
        pc.save()
        return pc

    def _tots_els_detalls(self):

        telefon = u""
        if self.telefon:
            telefon = u" - tel:%s" % self.telefon

        dni = u""
        if self.dni:
            dni = u" - %s:%s" % (self.get_tipus_document_dni_display(), self.dni)

        result = self.nom_sencer + telefon + dni

        return result

    def _nom_sencer(self, sense_email=False):

        pseudonim = u''
        if self.pseudonim:
            pseudonim = u'"%s"' % self.pseudonim

        email = ''
        if self.email and not sense_email:
            email = '<%s>' % self.email

        return u' '.join(u' '.join((self.nom,
                                    pseudonim,
                                    self.cognom1,
                                    self.cognom2,
                                    email)).split())

    def crear_usuaria_si_cal(self):
        import os, base64
        
        try:

            pu = self.usuaria

            assert pu.usuari.email == self.email  # TODO improve "error" handling

        except PersonaUsuaria.DoesNotExist:

            if self.nom:
                nom = self.nom
                cognoms = (u'%s %s' % (self.cognom1, self.cognom2)).strip()
            else:
                nom = self.pseudonim
                cognoms = u''

            # TODO: fix this fucking piece of shit; we don't have time to deal with this right now.
            # see https://code.djangoproject.com/ticket/20846
            # see https://github.com/django/django/commit/780bddf75b93784470a2e352ed44ee35a751d667
            username = self.email[0:30]  # chop "too long usernames"

            # TODO no hauria d'existir cap usuari amb aquest correu electrònic; com que això hauria d'acabar a l'LDAP
            # no em sembla un problema que haguem de tractar en aquest moment; per ara, cal la nostra intervenció manual
            if User.objects.filter(email=self.email).exists() | User.objects.filter(username=username).exists():
                raise Exception(u"Hi ha un problema amb el compte de \"%s\". "
                                u"Si us plau avisa a sat@cooperativaintegral.cat" % self.email)

            
            u = User.objects.create_user(username=username,
                                         email=self.email,
                                         first_name=nom[0:30],
                                         last_name=cognoms[0:30])
            u.set_password(base64.b64encode(os.urandom(16)))
            u.save()
            pu = PersonaUsuaria.objects.create(usuari=u, persona=self)

            # TODO cal fer alguna cosa per activar l'usuari?
            # TODO caldria engegar en aquest punt el procés de 'password reset' per al nou usuari

        return pu


class PersonaUsuaria(models.Model):

    persona = models.OneToOneField(Persona, related_name='usuaria')
    usuari = models.OneToOneField(User, related_name='persona_cooperativa')

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def get_preferencia(self, clau, valor_si_no_existeix=None):
        try:
            return self.preferencies.get(clau=clau)
        except PreferenciaPersonaUsuaria.DoesNotExist:
            return valor_si_no_existeix

    class Meta:
        verbose_name = u"Preferències de persona usuaria"
        verbose_name_plural = u"Prefències de persones usuaries"


class PreferenciaPersonaUsuaria(models.Model):

    persona = models.ForeignKey(PersonaUsuaria, related_name='preferencies')
    clau = models.CharField(max_length=128)
    valor = models.CharField(max_length=128)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u"Preferència de persona usuaria"
        verbose_name_plural = u"Preferències de persones usuàries"


class ContribucioCIC(models.Model):

    nom = models.CharField(max_length=128, blank=False)

    class Meta:
        verbose_name = u"Tipus de contribució a la CIC"
        verbose_name_plural = u"Tipus de contribucions a la CIC"

    def __unicode__(self):
        return self.nom


class GrupHabilitat(models.Model):

    nom = models.CharField(max_length=128, blank=False)

    class Meta:
        ordering = ['nom']
        verbose_name = u"Grup d'habilitats"
        verbose_name_plural = u"Grups d'habilitats"

    def __unicode__(self):
        return self.nom


class Habilitat(models.Model):

    nom = models.CharField(max_length=128, blank=False)
    grup = models.ForeignKey(GrupHabilitat)

    class Meta:
        ordering = ['nom']
        verbose_name = u"Habilitat"
        verbose_name_plural = u"Habilitats"

    def __unicode__(self):
        return self.nom


class HabilitatsContribucionsPersona(models.Model):
    """
    Enregistra, per a una persona, quines habilitats, coneixements i experiència pot
    aportar (d'aqui la part "Habilitats" del nom de la classe) i en quines coses pot i vol
    contribuïr o implicar-se amb la cooperativa (contribucions). Addicionalment hi ha
    un camp de text per ampliar o aclarir el que calgui aclarir. En principi han demanat
    únicament aquest camp per les "habilitats" però podria servir per totes dues coses
    o bé podriem afegir un altre camp per les "contribucions".

    Quan pensem en tots els detalls no s'acaba mai, oi? :)
    """

    persona = models.OneToOneField(Persona, related_name='_habilitats_contribucions')
    contribucions = models.ManyToManyField(ContribucioCIC, blank=True)
    habilitats = models.ManyToManyField(Habilitat, blank=True)
    detalls_habilitats = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = u"habilitats i contribucions de persona"
        verbose_name_plural = u"habilitats contribucions de persones"


class Activitat(models.Model):

    nom = models.CharField(max_length=256, unique=True)
    firaire = models.BooleanField(default=False)

    class Meta:
        ordering = ['nom']

    def __unicode__(self):
        return self.nom


class Adreca(models.Model):

    pais = models.CharField(max_length=128, blank=True, verbose_name=u"país")
    provincia = models.CharField(max_length=128, blank=True, verbose_name=u"província")
    comarca = models.CharField(max_length=128, blank=False)
    poblacio = models.CharField(max_length=128, verbose_name=u"població")
    codi_postal = models.CharField(max_length=16)
    adreca = models.CharField(max_length=128, verbose_name=u"adreça")
    ubicacio_especifica = models.CharField(max_length=256, blank=True, verbose_name=u"ubicació específica")

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = "adreça"
        verbose_name_plural = "adreces"
        abstract = True

    def __unicode__(self):
        return u' - '.join([
            self.adreca,
            self.poblacio,
            self.comarca,
            self.provincia,
            self.pais])


class AdrecaFiscal(Adreca):

    class Meta:
        verbose_name = u"Adreça fiscal"
        verbose_name_plural = u"Adreces fiscals"


class AdrecaProjecteCollectiu(Adreca):

    class Meta(Adreca.Meta):
        verbose_name = u"adreça de projecte col·lectiu"
        verbose_name_plural = u"adreces de projectes col·lectius"


class AdrecaSociaCooperativa(Adreca):

    class Meta(Adreca.Meta):
        verbose_name = u"Adreça d'una socia de la cooperativa"
        verbose_name_plural = u"Adreçes de les socies de la cooperativa"


class AdrecaProjecteAutoocupat(Adreca):

    TIPUS_TRASPAS_CAP = 'cap'
    TIPUS_TRASPAS_LLOGUER = 'lloguer'
    TIPUS_TRASPAS_CESSIO = 'cessio'

    TIPUS_TRASPAS = (
        (TIPUS_TRASPAS_CAP, u"no cal cap mena de traspàs"),
        (TIPUS_TRASPAS_CESSIO, u"cal cessió d'ús a la CIC"),
        (TIPUS_TRASPAS_LLOGUER, u"cal el lloguer a nom de la CIC")
    )

    designacio = models.CharField(max_length=128, verbose_name=u"designació",
                                  help_text=u"Nom per identificar aquesta adreça, especialment si n'hi ha més d'una "
                                            u"al projecte; p.ex. es podrien dir \"restaurant\" i \"botiga\".")

    traspas = models.CharField(max_length=10, choices=TIPUS_TRASPAS, default=TIPUS_TRASPAS_CAP)
    traspas_comentaris_avaluacio = models.TextField(max_length=1024, blank=True, null=True,
                                                    help_text=u"Comentaris que es fan als membres de referència en el "
                                                              u"moment de demanar-los la cessió")
    traspas_inici = models.DateField(blank=True, null=True)
    traspas_final = models.DateField(blank=True, null=True)
    traspas_comentaris_alta = models.TextField(max_length=1024, blank=True, null=True,
                                               help_text=u"Observacions i particularitats recollides durant l'alta")

    traspas_lloguer_import_mensual = models.DecimalField(max_digits=8, decimal_places=2,
                                                         blank=True, null=True,
                                                         verbose_name=u"import mensual del lloguer")

    activitats = models.ManyToManyField(Activitat,
                                        related_name='+',
                                        limit_choices_to={'firaire': False})

    class Meta:
        verbose_name = u"adreça de projecte autoocupat"
        verbose_name_plural = u"adreces de projectes autoocupats"


# class PeriodeActiuProjecteAutoocupat(models.Model):
#
#     socia = models.ForeignKey(ProjecteAutoocupat)
#     inici = models.DateField()
#     final = models.DateField(null=True, blank=True)
#     motiu_baixa = models.TextField(null=False, blank=False, verbose_name=u"motiu de la baixa")

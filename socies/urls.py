from django.conf.urls import patterns, url

urlpatterns = patterns(
    'socies.views',
    url(r'^cerca_persones/$', 'cerca_persones', name='cerca_persones'),
)
